// BASE SETUP
// =============================================================================
var express = require('express'),
    https = require('https'),
    mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities'),
    app = express(),
    paypal = require('paypal-rest-sdk'),
    a_paypal = require('paypal-adaptive'),
    crypto = require('crypto'),
    getSlug = require('speakingurl'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    range = require("range"),
    cron = require('node-cron'),
    jwt = require('jsonwebtoken'),
    pool = mysql.createPool({
        host: '185.65.244.247',
        user: 'gv_user',
        password: 'JzuNbRa5MF4Uiit6',
        database: 'gymventory',
        multipleStatements: true
    }),
    fs = require('fs'),
    path = require('path'),

    Notification = require('./notification'),

    multer = require('multer'),
    storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, './uploads')
        },
        filename: function(req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    }),
    storage2 = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, './uploads/chat_imgs')
        },
        filename: function(req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    }),
    upload = multer({
        storage: storage
    })
uploadChat = multer({
    storage: storage2
})
nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport('smtps://padlasmail@gmail.com:alexandr9317@smtp.gmail.com'),
    emailTemplate = '',

    sendPwdReset = transporter.templateSender({
        subject: 'Gymventory',
        html: emailTemplate
    }, {
        from: 'no-replay@Gymventory.com',
    });

paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AZ6TyBRQg7vtXpJEDhCPbJXUekzl9RblmL5e01BN9kPxs3976aLHLyu0yNav5uQi80bQsuV3ivZ6kUS6',
    'client_secret': 'EAmkhkZWF5Tbotu5wq9WL7yZECnnk51-Gcy-GRXIuDKAaQPwNKhuHWsYw4WZvMKfb3YOHumWA5bd5X-d',
    'headers': {
        'custom': 'header'
    }
});

var paypalSdk = new a_paypal({
    userId: 'info_api1.gymventory.com',
    password: 'SB52TXEC8XB9ZP6H',
    signature: 'AuXiEO9ElO9UDsT-1lv2IfiZmFARAsl0llblU7UpBJvc06IAF3R.aVKh',
    sandbox: true //defaults to false
});
var findWithAttr = function(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}


//  connection = mysql.createConnection('mysql://snightco_new2:LddIE43(23m(30@_@odo@testing.snight.com/snightco_new2')
app.use(cors())
app.use('/uploads', express.static('./uploads'));
app.use('/img', express.static('./img'));
app.use('/data', express.static('./data'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var port = 6969;

fs.readFile('./templates/email_template.html', (err, data) => {
    if (err) throw err;
    emailTemplate = data;
    sendPwdReset = transporter.templateSender({
        subject: 'Gymventory',
        html: emailTemplate
    }, {
        from: 'no-replay@gymventory.com',
    });
});

fs.readFile('./templates/email_notification.html', (err, data) => {
    if (err) throw err;
    emailTemplate = data;
    notificationSendMail = transporter.templateSender({
        subject: 'Gymventory',
        html: emailTemplate
    }, {
        from: 'no-replay@gymventory.com',
    });
});
// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();

router.route('/authenticate')
    .post(function(req, res) {
        var email = req.body.email || ''
        var password = crypto.createHash('md5').update(req.body.password).digest("hex") || '';
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.queryRow('SELECT * from users WHERE `email` = "' + email + '" AND `type` <> 0', function(err, row, fields) {
                    if (!err) {
                        if (row) {
                            passwordHash = crypto.createHash('sha1').update(password + row.salt).digest("hex");
                            if (passwordHash === row.password) {

                                var user = {
                                    "id": row.id,
                                    "email": row.email
                                }
                                var token = jwt.sign(user, ' ', {
                                    expiresIn: 1440 * 60 * 30
                                });
                                res.json({
                                    status: true,
                                    token: token
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: 'Invalid password',
                                    field: "password"
                                });
                            }
                        } else {
                            res.json({
                                status: false,
                                message: 'User not found',
                                field: "username"
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            }
            connection.release();
        });
    })
router.route('/adminauth')
    .post(function(req, res) {
        var login = req.body.email || ''
        var password = crypto.createHash('md5').update(req.body.password).digest("hex") || '';
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.queryRow('SELECT salt, password, email from users WHERE `login` = "' + login + '" AND `type` = 0', function(err, row, fields) {
                    if (!err) {
                        if (row) {
                            passwordHash = crypto.createHash('sha1').update(password + row.salt).digest("hex");
                            if (passwordHash === row.password) {

                                var user = {
                                    "id": row.id,
                                    "email": row.email
                                }
                                var token = jwt.sign(user, ' ', {
                                    expiresIn: 1440 * 60 * 30
                                });
                                res.json({
                                    status: true,
                                    token: token
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: 'Invalid password',
                                    field: "password"
                                });
                            }
                        } else {
                            res.json({
                                status: false,
                                message: 'User not found',
                                field: "username"
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            }
            connection.release();
        });
    })
router.post('/imageupload', upload.any(), function(req, res) {
    res.json({
        status: true,
        file: req.files[0]
    });
});
router.post('/imageuploadM', uploadChat.any(), function(req, res) {
    res.json({
        status: true,
        file: req.files[0]
    });
});
router.route('/forgotpsw')
    .post(function(req, res) {
        var email = req.body.email;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.queryRow('SELECT * FROM users WHERE (`email` = "' + email + '") AND (`type` <> 2)', function(err, user) {
                    if (!err) {
                        if (user) {
                            var salt = Math.random().toString(36).substr(0, 8),
                                _password = Math.random().toString(36).substr(0, 8)
                            password = crypto.createHash('md5').update(_password).digest("hex"),
                                passwordHash = crypto.createHash('sha1').update(password + salt).digest("hex");
                            connection.update(
                                'users', {
                                    password: passwordHash,
                                    salt: salt
                                }, {
                                    id: user.id
                                },
                                function(err, affectedRows) {
                                    if (!err) {
                                        user.password = _password;
                                        sendPwdReset({
                                                to: email
                                            }, user,
                                            function(err, info) {
                                                if (err) {
                                                    res.json({
                                                        status: false,
                                                        message: 'Mail Error',
                                                        field: 'mail'
                                                    });
                                                } else {
                                                    res.json({
                                                        status: true,
                                                        massage: 'Password reset sent'
                                                    });
                                                }

                                            });
                                    } else {
                                        res.json({
                                            status: false,
                                            message: 'Db error',
                                            field: 'db'
                                        });
                                    }
                                }
                            );
                        } else {
                            res.json({
                                status: false,
                                message: "No such user",
                                field: email
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                })
            } else {
                console.log(err);
            }
            connection.release();
        });
    });
router.route('/signUp')
    .post(function(req, res) {
        var user = req.body.user;
        var password = crypto.createHash('md5').update(user.password).digest("hex"),
            salt = Math.random().toString(36).substr(0, 8),
            passwordHash = crypto.createHash('sha1').update(password + salt).digest("hex");
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query('SELECT * FROM users WHERE email ="' + user.email + '"', function(err, results) {
                    if (err) {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    } else {
                        if (Object.keys(results).length == 0) {
                            connection.insert('users', {
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email,
                                password: passwordHash,
                                salt: salt,
                                status: '1',
                                type: '1',
                                gender: user.gender,
                                dob: user.dob,
                                phone: user.tel,
                                dial_code: user.telCode,
                                height: user.height,
                                avatar: user.img,
                                social_id: ''
                            }, function(err, results) {
                                if (!err) {
                                    res.json({
                                        status: true,
                                        countries: results
                                    });
                                } else {
                                    var errors = [];
                                    if (err.code == 'ER_DUP_ENTRY') {
                                        errors.push({
                                            status: false,
                                            message: 'This Username is already in use by another member. Please select another Username.',
                                            field: 'username'
                                        });
                                    }
                                    if (errors.length > 0) {
                                        res.json({
                                            status: false,
                                            errors: errors
                                        });
                                    } else {
                                        res.json({
                                            status: false,
                                            message: 'Db error',
                                            field: 'db'
                                        });
                                    }
                                }
                            });
                        } else {
                            var errors = [];
                            results.forEach(function(row) {
                                if (row.email.toUpperCase() == user.email.toUpperCase()) {
                                    errors.push({
                                        status: false,
                                        message: 'This Email is already in use by another member. Please select another Email.',
                                        field: 'email'
                                    })
                                }
                            })
                            if (errors.length > 0) {
                                res.json({
                                    status: false,
                                    errors: errors
                                });
                            }
                        }
                    }
                })
                return;
            }
            connection.release();
        });
    })
router.route('/authenticateSocial')
    .post(function(req, res) {
        var profile = req.body.profile;
        if (!profile) {
            res.json({
                status: false,
                message: 'Incorect data',
                field: 'user'
            });
            return 0;
        }
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                // select('users', '*', {
                //     email: profile.email,
                // }
                connection.query(
                    "SELECT * " +
                    "FROM users u " +
                    "WHERE " +
                    "u.social_id = '" + profile.id + "' AND u.social_id <> '' ",
                    function(err, results) {
                        if (err) {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        } else {
                            if (Object.keys(results).length === 0) {
                                var name = profile.name.split(' ')
                                connection.insert('users', {
                                    first_name: name[0],
                                    last_name: name[name.length - 1],
                                    email: profile.email,
                                    status: '1',
                                    type: '1',
                                    avatar: 'http://graph.facebook.com/' + profile.id + '/picture?type=large',
                                    social_id: profile.id
                                }, function(err, recordId) {
                                    if (err) {
                                        console.log(err);
                                        res.json({
                                            status: false,
                                            message: 'Db error',
                                            field: 'db'
                                        });
                                    } else {
                                        var user = {
                                            "id": recordId,
                                            "email": profile.email
                                        }
                                        var token = jwt.sign(user, ' ', {
                                            expiresIn: 1440 * 60 * 30
                                        });
                                        res.json({
                                            status: true,
                                            token: token
                                        });
                                    }
                                });
                            } else {
                                var user = {
                                    "id": results[0].id,
                                    "email": results[0].email
                                }
                                var token = jwt.sign(user, ' ', {
                                    expiresIn: 1440 * 60 * 30
                                });
                                res.json({
                                    status: true,
                                    token: token
                                });
                            }
                        }
                    })
            }
            connection.release();
        });
    })

router.use(function(req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, ' ', function(err, decoded) {
            if (err) {
                return res.json({
                    status: false,
                    data: {
                        message: 'Failed to authenticate token.'
                    }
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
});
router.get('/', function(req, res) {
    res.json({
        message: 'Gymventory API'
    });
});

router.get('/getUserData', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT   * from users WHERE `id` = "' + user_id + '"', function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getProfile', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT * from users WHERE `id` = "' + user_id + '"', function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.post('/updateProfile', function(req, res) {
    var user_id = req.decoded.id;

    var data = {
        email: req.body.email,
        first_name: req.body.firstname,
        last_name: req.body.lastname,
        phone: req.body.phone,
        address: req.body.address,
        latitude: req.body.coordinates.lat,
        longitude: req.body.coordinates.lng,
        gender: req.body.gender,
        dob: req.body.dayS + '-' + req.body.monS + '-' + req.body.yearS,
        dial_code: req.body.dial_code,
        phone: req.body.phone,
        avatar: req.body.avatar,
        height: req.body.h,
        avatar_type: req.body.avatar_type,
        wld: req.body.wld,
        education: req.body.education,
        hometown: req.body.hometown,
        lkf: req.body.lkf,
        ft: req.body.ft,
        jn: req.body.jn
    }
    if (req.body.password != '') {
        var password = crypto.createHash('md5').update(req.body.password).digest("hex"),
            salt = Math.random().toString(36).substr(0, 8),
            passwordHash = crypto.createHash('sha1').update(password + salt).digest("hex");
        data.password = passwordHash;
        data.salt = salt;
    }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('UPDATE users SET ? WHERE id = "' + user_id + '"', data, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json({
                            status: true,
                            message: 'UpdateProfile'
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});

router.route('/createClub')
    .post(function(req, res) {
        var user_id = req.decoded.id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.insert('clubs', {
                    name: req.body.club_name,
                    address: req.body.club_address,
                    phone: req.body.dial_code + req.body.phone,
                    email: req.body.email,
                    url: req.body.slug,
                    latitude: req.body.coordinates.lat,
                    longitude: req.body.coordinates.lng
                }, function(err, recordId) {
                    if (!err) {
                        connection.insert('clubs_to_user', {
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            phone: req.body.dial_code + req.body.phone,
                            email: req.body.email,
                            user_id: user_id,
                            club_id: recordId
                        }, function(err, results) {
                            if (!err) {
                                res.json({
                                    status: true,
                                    message: 'User club added'
                                });
                            } else {
                                console.log(err);
                                res.json({
                                    status: false,
                                    message: 'Db error',
                                    field: 'db'
                                });
                            }
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                        throw err;
                    }
                });
            }
            connection.release();
        });
    });
router.route('/requestOwnership')
    .post(function(req, res) {
        console.log(req.body)
        var user_id = req.decoded.id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.insert('clubs_to_user', {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    phone: req.body.dial_code + req.body.phone,
                    email: req.body.email,
                    user_id: user_id,
                    club_id: req.body.id
                }, function(err, results) {
                    if (!err) {
                        res.json({
                            status: true,
                            message: 'User club added'
                        });
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            }
            connection.release();
        });
    });

router.get('/getAdminUClubs', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT ctu.first_name, ctu.last_name, ctu.phone, ctu.email, c.name, c.fees, c.address, c.id from clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE c.status = 0 AND 1 <> ISNULL(ctu.first_name)", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/confirmAdminUClubs', function(req, res) {
    console.log(req.body);
    var club_id = req.body.id,
        data = {
            fees: req.body.fees,
            status: 1
        }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('UPDATE clubs SET ? WHERE id = "' + club_id + '"', data, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json({
                            status: true,
                            message: 'UpdateProfile'
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});

router.get('/uniqueNameClub/:c_name', function(req, res) {
    slug = getSlug(req.params.c_name);

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT c.id FROM clubs c WHERE c.url = '" + slug + "' LIMIT 1", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        if (rows.length > 0) {
                            res.json({
                                status: false,
                                url: 'isset',
                                slug: slug
                            });
                        } else {
                            res.json({
                                status: true,
                                url: 'no-isset',
                                slug: slug
                            });
                        }

                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.put('/addClubAdmin', function(req, res) {
    var user_id = req.decoded.id;
    var data = {
        name: req.body.name,
        address: req.body.address,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        type: req.body.type,
        website: req.body.website,
        email: req.body.email,
        phone: req.body.phone,
        url: req.body.slug,
        fax: req.body.fax,
        hours: JSON.stringify(req.body.day)
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('INSERT INTO clubs SET ?', data, function(err) {
                if (!err) {
                    res.json({
                        status: true,
                        message: 'Club added'
                    });
                } else {
                    console.log(err);
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.post('/addClubType', function(req, res) {
    var name = req.body.name,
        percent = req.body.percent;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("INSERT INTO `club_types` (`name`, `percent`) VALUES ('" + name + "', '" + percent + "');", function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json({
                            status: true,
                            message: 'Type added'
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});

router.get('/getClubTypes', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT * FROM `club_types` ORDER BY id DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json({
                            status: true,
                            rows
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});
router.post('/updateClubTypes', function(req, res) {
    var id = req.body.id,
        percent = req.body.percent,
        name = req.body.name;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("UPDATE `club_types` SET `percent` = '" + percent + "', `name` = '" + name + "' WHERE `club_types`.`id` = " + id, function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json({
                            status: true,
                            message: 'Type updated'
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});
router.post('/changeSocial', function(req, res) {
    var id = req.decoded.id,
        social_id = req.body.social_id
    email = req.body.email
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT * FROM `users` WHERE `users`.social_id = '" + social_id + "'", function(err, rows) {
                if (!err) {
                    if (!rows.length) {
                        connection.query("UPDATE `users` SET `social_id` = '" + social_id + "',`email` = '" + email + "' WHERE `users`.`id` = " + id, function(err, rows) {
                            console.log(err);
                            if (!err) {
                                res.json({
                                    status: true,
                                    message: 'social updated'
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: err.stack,
                                    field: 'db'
                                });
                            }
                        })
                    } else {
                        res.json({
                            status: false,
                            message: 'already used',
                            field: 'db'
                        });

                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});

router.post('/removeAdminUClubs', function(req, res) {
    var club_id = req.body.club
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('DELETE FROM clubs WHERE id = ' + club_id, function(err, result) {
                console.log(err);
                if (!err) {

                    if (result) {
                        res.json({
                            status: true,
                            message: 'UpdateProfile'
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: err.stack,
                        field: 'db'
                    });
                }
            });
        }
        connection.release();
    });
});

router.get('/countClubUser', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT count(*) AS count from clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE ctu.user_id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/checkClubUser', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT count(*) AS count from clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE `status` = 1 AND ctu.user_id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/clubData', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT * FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE c.status = 1 AND ctu.user_id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/clubAdminData/:id', function(req, res) {
    var user_id = req.decoded.id,
        club_id = req.params.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT * FROM clubs c WHERE c.id = ' + club_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/updateClubData', function(req, res) {
    var user_id = req.decoded.id;
    var data = {
        name: req.body.name,
        address: req.body.address,
        price: req.body.price,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        type: req.body.type,
        website: req.body.website,
        email: req.body.email,
        phone: req.body.phone,
        fax: req.body.fax,
        hours: JSON.stringify(req.body.day)
    }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('UPDATE clubs SET ? WHERE id = ' + req.body.id, data, function(err, row, fields) {
                if (!err) {
                    res.json({
                        status: true,
                        message: 'Update Club'
                    });
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/clubTypeHours', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT c.type, c.hours, c.id FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE ctu.user_id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.route('/createGym')
    .post(function(req, res) {
        var user_id = req.decoded.id,
            club_id = req.body.club_id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.insert('gyms', {
                    name: req.body.name,
                    type: req.body.type,
                    price: req.body.price,
                    price_for_member: req.body.price_for_member,
                    number_members: req.body.members,
                    min_members: req.body.min_members,
                    gym_type: req.body.add_gym_type,
                    gym_sport: req.body.gym_sport,
                    hours: ''

                }, function(err, recordId) {
                    if (!err) {
                        connection.insert('gyms_to_club', {
                            club_id: club_id,
                            gym_id: recordId
                        }, function(err, results) {
                            if (!err) {
                                res.json({
                                    status: true,
                                    message: 'gym added you club'
                                });
                            } else {
                                res.json({
                                    status: false,
                                    err,
                                    field: 'db2'
                                });
                            }
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            err,
                            field: 'db1'
                        });
                    }
                });
            }
            connection.release();
        });
    });

router.get('/getAllGyms/:type', function(req, res) {
    var user_id = req.decoded.id,
        type = req.params.type;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT g.* FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id LEFT JOIN gyms_to_club gtc ON ctu.club_id = gtc.club_id LEFT JOIN gyms g ON g.id = gtc.gym_id WHERE c.status = 1 AND ctu.user_id = " + user_id + " AND g.gym_type = '" + type + "' ORDER BY g.id DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.get('/getGym/:id', function(req, res) {
    var user_id = req.decoded.id,
        id = req.params.id;
    console.log('getGym - ', id);
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT g.* FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id LEFT JOIN gyms_to_club gtc ON ctu.club_id = gtc.club_id LEFT JOIN gyms g ON g.id = gtc.gym_id WHERE c.status = 1 AND ctu.user_id = " + user_id + " AND g.id = '" + id + "' ORDER BY g.id DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});


router.post('/updateGymData', function(req, res) {
    var data = {
        name: req.body.name,
        type: req.body.type,
        price: req.body.price,
        price_for_member: req.body.price_for_member,
        number_members: req.body.number_members,
        min_members: req.body.min_members,
        gym_type: req.body.gym_type,
        gym_sport: req.body.gym_sport,
        hours: ''
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('UPDATE gyms SET ? WHERE id = ' + req.body.id, data, function(err, row, fields) {
                if (!err) {
                    res.json({
                        status: true,
                        message: 'Updated Gym'
                    });
                } else {
                    console.log(err)
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        } else {
            throw err;
        }
    });
});

router.get('/getProfileAddress', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT u.address FROM users u WHERE u.id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/clubFilter', function(req, res) {
    var user_id = req.decoded.id;
    var club_day = '',
        club_filter_sql = '',
        club_latitude_sql = '',
        club_longitude_sql = '';
    club_distance_s = '';
    club_distance_h = '';
    club_distance_ob = '';

    // if (req.body.day != '') {
    //     club_day = req.body.day;
    //     if (typeof req.body.start_time == "number") {
    //         club_filter_sql += ' AND c.hours->"$.' + club_day + '.start" <= ' + req.body.start_time;
    //     }
    //     if (typeof req.body.end_time == "number") {
    //         club_filter_sql += ' AND c.hours->"$.' + club_day + '.end" >= ' + req.body.end_time;
    //     }
    // }
    if (req.body.latitude != '' && req.body.longitude != '' && req.body.distance != '') {
        club_distance_s = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';
        club_distance_h = ' AND CEILING(distance) <= ' + req.body.distance;
        club_distance_ob = ' distance ASC';
    }

    if (typeof req.body.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball', 'Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                club_filter_sql += " AND '" + sports_types[sport_item - 1] + "' IN (SELECT g.gym_sport FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id)";
            }
        }
    }

    if (req.body.business_name != '') {
        club_filter_sql += " AND c.name LIKE '%" + req.body.business_name + "%'";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            q = connection.query("SELECT c.id, c.price, c.type, c.latitude, c.longitude, c.name, EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id) as favorite, c.address, c.hours, c.website, ctu.email, ctu.phone," +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id AND cm.user_id = " + user_id + ") as me_member, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members " + club_distance_s + ", (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE  1 = 1 " + club_filter_sql + " HAVING 1=1 " + club_distance_h + " ORDER BY " + club_distance_ob + "",
                function(err, rows, fields) {
                    if (!err) {
                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
                console.log(q.sql);
            connection.release();
        }
    });
});
router.post('/clublast-visitedA', function(req, res) {
    var user_id = req.decoded.id;
    var club_day = '',
        club_filter_sql = '',
        club_latitude_sql = '',
        club_longitude_sql = '';
    club_distance_s = '';
    club_distance_h = '';
    club_distance_ob = '';


    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT c.id, c.type, c.latitude, c.longitude, EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id) as favorite,c.name, c.address, c.hours, c.website, ctu.email, ctu.phone," +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id AND cm.user_id = " + user_id + ") as me_member, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members, JSON_SEARCH(JSON_KEYS(c.hours->\"$\"), 'one', '" + club_day + "') as fday " + club_distance_s + ", (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM club_visite_history cvh LEFT JOIN clubs c ON cvh.club_id = c.id LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE c.status = 1 AND cvh.user_id = '" + user_id +
                "' AND NOT EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id)" +
                " GROUP BY cvh.club_id ORDER BY cvh.date DESC ",
                function(err, rows, fields) {
                    if (!err) {
                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});
router.route('/club-favorite')
    .get(function(req, res) {
        var user_id = req.decoded.id,
            club_day = '',
            club_filter_sql = '',
            club_latitude_sql = '',
            club_longitude_sql = '';
        club_distance_s = '';
        club_distance_h = '';
        club_distance_ob = '';


        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.query("SELECT c.id, c.type, c.latitude, c.longitude, c.name, EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id) as favorite, c.address, c.hours, c.website, ctu.email, ctu.phone," +
                    " (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id AND cm.user_id = " + user_id + ") as me_member, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members, JSON_SEARCH(JSON_KEYS(c.hours->\"$\"), 'one', '" + club_day + "') as fday " + club_distance_s + ", (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM club_favorite cf LEFT JOIN clubs c ON cf.club_id = c.id LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE c.status = 1 AND cf.user_id = '" + user_id + "' GROUP BY cf.club_id ORDER BY cf.date DESC ",
                    function(err, rows, fields) {
                        if (!err) {
                            res.json({
                                status: true,
                                rows
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    });
                connection.release();
            }
        });
    })
    .post(function(req, res) {
        var user_id = req.decoded.id,
            club_id = req.body.club_id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.query("INSERT INTO club_favorite " +
                    "  SELECT null as id, " + club_id + " as club_id, " + user_id + " as user_id, CURRENT_TIMESTAMP() as date " +
                    "  FROM club_favorite " +
                    "  WHERE (user_id=" + user_id + ")  " +
                    "  HAVING COUNT(*) < 5 AND NOT EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id = " + user_id + " AND cf.club_id = " + club_id + "); ",
                    function(err, rows) {
                        if (!err) {
                            console.log(rows);
                            res.json({
                                status: true,
                                rows
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    });
                connection.release();
            }
        });
    })
    .delete(function(req, res) {
        var user_id = req.decoded.id,
            club_id = req.query.club_id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.query("DELETE FROM club_favorite  " +
                    " WHERE user_id = " + user_id + " AND club_id = " + club_id + " ",
                    function(err, rows) {
                        if (!err) {
                            console.log(rows);
                            res.json({
                                status: true,
                                rows
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    });
                connection.release();
            }
        });
    })
router.post('/addClubMember', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.insert('clubs_members', {
                club_id: req.body.club_id,
                user_id: user_id,
            }, function(err, results) {
                if (!err) {
                    res.json({
                        status: true,
                        message: 'Member club added'
                    });
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/deleteClubMember', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query('DELETE FROM clubs_members WHERE user_id = ' + user_id + ' AND club_id = ' + req.body.club_id, function(err, results) {
                if (!err) {
                    res.json({
                        status: true,
                        message: 'Member club deleted'
                    });
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getAdminListClubs', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT c.id, c.latitude, c.longitude, c.phone, c.name, c.address, c.hours, c.website, ctu.email, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members, (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id ORDER BY c.id DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getAdminRegUsers', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT u.* FROM users u WHERE u.status <> 0", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getAdminRegUsers', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT u.* FROM users u WHERE u.status <> 0", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/bookingGames', function(req, res) {
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '1=1',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = '',
    club_id_search = '';
    if (typeof req.body.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
    }
    if (!!req.body.club_id) {
        club_id_search = "AND c.id = "+ req.body.club_id
    }
    console.log(req.body.club_id);
    console.log(club_id_search);
    if (req.body.access != '') {
        var gym_type = '';
        switch (req.body.access) {
            case 'All':
            {
                gym_type = " AND (g.type = 'public' OR c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                break;
            }
            case 'Public':
            {
                gym_type += " AND g.type = 'public'";
                break;
            }
            case 'Private':
            {
                gym_type = " AND (g.type = 'private' AND c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                break;
            }
        }
    }

    if (req.body.gender != '') {
        var game_gender = '';
        switch (req.body.gender) {
            case 'Male':
            {
                game_gender = " AND gm.gender = 'Male'";
                break;
            }
            case 'Female':
            {
                game_gender = " AND gm.gender = 'Female'";
                break;
            }
        }
    }
    if (typeof req.body.age == 'object') {
        var game_age = " AND (gm.age_start >= " + req.body.age.min + " AND gm.age_end <= " + req.body.age.max + ")";
    }

    if (req.body.dt && req.body.dt != '' && req.body.dt != null) {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        if (typeof req.body.start_time == "number" && typeof req.body.end_time == "number") {
            game_filter_where += " ";
        }
    }


    if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
        game_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';
        game_distance_heving = ' CEILING(distance) <= ' + req.body.distance + ' ';
    }


    game_orderby = ' gm.date ASC, gm.start_time DESC';

    var ifdate = ' ';
    var ifdate2 = ' ';
    if (typeof date != 'undefined') {
        ifdate = "AND  gm.date = '" + date + "'";
        ifdate2 = " gm_s.date = '" + date + "' AND ";
    }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            q = connection.query(
                "SELECT gm.must_start, c.address,c.name as club_name, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.price_member, gm.price_member, gm.start_time, gm.end_time, (SELECT COUNT(*) as count FROM games gm_s WHERE " + ifdate2 + " ((gm_s.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " >= gm_s.start_time AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.end_time)) AND gm_s.gym_id = g.id) as game," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar,'id',u.id, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                ", (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, " +
                "(SELECT SUM(gms.members) FROM games_members gms WHERE gms.game_id = gm.id) AS g_member" +
                " FROM clubs c" +
                " LEFT JOIN gyms_to_club gtc ON c.id = gtc.club_id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON g.id = gm.gym_id " + ifdate + " AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time)) AND gm.status = 1" +
                " WHERE gm.date <> '' " + gym_sport + gym_type + game_filter_where + club_id_search +
                " HAVING " + game_distance_heving + " AND g_member IS NOT NULL" +
                " ORDER BY " + game_orderby + ' LIMIT ' + req.body.limit_start + ', 10000',
                function(err, rows, fields) {
                    if (!err) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);

                            global_count += rows[index].members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (rows[index].members_avatar) {
                                rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img,
                                            'id': a_e.id
                                        })
                                    }
                                })
                                rows[index].members_avatar = tmp_arr;
                            }
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            rows[index].global_members = count;
                            rows[index].members_range = range.range(0, rows[index].number_members);

                        })

                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});


router.post('/findGames', function(req, res) {
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '1=1',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = '',
        club_id_search = '';
    if (typeof req.body.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND (g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
        if (gym_sport !== '') {
            gym_sport += ')';
        }
    }
    if (req.body.club_id) {
      club_id_search = "AND c.id = "+ req.body.club_id
    }
    console.log(req.body.club_id);
    console.log(club_id_search);
    if (req.body.access != '') {
        var gym_type = '';
        switch (req.body.access) {
            case 'All':
                {
                    gym_type = " AND (g.type = 'public' OR c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                    break;
                }
            case 'Public':
                {
                    gym_type += " AND g.type = 'public'";
                    break;
                }
            case 'Private':
                {
                    gym_type = " AND (g.type = 'private' AND c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                    break;
                }
        }
    }

    if (req.body.gender != '') {
        var game_gender = '';
        switch (req.body.gender) {
            case 'Male':
                {
                    game_gender = " AND gm.gender = 'Male'";
                    break;
                }
            case 'Female':
                {
                    game_gender = " AND gm.gender = 'Female'";
                    break;
                }
        }
    }
    if (typeof req.body.age == 'object') {
        var game_age = " AND (gm.age_start >= " + req.body.age.min + " AND gm.age_end <= " + req.body.age.max + ")";
    }

    if (req.body.dt && req.body.dt != '' && req.body.dt != null) {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        if (typeof req.body.start_time == "number" && typeof req.body.end_time == "number") {
            game_filter_where += " ";
        }
    }


    if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
        game_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';
        game_distance_heving = ' CEILING(distance) <= ' + req.body.distance + ' ';
    }

    if (req.body.sort != '') {
        switch (req.body.sort) {
            case 'Nearest':
                {
                    if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
                        game_orderby = ' distance ASC, gm.date ASC, gm.start_time ASC';
                    }else {
                      game_orderby = ' gm.date ASC, gm.start_time ASC';
                    }

                    break;
                }
            case 'Soonest':
                {
                  if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
                      game_orderby = "gm.date ASC, gm.start_time ASC, distance ASC";
                  }else {
                      game_orderby = " gm.date ASC, gm.start_time ASC";
                  }
                    break;
                }
        }
    }
    var ifdate = ' ';
    var ifdate2 = ' ';
    if (typeof date != 'undefined') {
        ifdate = "AND  gm.date = '" + date + "'";
        ifdate2 = " gm_s.date = '" + date + "' AND ";
    }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            q = connection.query(
                "SELECT c.address,c.name as club_name, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.price_member, gm.price_member, gm.start_time, gm.end_time, IF(ISNULL(gm.members), 0, gm.members) AS members, (SELECT COUNT(*) as count FROM games gm_s WHERE " + ifdate2 + " ((gm_s.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " >= gm_s.start_time AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.end_time)) AND gm_s.gym_id = g.id) as game," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar,'id',u.id, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                ", (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, (SELECT COUNT(*) FROM game_watch gw2 WHERE gw2.game_id = gm.id AND gw2.user_id = " + user_id + " ) AS my_watch" +
                " FROM clubs c" +
                " LEFT JOIN gyms_to_club gtc ON c.id = gtc.club_id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON g.id = gm.gym_id " + ifdate + " AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time)) AND gm.status = 1" +
                " WHERE g.gym_type = 'Gym' AND gm.status = 1 AND gm.date <> '' " + gym_sport + gym_type + game_filter_where + club_id_search +
                " HAVING " + game_distance_heving +
                " ORDER BY game DESC, " + game_orderby + ' LIMIT ' + req.body.limit_start + ', 10000',
                function(err, rows, fields) {
                    if (!err) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);

                            global_count += rows[index].members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (rows[index].members_avatar) {
                                rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img,
                                            'id': a_e.id
                                        })
                                    }
                                })
                                rows[index].members_avatar = tmp_arr;
                            }
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            rows[index].global_members = count;
                            rows[index].members_range = range.range(0, rows[index].number_members);

                        })

                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/findClass', function(req, res) {
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = '',
        club_id_search = '';

    if (typeof req.body.sport == 'object') {
        var sports_types = ['Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND (g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
        if (gym_sport !== '') {
            gym_sport += ')';
        }
    }
    if (!!req.body.club_id) {
        club_id_search = "AND c.id = "+ req.body.club_id
    }
    console.log(req.body.club_id);
    console.log(club_id_search);
    if (req.body.access != '') {
        var gym_type = '';
        switch (req.body.access) {
            case 'All':
            {
                gym_type = " AND (g.type = 'public' OR c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                break;
            }
            case 'Public':
            {
                gym_type += " AND g.type = 'public'";
                break;
            }
            case 'Private':
            {
                gym_type = " AND (g.type = 'private' AND c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                break;
            }
        }
    }

    if (req.body.gender != '') {
        var game_gender = '';
        switch (req.body.gender) {
            case 'Male':
            {
                game_gender = " AND gm.gender = 'Male'";
                break;
            }
            case 'Female':
            {
                game_gender = " AND gm.gender = 'Female'";
                break;
            }
        }
    }
    if (typeof req.body.age == 'object') {
        var game_age = " AND (gm.age_start >= " + req.body.age.min + " AND gm.age_end <= " + req.body.age.max + ")";
    }

    if (req.body.dt && req.body.dt != '' && req.body.dt != null) {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        if (typeof req.body.start_time == "number" && typeof req.body.end_time == "number") {
            game_filter_where += " ";
        }
    }


    if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
        game_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';
        game_distance_heving = ' CEILING(distance) <= ' + req.body.distance + ' ';
    }

    if (req.body.sort != '') {
        switch (req.body.sort) {
            case 'Nearest':
            {
                if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
                    game_orderby = ' distance ASC, gm.date ASC, gm.start_time ASC';
                }else {
                    game_orderby = ' gm.date ASC, gm.start_time ASC';
                }

                break;
            }
            case 'Soonest':
            {
                if (!!req.body.latitude && !!req.body.longitude && !!req.body.distance != '') {
                    game_orderby = "gm.date ASC, gm.start_time ASC, distance ASC";
                }else {
                    game_orderby = " gm.date ASC, gm.start_time ASC";
                }
                break;
            }
        }
    }
    var ifdate = ' ';
    var ifdate2 = ' ';
    if (typeof date != 'undefined') {
        ifdate = "AND  gm.date = '" + date + "'";
        ifdate2 = " gm_s.date = '" + date + "' AND ";
    }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            q = connection.query(
                "SELECT c.address,c.name as club_name, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.price_member, gm.price_member, gm.start_time, gm.end_time, IF(ISNULL(gm.members), 0, gm.members) AS members, (SELECT COUNT(*) as count FROM games gm_s WHERE " + ifdate2 + " ((gm_s.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " >= gm_s.start_time AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.end_time)) AND gm_s.gym_id = g.id) as game," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar,'id',u.id, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                ", (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, (SELECT COUNT(*) FROM game_watch gw2 WHERE gw2.game_id = gm.id AND gw2.user_id = " + user_id + " ) AS my_watch" +
                " FROM clubs c" +
                " LEFT JOIN gyms_to_club gtc ON c.id = gtc.club_id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON g.id = gm.gym_id " + ifdate + " AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time)) AND gm.status = 1" +
                " WHERE g.gym_type = 'Class' AND gm.status = 1 AND gm.date <> '' " + gym_sport + gym_type + game_filter_where + club_id_search +
                " HAVING " + game_distance_heving +
                " ORDER BY game DESC, " + game_orderby + ' LIMIT ' + req.body.limit_start + ', 10000',
                function(err, rows, fields) {
                    if (!err) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);

                            global_count += rows[index].members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (rows[index].members_avatar) {
                                rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img,
                                            'id': a_e.id
                                        })
                                    }
                                })
                                rows[index].members_avatar = tmp_arr;
                            }
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            rows[index].global_members = count;
                            rows[index].members_range = range.range(0, rows[index].number_members);

                        })

                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/findAfterGames', function(req, res) {
    console.log(req.body);
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = ''

    if (req.body.dt && req.body.dt != '') {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        if (typeof req.body.start_time == "number" && typeof req.body.end_time == "number") {
            game_filter_where += " ";
        }
    }


    if (req.body.latitude != '' && req.body.longitude != '' && req.body.distance != '') {
        game_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';
        game_distance_heving = ' CEILING(distance) <= ' + req.body.distance;
    }

    if (req.body.sort != '') {
        switch (req.body.sort) {
            case 'Soonest':
                {
                    game_orderby = ' distance ASC, gm.start_time ASC';
                    break;
                }
            case 'Nearest':
                {
                    game_orderby = " gm.start_time ASC, distance ASC";
                    break;
                }
        }
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query(
                "SELECT (SELECT COUNT(*) FROM clubs_members cm_t WHERE cm_t.user_id = " + user_id + " AND cm_t.club_id = c.id) AS clubMember, c.address,c.name as club_name, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.price_member, gm.price_member, gm.start_time, gm.end_time, IF(ISNULL(gm.members), 0, gm.members) AS members, (SELECT COUNT(*) as count FROM games gm_s WHERE gm_s.date > '" + date + "' AND ((gm_s.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.end_time) OR (" + req.body.start_time + " <= gm_s.start_time AND " + req.body.end_time + " >= gm_s.start_time AND " + req.body.end_time + " <= gm_s.end_time) OR (" + req.body.start_time + " >= gm_s.start_time AND " + req.body.start_time + " <= gm_s.end_time AND " + req.body.end_time + " >= gm_s.end_time)) AND gm_s.gym_id = g.id) as game," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar,'id',u.id, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                ", (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching" +
                " FROM clubs c" +
                " LEFT JOIN gyms_to_club gtc ON c.id = gtc.club_id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON g.id = gm.gym_id AND  gm.date > '" + date + "' AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time)) AND gm.status = 1" +
                " WHERE g.gym_type = '" + req.body.type + "' " + game_filter_where +
                " HAVING game > 0 AND " + game_distance_heving +
                " ORDER BY gm.date ASC, " + game_orderby + ' LIMIT ' + req.body.limit_start + ', ' + req.body.limit_end,
                function(err, rows, fields) {
                    if (!err) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);

                            global_count += rows[index].members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (rows[index].members_avatar) {
                                rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img
                                        })
                                    }
                                })
                                rows[index].members_avatar = tmp_arr;
                            }
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            rows[index].global_members = count;
                            rows[index].members_range = range.range(0, rows[index].number_members);

                        })

                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/buyCredits', function(req, res) {
    var user_id = req.decoded.id;
    var create_payment_json = '';
    var total = (parseFloat(req.body.credits) + parseFloat(req.body.credits * 0.03)).toFixed(2);

    var credit = parseFloat(total / req.body.credits).toFixed(2);
    var credit_total = (credit * req.body.credits).toFixed(2);

    switch (req.body.payment_type) {
        case 'paypal':
            {
                var create_payment_json = {
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"
                    },
                    "redirect_urls": {
                        "return_url": req.headers.origin + "/paypal?paypal_url=success",
                        "cancel_url": req.headers.origin + "/paypal?paypal_url=cancel"
                    },
                    "transactions": [{
                        "item_list": {
                            "items": [{
                                "name": "Credits",
                                "sku": "credit",
                                "price": credit,
                                "currency": "USD",
                                "quantity": req.body.credits
                            }]
                        },
                        "amount": {
                            "currency": "USD",
                            "total": credit_total
                        },
                        "description": "This is the payment description."
                    }]
                };
                break;
            }
        case 'credit_card':
            {
                var expire_month_year = req.body.expire_month_year.split('/');
                var month = expire_month_year[0];
                var year = expire_month_year[1];
                create_payment_json = {
                    "intent": "sale",
                    "payer": {
                        "payment_method": "credit_card",
                        "funding_instruments": [{
                            "credit_card": {
                                "type": req.body.type, // mastercard
                                "number": req.body.number,
                                "expire_month": month,
                                "expire_year": year,
                                "cvv2": req.body.cvv2,
                                "first_name": req.body.first_name,
                                "last_name": req.body.last_name
                            }
                        }]
                    },
                    "transactions": [{
                        "amount": {
                            "total": credit_total,
                            "currency": "USD"
                        },
                        "description": "This is the payment transaction description."
                    }]
                };
                break;
            }
    }

    paypal.payment.create(create_payment_json, function(error, payment) {
        if (error) {
            res.json({
                status: false,
                error
            });
        } else {
            if (payment.state == 'approved') {
                var date = new Date(), // 'yyyy-mm-dd'
                    currentDate = formatDate(date);
                pool.getConnection(function(err, connection) {
                    if (!err) {
                        mysqlUtilities.upgrade(connection);
                        mysqlUtilities.introspection(connection);
                        connection.queryRow('SELECT balance FROM user_credits WHERE user_id ="' + user_id + '"', function(err, results) {
                            if (err) {
                                res.json({
                                    status: false,
                                    message: 'Db error',
                                    field: 'db'
                                });
                            } else {
                                if (!results) {
                                    connection.beginTransaction(function(err) {
                                        if (err) {
                                            throw err;
                                        }
                                        connection.insert('user_credits', {
                                            user_id: user_id,
                                            balance: req.body.credits
                                        }, function(err, result) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }

                                            connection.insert('user_transactions', {
                                                user_id: user_id,
                                                date: currentDate,
                                                credits: req.body.credits,
                                                card: JSON.stringify(payment.payer.funding_instruments[0].credit_card),
                                                payment_id: payment.id
                                            }, function(err, result) {
                                                if (err) {
                                                    return connection.rollback(function() {
                                                        throw err;
                                                    });
                                                }
                                                connection.commit(function(err) {
                                                    if (err) {
                                                        return connection.rollback(function() {
                                                            throw err;
                                                        });
                                                    }
                                                    console.log('success!');
                                                    res.json({
                                                        status: true,
                                                        credits: req.body.credits,
                                                        payment
                                                    });
                                                });
                                            });
                                        });
                                    });
                                } else if (results.balance > 0) {
                                    var credit_count = parseInt(results.balance) + parseInt(req.body.credits);
                                    connection.beginTransaction(function(err) {
                                        if (err) {
                                            throw err;
                                        }
                                        connection.update('user_credits', {
                                            balance: credit_count
                                        }, {
                                            user_id: user_id
                                        }, function(err, result) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }

                                            connection.insert('user_transactions', {
                                                user_id: user_id,
                                                date: currentDate,
                                                credits: req.body.credits,
                                                card: JSON.stringify(payment.payer.funding_instruments[0].credit_card),
                                                payment_id: payment.id
                                            }, function(err, result) {
                                                if (err) {
                                                    return connection.rollback(function() {
                                                        throw err;
                                                    });
                                                }
                                                connection.commit(function(err) {
                                                    if (err) {
                                                        return connection.rollback(function() {
                                                            throw err;
                                                        });
                                                    }
                                                    console.log('success!');
                                                    res.json({
                                                        status: true,
                                                        credits: credit_count,
                                                        payment
                                                    });
                                                });
                                            });
                                        });
                                    });
                                }

                            }
                        })
                        return;
                    }
                    connection.release();
                });
            } else if (payment.state == 'created') {
                res.json({
                    status: true,
                    payment: payment
                });
            }
        }
    });
});


router.post('/paymentGame', function(req, res) {
    var user_id = req.decoded.id;
    var create_payment_json = '';
    var total = ((parseFloat(req.body.credits) + parseFloat((req.body.credits) * req.body.member) * 0.03)).toFixed(2);

    var credit = parseFloat(total / req.body.credits).toFixed(2);
    var credit_total = (credit * req.body.credits).toFixed(2);

    var percent = parseFloat((req.body.game_cost * req.body.fees) / 100).toFixed(2);

    switch (req.body.payment_type) {
        case 'paypal':
        {
            var create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": req.headers.origin + "/paypal?paypal_url=game_success&game_id=" + req.body.game_id + "&date=" + req.body.date + "&end_time=" + req.body.end_time + "&start_time=" + req.body.start_time + "&join=" + req.body.join + "&fees=" + req.body.fees + "&gym_id=" + req.body.gym_id + "&reserve=" + req.body.reserve + "&members=" + ((req.body.member) ? req.body.member : 1) + "&credits=" + req.body.game_cost + "&percent=" + percent + "&action=profit&total=" + total + "&balance=" + req.body.balance + "&club_user=" + req.body.club_user,
                    "cancel_url": req.headers.referer + "?paypal_url=cancel"
                },
                "transactions": [{
                    "item_list": {
                        "items": [{
                            "name": "Games",
                            "sku": req.body.member + " place(s)",
                            "price": total,
                            "currency": "USD",
                            "quantity": 1
                        }]
                    },
                    "amount": {
                        "currency": "USD",
                        "total": total
                    },
                    "description": "This is the payment description."
                }]
            };
            break;
        }
        case 'credit_card':
        {
            var expire_month_year = req.body.expire_month_year.split('/');
            var month = expire_month_year[0];
            var year = expire_month_year[1];
            create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "credit_card",
                    "funding_instruments": [{
                        "credit_card": {
                            "type": req.body.type, // mastercard
                            "number": req.body.number,
                            "expire_month": month,
                            "expire_year": year,
                            "cvv2": req.body.cvv2,
                            "first_name": req.body.first_name,
                            "last_name": req.body.last_name
                        }
                    }]
                },
                "transactions": [{
                    "amount": {
                        "total": total,
                        "currency": "USD"
                    },
                    "description": "This is the payment transaction description."
                }]
            };
            break;
        }
    }

    paypal.payment.create(create_payment_json, function(error, payment) {
        if (error) {
            res.json({
                status: false,
                error
            });
        } else {
            var date = new Date(), // 'yyyy-mm-dd'
                currentDate = formatDate(date);

            var t_data = {
                user_id: user_id,
                date: currentDate,
                credits: (req.body.credits - percent).toFixed(2),
                card: '',
                payment_id: payment.id,
                fees: 0.03,
                action: 'purchase'
            }

            console.log(t_data);

            if (payment.state == 'approved') {
                t_data.card = JSON.stringify(payment.payer.funding_instruments[0].credit_card);
                pool.getConnection(function(err, connection) {
                    if (!err) {
                        mysqlUtilities.upgrade(connection);
                        mysqlUtilities.introspection(connection);
                        connection.beginTransaction(function(err) {
                            if (err) {
                                throw err;
                            }

                            if (req.body.invites) {
                                req.body.invites.forEach(function(el, ind, arr) {
                                    connection.insert('game_invites', {
                                        user_id: el.id,
                                        game_id: req.body.game_id

                                    }, function(err, result) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                    });
                                })
                            }

                            connection.insert('teammates', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                gym_id: req.body.gym_id

                            }, function(err, result) {
                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }

                                console.log('games_members ************************* ', {
                                    user_id: user_id,
                                    game_id: req.body.game_id,
                                    payment: req.body.join,
                                    reserve: req.body.reserve,
                                    members: (req.body.member) ? req.body.member : 1,

                                });

                                connection.insert('games_members', {
                                    user_id: user_id,
                                    game_id: req.body.game_id,
                                    payment: req.body.join,
                                    reserve: req.body.reserve,
                                    members: (req.body.member) ? req.body.member : 1,

                                }, function(err, result) {
                                    console.log('games_members result *************** ', result);
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    t_data.credits = req.body.credits - percent;
                                    t_data.fees = req.body.fees;
                                    t_data.action = 'profit';

                                    connection.commit(function(err) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                        change_balance(t_data.credits - percent, req.body.club_user, t_data, function(data) {
                                            console.log("data ", data);
                                            if (data) {
                                                CreateGameTask('game_start', req.body.gym_id, req.body.game_id, req.body.start_time, req.body.end_time, formatDate(req.body.date));
                                                res.json({
                                                    status: true,
                                                    payment: payment
                                                });
                                            }
                                        });

                                    });

                                });
                            });

                        })
                    }
                    connection.release();
                });
            } else if (payment.state == 'created') {
                res.json({
                    status: true,
                    payment: payment
                });
            }
        }
    });
});

router.post('/successPayment', function(req, res) {
    var user_id = req.decoded.id;
    console.log('successPayment', req.body);

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            var date = new Date(),
                currentDate = formatDate(date);
            if (req.body.type == 'pay_credit') {
                connection.queryRow('SELECT balance FROM user_credits WHERE user_id ="' + user_id + '"', function(err, results) {
                    if (err) {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    } else {
                        if (!results) {
                            connection.beginTransaction(function(err) {
                                if (err) {
                                    throw err;
                                }
                                connection.insert('user_credits', {
                                    user_id: user_id,
                                    balance: req.body.credits
                                }, function(err, result) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }

                                    connection.insert('user_transactions', {
                                        user_id: user_id,
                                        date: currentDate,
                                        credits: req.body.credits,
                                        card: '',
                                        payment_id: req.body.payment_id
                                    }, function(err, result) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                        connection.commit(function(err) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }
                                            console.log('success!');
                                            res.json({
                                                status: true,
                                                credits: req.body.credits
                                            });
                                        });
                                    });
                                });
                            });
                        } else if (results.balance > 0) {
                            var credit_count = parseInt(results.balance) + parseInt(req.body.credits);
                            connection.beginTransaction(function(err) {
                                if (err) {
                                    throw err;
                                }
                                connection.update('user_credits', {
                                    balance: credit_count
                                }, {
                                    user_id: user_id
                                }, function(err, result) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }

                                    connection.insert('user_transactions', {
                                        user_id: user_id,
                                        date: currentDate,
                                        credits: req.body.credits,
                                        card: '',
                                        payment_id: req.body.payment_id
                                    }, function(err, result) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                        connection.commit(function(err) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }
                                            console.log('success!');
                                            res.json({
                                                status: true,
                                                credits: credit_count
                                            });
                                        });
                                    });
                                });
                            });
                        }

                    }
                })
            } else if (req.body.type == 'pay_game') {
                pool.getConnection(function(err, connection) {
                    if (!err) {
                        mysqlUtilities.upgrade(connection);
                        mysqlUtilities.introspection(connection);
                        connection.beginTransaction(function(err) {
                            if (err) {
                                throw err;
                            }

                            if (req.body.invites) {
                                req.body.invites.forEach(function(el, ind, arr) {
                                    connection.insert('game_invites', {
                                        user_id: el.id,
                                        game_id: req.body.game_id

                                    }, function(err, result) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                    });
                                })
                            }

                            console.log('teammates ************************* ', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                gym_id: req.body.gym_id
                            });


                            connection.insert('teammates', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                gym_id: req.body.gym_id

                            }, function(err, result) {
                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }
                            });

                            var t_data = {
                                user_id: user_id,
                                date: currentDate,
                                credits: req.body.credits - req.body.percent,
                                card: '',
                                payment_id: req.body.paymentId,
                                fees: 0.03,
                                action: 'purchase'
                            }

                            console.log('games_members ************************* ', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                payment: req.body.join,
                                reserve: req.body.reserve,
                                members: (req.body.members) ? req.body.members : 1,

                            });

                            connection.insert('games_members', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                payment: req.body.join,
                                reserve: req.body.reserve,
                                members: (req.body.members) ? req.body.members : 1,

                            }, function(err, result) {

                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });

                                }else{
                                    console.log('games_members result *************** ', result);
                                }

                                connection.commit(function(err) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    t_data.credits = req.body.credits - req.body.percent;
                                    t_data.fees = req.body.percent;
                                    t_data.action = 'profit';

                                    console.log('change_balance', t_data.credits - req.body.percent, req.body.club_user, t_data);
                                    change_balance(t_data.credits - req.body.percent, req.body.club_user, t_data, function(data) {
                                        console.log('-------------------------------------------------------')
                                        if (data) {
                                            console.log('***************************************************')
                                            CreateGameTask('game_start', req.body.gym_id, req.body.game_id, req.body.start_time, req.body.end_time, formatDate(req.body.date));
                                            res.json({
                                                status: true,
                                                credits: 0
                                            });
                                        }
                                    });

                                });

                            });
                        })
                    }
                    connection.release();
                });
            }
        }
        connection.release();
    });
});
router.post('/paymentClub', function(req, res) {
    var user_id = req.decoded.id;
    var create_payment_json = '';

    var total = (parseFloat(req.body.credits) + parseFloat(req.body.credits) * 0.03).toFixed(2);

    switch (req.body.payment_type) {
        case 'paypal':
        {
            var create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": req.headers.origin + "/paypal?paypal_url=club_success&club_id="+req.body.club,
                    "cancel_url": req.headers.referer + "?paypal_url=cancel"
                },
                "transactions": [{
                    "item_list": {
                        "items": [{
                            "name": req.body.name,
                            "sku": "Payment entrance to the club",
                            "price": total,
                            "currency": "USD",
                            "quantity": 1
                        }]
                    },
                    "amount": {
                        "currency": "USD",
                        "total": total
                    },
                    "description": "Payment entrance to the club."
                }]
            };
            break;
        }
        case 'credit_card':
        {
            var expire_month_year = req.body.expire_month_year.split('/');
            var month = expire_month_year[0];
            var year = expire_month_year[1];
            create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "credit_card",
                    "funding_instruments": [{
                        "credit_card": {
                            "type": req.body.type, // mastercard
                            "number": req.body.number,
                            "expire_month": month,
                            "expire_year": year,
                            "cvv2": req.body.cvv2,
                            "first_name": req.body.first_name,
                            "last_name": req.body.last_name
                        }
                    }]
                },
                "transactions": [{
                    "amount": {
                        "total": total,
                        "currency": "USD"
                    },
                    "description": "Payment entrance to the club."
                }]
            };
            break;
        }
    }

    paypal.payment.create(create_payment_json, function(error, payment) {
        if (error) {
            res.json({
                status: false,
                error
            });
        } else {
            var date = new Date(), // 'yyyy-mm-dd'
                currentDate = formatDate(date);

            if (payment.state == 'approved') {
                pool.getConnection(function(err, connection) {
                    if (!err) {
                        mysqlUtilities.upgrade(connection);
                        mysqlUtilities.introspection(connection);
                        connection.insert('clubs_members', {
                            club_id: req.body.club,
                            user_id: user_id,
                        }, function(err, results) {
                            if (!err) {
                                res.json({
                                    status: true,
                                    payment: payment
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: 'Db error',
                                    field: 'db'
                                });
                            }
                        });
                        connection.release();
                    }
                });
            } else if (payment.state == 'created') {
                res.json({
                    status: true,
                    payment: payment
                });
            }
        }
    });
});
router.get('/club-request', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT ctu.club_id, u.id, u.first_name, u.last_name, (SELECT c.name FROM clubs c WHERE c.id = mcr.club_id) AS c_name " +
            "FROM clubs_to_user ctu " +
            "RIGHT JOIN manager_club_requests mcr ON mcr.club_id = ctu.club_id " +
            "LEFT JOIN users u ON u.id = mcr.user_id " +
            "WHERE ctu.user_id = 46", function(err, rows, fields) {
                if (err) {
                    res.json({
                        status: false,
                        result: err
                    });
                }else{
                    res.json({
                        status: true,
                        result: rows
                    });
                }
            });
        }
    });
});

router.post('/accept-club-request', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }
                connection.insert('clubs_members', {
                    club_id: req.body.club_id,
                    user_id: req.body.user_id
                }, function (err, results) {
                    if (err) {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }

                    connection.delete('manager_club_requests', { club_id: req.body.club_id, user_id: req.body.user_id }, function(err, affectedRows) {
                        if (err) {
                            return connection.rollback(function() {
                                throw err;
                            });
                        }
                        connection.commit(function(err) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                            res.json({
                                status: true,
                                result: affectedRows
                            });
                        });
                    });
                });
                connection.release();
            });
        }
    });
});

router.post('/decline-club-request', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.delete('manager_club_requests', { club_id: req.body.club_id, user_id: req.body.user_id }, function(err, affectedRows) {
                if (err) {
                    return connection.rollback(function() {
                        throw err;
                    });
                }
                connection.commit(function(err) {
                   if(!err) {
                       res.json({
                           status: true,
                           result: affectedRows
                       });
                   }else{
                       res.json({
                           status: false,
                           result: 'db error'
                       });
                   }
                });
            });

            connection.release();
        }
    });
});

router.post('/manager-club', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.insert('manager_club_requests', {
                club_id: req.body.club_id,
                user_id: user_id

            }, function(err, result) {
                if (err) {
                    res.json({
                        status: false,
                        result: err
                    });
                }else{
                    res.json({
                        status: true,
                        result: 'manage-club add request '+result
                    });
                }
            });
        }
    });
});
router.post('/joinGame', function(req, res) {

    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }

                if (req.body.invites) {
                    req.body.invites.forEach(function(el, ind, arr) {
                        connection.insert('game_invites', {
                            user_id: el.id,
                            game_id: req.body.game_id

                        }, function(err, result) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                        });
                    })
                }

                connection.insert('games_members', {
                    user_id: user_id,
                    game_id: req.body.game_id,
                    payment: 1,
                    reserve: 0,
                    members: (req.body.members_price.member) ? req.body.members_price.member : 1,

                }, function(err, result) {
                    if (err) {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }
                    connection.query('UPDATE user_credits SET balance = balance - ' + req.body.members_price.price + '  WHERE user_id = ' + user_id, function(err, results) {
                        if (err) {
                            return connection.rollback(function() {
                                throw err;
                            });
                        }
                        connection.queryRow("SELECT count(*) as count from user_credits WHERE user_id = " + req.body.club_user, function(err, row) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                            connection.insert('teammates', {
                                user_id: user_id,
                                game_id: req.body.game_id,
                                gym_id: req.body.gym_id

                            }, function(err, result) {
                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }
                                if (row.count > 0) {
                                    connection.query('UPDATE user_credits SET balance = balance + ' + req.body.members_price.price + '  WHERE user_id = ' + req.body.club_user, function(err, results) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                        connection.commit(function(err) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }

                                            console.log('success')
                                            res.json({
                                                status: true,
                                                message: 'Game added'
                                            });

                                        });
                                    });
                                } else {
                                    connection.insert('user_credits', {
                                        user_id: req.body.club_user,
                                        balance: req.body.members_price.price,

                                    }, function(err, result) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }
                                        connection.commit(function(err) {
                                            if (err) {
                                                return connection.rollback(function() {
                                                    throw err;
                                                });
                                            }

                                            console.log('success')
                                            res.json({
                                                status: true,
                                                message: 'Game added'
                                            });

                                        });
                                    })
                                }
                            });
                        })
                    });
                });
            })
        }
        connection.release();
    });
});

router.get('/getBalance', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow('SELECT uc.balance, (SELECT ct.percent FROM clubs_to_user ctu LEFT JOIN clubs c ON ctu.club_id = c.id LEFT JOIN club_types ct ON c.fees = ct.id WHERE ctu.user_id = uc.user_id) as percent from user_credits uc WHERE uc.user_id = ' + user_id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getConfig', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow("SELECT s.s_value from settings s WHERE s.s_code = '" + req.query.code + "' AND s.s_key = '" + req.query.key + "'", function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json({
                            status: true,
                            row
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                    ''
                }
            });
            connection.release();
        }
    });
});

router.post('/createPayout', function(req, res) {
    var user_id = req.decoded.id;
    var sender_batch_id = Math.random().toString(36).substring(9);

    var create_payout_json = {
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [{
            "recipient_type": "EMAIL",
            "amount": {
                "value": req.body.amount,
                "currency": "USD"
            },
            "receiver": req.body.email,
            "note": "Thank you.",
            "sender_item_id": user_id
        }]
    };

    var sync_mode = 'true';

    paypal.payout.create(create_payout_json, sync_mode, function(error, payout) {
        if (error) {
            throw error;
        } else {
            console.log("Create Single Payout Response");
            if (payout.batch_header.batch_status = 'SUCCESS') {
                pool.getConnection(function(err, connection) {
                    if (!err) {
                        mysqlUtilities.upgrade(connection);
                        mysqlUtilities.introspection(connection);


                        connection.query("INSERT INTO `payout_history` (`payout_id`, `user_id`, `amount`, `time_created`, `time_completed`, `payout_status`) " +
                            "VALUES ('" + payout.batch_header.payout_batch_id + "', '" + user_id + "', '" + req.body.amount + "', '" + payout.batch_header.time_created + "', '" + payout.batch_header.time_completed + "', 'SUCCESS')",
                            function(err, row, fields) {
                                if (!err) {
                                    if (row) {
                                        connection.queryRow('SELECT balance FROM user_credits WHERE user_id ="' + user_id + '"', function(err, results) {
                                            if (err) {
                                                res.json({
                                                    status: false,
                                                    message: 'Db error',
                                                    field: 'db'
                                                });
                                            } else {
                                                var credit_count = parseInt(results.balance) - (parseFloat(req.body.amount) + parseFloat(req.body.percent));
                                                if (err) {
                                                    throw err;
                                                } else {
                                                    connection.update('user_credits', {
                                                        balance: credit_count
                                                    }, {
                                                        user_id: user_id
                                                    }, function(err, result) {
                                                        if (err) {
                                                            throw err;
                                                        } else {
                                                            res.json({
                                                                status: true,
                                                                message: 'Payout completed',
                                                                payout
                                                            });
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                } else {
                                    res.json({
                                        status: false,
                                        message: 'Db error',
                                        field: 'db'
                                    });
                                }
                            });
                        connection.release();
                    }
                });
            } else {
                res.json({
                    status: false,
                    message: 'Payout canceled',
                    field: 'db'
                });
            }
        }
    });
});


router.get('/payout_history', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT ph.* FROM `payout_history` ph WHERE ph.user_id = " + user_id + " ORDER BY ph.id DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json({
                            status: true,
                            rows
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.get('/game_forInvite/:user', function(req, res) {
    var user_id = req.decoded.id,
        user = req.params.user;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT DISTINCT g.*, (SELECT COUNT(*) FROM game_invites WHERE user_id = "+user+" AND game_id = gms.game_id) as invite, gm.gym_sport, gm.name " +
                "FROM games_members gms " +
                "LEFT JOIN games g ON gms.game_id = g.id " +
                "LEFT JOIN gyms gm ON g.gym_id = gm.id " +
                "WHERE g.status = 1 AND gms.user_id = " + user_id,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            res.json({
                                status: true,
                                result: rows
                            });
                        }
                    } else {
                        res.json({
                            status: false,
                            message: err,
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });

});
router.get('/game/:id', function(req, res) {
    var user_id = req.decoded.id,
        id_game = req.params.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow("SELECT ctu.user_id as club_user, (SELECT count(cm.id) FROM clubs_members cm WHERE cm.club_id = c.id AND cm.user_id = " + user_id + ") as c_member, gm.price_member, gm.price_hours, g.name, g.id AS gym_id, c.latitude, c.longitude, c.name as club_name, (SELECT u.avatar FROM users u WHERE u.id = " + user_id + ") AS user_avatar, g.price, g.number_members, g.gym_sport, gm.members, gm.date, gm.start_time, gm.end_time, (SELECT DISTINCT COUNT(*) FROM game_watch gw2 WHERE gw2.game_id = " + id_game + " AND gw2.user_id = " + user_id + " ) AS my_watch, (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, IF(ctf.percent>=0, ctf.percent, 0) AS fees, c.address, c.latitude, c.longitude, c.website, c.email, ctu.phone, " +
                "CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('id', u.id, 'img', u.avatar, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, " +
                "CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count " +
                "FROM games gm LEFT JOIN gyms g ON g.id = gm.gym_id LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id LEFT JOIN clubs c ON gtc.club_id = c.id LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id LEFT JOIN club_types ctf ON c.fees = ctf.id WHERE gm.id = " + id_game,
                function(err, row, fields) {
                    if (!err) {
                        if (row) {
                            var global_count = 0;

                            row.members_avatar = JSON.parse(row.members_avatar); //.replace('\\', '/').split(',')
                            row.members_count = JSON.parse(row.members_count);;
                            global_count += row.members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (row.members_avatar) {
                                row.members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img,
                                            'id': a_e.id
                                        })
                                    }
                                })
                                row.members_avatar = tmp_arr;
                            }
                            if (row.members_count) {
                                row.members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            row.free_members = [];
                            for (var j = 0; j < (row.number_members - count); j++) {
                                row.free_members.push(j);
                            }
                            row.global_members = count;
                            row.members_range = range.range(0, row.number_members);

                            res.json({
                                status: true,
                                row
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/gym/:id', function(req, res) {
    var user_id = req.decoded.id,
        id_gym = req.params.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow("SELECT ctu.user_id as club_user, g.name, g.price, g.number_members, g.gym_sport, c.fees, c.address, c.latitude, c.longitude, c.website, c.email, ctu.phone, (SELECT u.avatar FROM users u WHERE u.id = " + user_id + ") AS user_avatar, (SELECT u.avatar_type FROM users u WHERE u.id = " + user_id + ") AS user_avatar_type FROM gyms g JOIN gyms_to_club gtc ON gtc.gym_id = g.id LEFT JOIN clubs c ON gtc.club_id = c.id LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id WHERE g.id = " + id_gym, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        row.members_range = range.range(0, row.number_members);
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/clublast-visited', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT c.id, c.name FROM club_favorite cf " +
                " LEFT JOIN clubs c ON c.id = cf.club_id " +
                " WHERE " +
                "	cf.user_id = " + user_id +
                " GROUP BY cf.club_id " +
                " ORDER BY " +
                " cf.date DESC ; SELECT c.id, c.name FROM club_visite_history cvh " +
                " LEFT JOIN clubs c ON c.id = cvh.club_id " +
                " WHERE " +
                "	cvh.user_id = " + user_id + " AND NOT EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id) " +
                " GROUP BY cvh.club_id " +
                " ORDER BY " +
                " cvh.date DESC LIMIT 5",
                function(err, row, fields) {
                    if (!err) {
                        result = row[0].concat(row[1].slice(0, 5 - row[0].length))
                        res.json({
                            status: true,
                            rows: result
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});
router.get('/club/:id', function(req, res) {
    var user_id = req.decoded.id,
        club_id = req.params.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow("SELECT c.id, c.type, c.price, c.latitude, EXISTS(SELECT * FROM club_favorite cf WHERE cf.user_id=" + user_id + " AND cf.club_id=c.id) as favorite, c.longitude, c.name, c.address, c.hours, c.website, ctu.email, ctu.phone, (SELECT CEILING(3963.1676 * acos ( cos ( radians( u3.latitude ) ) * cos( radians( c2.`latitude` ) ) * cos( radians( c2.`longitude` ) - radians( u3.longitude ) ) + sin ( radians( u3.latitude ) ) * sin( radians( c2.`latitude` ) ) ) ) AS distance FROM clubs c2 LEFT JOIN clubs_to_user ctu2 ON c2.id = ctu2.club_id LEFT JOIN users u3 ON ctu2.user_id = u3.id WHERE c2.id = " + club_id + ") as distance, " +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id AND cm.user_id = " + user_id + ") as me_member, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members, (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM `clubs` c LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id WHERE c.id = " + club_id + ";INSERT INTO `club_visite_history` (`id`, `club_id`, `user_id`, `date`) VALUES (NULL, '" + club_id + "', '" + user_id + "', CURRENT_TIMESTAMP);",
                function(err, row, fields) {
                    if (!err) {
                        if (row[0]) {
                            row[0].members_range = range.range(0, row[0].number_members);
                            res.json(row[0]);
                        }
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});


router.post('/clubGames/', function(req, res) {
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = '';


    if (typeof req.body.filter.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball', 'Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.filter.sport) {
            if (req.body.filter.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND (g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
        if(gym_sport !== ''){
            gym_sport += ')';
        }
    }

    if (req.body.filter.access != '') {
        var gym_type = '';
        switch (req.body.filter.access) {
            case 'All':
                {
                    gym_type = " AND (g.type = 'public' OR c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                    break;
                }
            case 'Public':
                {
                    gym_type += " AND g.type = 'public'";
                    break;
                }
            case 'Private':
                {
                    gym_type = " AND (g.type = 'private' AND c.id IN (SELECT cm.club_id FROM clubs_members cm WHERE cm.user_id = " + user_id + "))";
                    break;
                }
        }
    }

    if (req.body.filter.gender != '') {
        var game_gender = '';
        switch (req.body.filter.gender) {
            case 'Male':
                {
                    game_gender = " AND gm.gender = 'Male'";
                    break;
                }
            case 'Female':
                {
                    game_gender = " AND gm.gender = 'Female'";
                    break;
                }
        }
    }
    if (typeof req.body.filter.age == 'object') {
        var game_age = " AND (gm.age_start >= " + req.body.filter.age.min + " AND gm.age_end <= " + req.body.filter.age.max + ")";
    }

    if (req.body.filter.dt && req.body.filter.dt != '') {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.filter.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        if (typeof req.body.filter.start_time == "number" && typeof req.body.filter.end_time == "number") {
            game_filter_where += " ";
        }
    }

    game_orderby = ' gm.start_time ASC';
    var ifdate = ' ';
    var ifdate2 = ' ';
    if (typeof date != 'undefined') {
        ifdate = "AND  gm.date = '" + date + "'";
        ifdate2 = " gm_s.date = '" + date + "' AND ";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query(
                "SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, IF(ISNULL(gm.members), 0, gm.members) AS members, (SELECT COUNT(*) as count FROM games gm_s WHERE " + ifdate2 + " ((gm_s.start_time <= " + req.body.filter.start_time + " AND " + req.body.filter.start_time + " <= gm_s.end_time AND " + req.body.filter.end_time + " >= gm_s.start_time AND " + req.body.filter.end_time + " <= gm_s.end_time) OR (" + req.body.filter.start_time + " <= gm_s.start_time AND " + req.body.filter.end_time + " >= gm_s.end_time) OR (" + req.body.filter.start_time + " <= gm_s.start_time AND " + req.body.filter.end_time + " >= gm_s.start_time AND " + req.body.filter.end_time + " <= gm_s.end_time) OR (" + req.body.filter.start_time + " >= gm_s.start_time AND " + req.body.filter.start_time + " <= gm_s.end_time AND " + req.body.filter.end_time + " >= gm_s.end_time)) AND gm_s.gym_id = g.id) as game," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                ", (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, (SELECT COUNT(*) FROM game_watch gw2 WHERE gw2.game_id = gm.id AND gw2.user_id = "+ user_id +" ) AS my_watch" +
                " FROM clubs c" +
                " LEFT JOIN gyms_to_club gtc ON c.id = gtc.club_id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON g.id = gm.gym_id " + ifdate + " AND ((gm.start_time <= " + req.body.filter.start_time + " AND " + req.body.filter.start_time + " <= gm.end_time AND " + req.body.filter.end_time + " >= gm.start_time AND " + req.body.filter.end_time + " <= gm.end_time) OR (" + req.body.filter.start_time + " <= gm.start_time AND " + req.body.filter.end_time + " >= gm.end_time) OR (" + req.body.filter.start_time + " <= gm.start_time AND " + req.body.filter.end_time + " >= gm.start_time AND " + req.body.filter.end_time + " <= gm.end_time) OR (" + req.body.filter.start_time + " >= gm.start_time AND " + req.body.filter.start_time + " <= gm.end_time AND " + req.body.filter.end_time + " >= gm.end_time)) AND gm.status = 1 " +
                " WHERE gm.date <> '' AND c.id = " + req.body.id + gym_sport + gym_type + game_filter_where +
                " ORDER BY game DESC, " + game_orderby,
                function(err, rows, fields) {
                    if (!err) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);

                            global_count += rows[index].members_count;
                            var count = 0;
                            var tmp_arr = [];
                            if (rows[index].members_avatar) {
                                rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                    for (var i = 0; i < a_e.count; i++) {
                                        tmp_arr.push({
                                            'img': a_e.img
                                        })
                                    }
                                })
                                rows[index].members_avatar = tmp_arr;
                            }
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                            }
                            rows[index].global_members = count;
                            rows[index].members_range = range.range(0, rows[index].number_members);

                        })

                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/teammates', function(req, res) {
    var user_id = req.decoded.id;
    var sql = '';
    if (req.query.name != '') {
        sql = " WHERE u.first_name like '%" + req.query.name + "%' OR u.last_name like '%" + req.query.name + "%'";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT u.* from users u " + sql, function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.get('/badQuetions', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT nb.* from notification_bad nb", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getMembersGame/:id', function(req, res) {
    var user_id = req.decoded.id,
        game_id = req.params.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT DISTINCT u.id, u.avatar, u.first_name, u.last_name FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = " + game_id, function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        res.json(rows);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});
router.post('/setUserRating', function(req, res) {
    var user_id = req.decoded.id;

    if (req.body.stars) {
        Object.keys(req.body.stars).forEach(function(e, i, arr) {
            pool.getConnection(function(err, connection) {
                if (!err) {
                    mysqlUtilities.upgrade(connection);
                    mysqlUtilities.introspection(connection);
                    connection.insert('user_rating', {
                        game_id: req.body.game_id,
                        to_user_id: user_id,
                        from_user_id: parseInt(e),
                        stars: req.body.stars[e]
                    }, function(err, recordId) {
                        if (err)
                            throw err;
                        console.dir({
                            insert: recordId
                        });
                    });
                }
                connection.release();
            });
        })
        res.json({
            status: true,
            message: 'Rate added'
        });
    } else {
        res.json({
            status: false,
            message: 'Rate added'
        });
    }
});

router.post('/setBad', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.insert('game_feedback', {
                game_id: req.body.game_id,
                to_user_id: user_id,
                quest_id: req.body.quest
            }, function(err, recordId) {
                if (err) {
                    throw err;
                } else {
                    res.json({
                        status: true,
                        message: 'Rate added'
                    });
                }
                console.dir({
                    insert: recordId
                });
            });
        }
        connection.release();
    });
});
router.get('/getManagerGames/:gym_id', function(req, res) {
    var user_id = req.decoded.id,
        gym_id = req.params.gym_id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.release();
            connection.query("SELECT *, gm.id as game_id, " +
                "CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members, 'sum', IF((SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = gms.user_id AND cm.club_id = gtc.club_id)>0, gm.price_member*gms.members, gm.price_hours*gms.members)) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_json " +
                "FROM `games` gm " +
                "LEFT JOIN gyms g ON g.id = gm.gym_id " +
                "LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id " +
                "WHERE gm.user_id = " + user_id + " AND gm.gym_id = " + gym_id + " AND gm.status = 1 ORDER BY gm.`id` DESC", function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        var promise = new Promise((resolve, reject) => {
                            rows.forEach(function (elem, index, array) {
                                var res = {
                                    count: 0,
                                    sum: 0
                                };
                                if(elem.members_json) {
                                    JSON.parse(elem.members_json).forEach(function (e, i, arr) {
                                        res.count += e.count;
                                        res.sum += e.sum;
                                    })
                                }
                                rows[index].members_json = res;
                            })
                            resolve(rows);
                        });
                        promise.then(
                            function (data) {
                                res.json({
                                    status: false,
                                    items: data
                                });
                            }
                        )

                    }
                } else {
                    console.log(err);
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
        }
    });
})
router.post('/ManagerCreateGame', function(req, res) {
    var user_id = req.decoded.id;
    calcPriceGame(req.body.start_time, req.body.end_time, req.body.price, req.body.price_for_member, function(data) {
        console.log('prices: ', data);
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.insert('games', {
                    gym_id: req.body.gym_id,
                    user_id: user_id,
                    type: req.body.type,
                    date: formatDate(req.body.dt),
                    start_time: req.body.start_time,
                    end_time: req.body.end_time,
                    age_start: req.body.age.min,
                    age_end: req.body.age.max,
                    price_hours: data.p1,
                    status: '1',
                    price_member: data.p2,
                    gender: req.body.gender,
                    watching: 0
                }, function(err, recordId) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.json({
                            status: true,
                            recordId: recordId
                        });
                        setCronStartGame(recordId, formatDate(req.body.dt), req.body.start_time);
                        CreateGameTask('game_create', req.body.gym_id, recordId, req.body.start_time, req.body.end_time, formatDate(req.body.dt));
                    }
                });
            }
            connection.release();
        });
    })
});
router.post('/msgInviteUser', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.insert('game_invites', {
                user_id: req.body.user_id,
                game_id: req.body.game_id

            }, function(err, result) {
                if (err) {
                    throw err;
                } else {
                    res.json({
                        status: true,
                        message: result
                    });
                }
            });
        }
    });
});
router.post('/ManagerDeleteGame', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            console.log('delete ', req.body.game_id);

            connection.delete('games', {
                id: req.body.game_id,
            }, function(err) {
                if (err) {
                    console.log(err);
                }else{
                    res.json({status: true})
                }
            });
        }
        connection.release();
    });
});

/*
router.post('/createGame', function (req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function (err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.beginTransaction(function (err) {
                if (err) {
                    throw err;
                }

                connection.insert('games', {
                    gym_id: req.body.gym_id,
                    user_id: user_id,
                    type: req.body.game.access,
                    date: formatDate(req.body.game.dt),
                    start_time: req.body.game.start_time,
                    end_time: req.body.game.end_time,
                    age_start: req.body.game.age.min,
                    age_end: req.body.game.age.max,
                    gender: req.body.game.gender,
                    watching: 0
                }, function (err, recordId) {
                    if (err) {
                        return connection.rollback(function () {
                            throw err;
                        });
                    }
                    if (req.body.invites) {
                        req.body.invites.forEach(function (el, ind, arr) {
                            connection.insert('game_invites', {
                                user_id: el.id,
                                game_id: recordId

                            }, function (err, result) {
                                if (err) {
                                    return connection.rollback(function () {
                                        throw err;
                                    });
                                }
                            });
                        })
                    }

                    connection.insert('games_members', {
                        user_id: user_id,
                        game_id: recordId,
                        payment: 1,
                        reserve: 0,
                        members: (req.body.members_price.member) ? req.body.members_price.member : 1,

                    }, function (err, result) {
                        if (err) {
                            return connection.rollback(function () {
                                throw err;
                            });
                        }
                        connection.query('UPDATE user_credits SET balance = balance - ' + req.body.members_price.price + '  WHERE user_id = ' + user_id, function (err, results) {
                            if (err) {
                                return connection.rollback(function () {
                                    throw err;
                                });
                            }
                            connection.queryRow("select count(*) as count from user_credits WHERE user_id = " + req.body.club_user, function (err, row) {
                                if (err) {
                                    return connection.rollback(function () {
                                        throw err;
                                    });
                                }
                                connection.insert('teammates', {
                                    user_id: user_id,
                                    game_id: recordId,
                                    gym_id: req.body.gym_id

                                }, function (err, result) {
                                    if (err) {
                                        return connection.rollback(function () {
                                            throw err;
                                        });
                                    }
                                    if (row.count > 0) {
                                        connection.query('UPDATE user_credits SET balance = balance + ' + req.body.members_price.price + '  WHERE user_id = ' + req.body.club_user, function (err, results) {
                                            if (err) {
                                                return connection.rollback(function () {
                                                    throw err;
                                                });
                                            }
                                            connection.commit(function (err) {
                                                if (err) {
                                                    return connection.rollback(function () {
                                                        throw err;
                                                    });
                                                }
                                                setCronStartGame(recordId, formatDate(req.body.game.dt), req.body.game.start_time);

                                                CreateGameTask('game_start', req.body.gym_id, recordId, req.body.game.start_time, req.body.game.end_time, formatDate(req.body.game.dt));

                                                console.log('success')
                                                res.json({status: true, message: 'Game added'});

                                            });
                                        });
                                    } else {
                                        connection.insert('user_credits', {
                                            user_id: req.body.club_user,
                                            balance: req.body.members_price.price,

                                        }, function (err, result) {
                                            if (err) {
                                                return connection.rollback(function () {
                                                    throw err;
                                                });
                                            }
                                            connection.commit(function (err) {
                                                if (err) {
                                                    return connection.rollback(function () {
                                                        throw err;
                                                    });
                                                }

                                                setCronStartGame(recordId, formatDate(req.body.game.dt), req.body.game.start_time);

                                                CreateGameTask('game_start', req.body.gym_id, recordId, req.body.game.start_time, req.body.game.end_time, formatDate(req.body.game.dt));

                                                console.log('success')
                                                res.json({status: true, message: 'Game added'});

                                            });
                                        })
                                    }
                                })
                            })
                        });
                    });
                });
            })
        }

        connection.release();
    });
});
*/

router.post('/addReserveGame', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }

                connection.insert('games', {
                    gym_id: req.body.gym_id,
                    user_id: user_id,
                    type: req.body.game.access,
                    date: formatDate(req.body.game.dt),
                    start_time: req.body.game.start_time,
                    end_time: req.body.game.end_time,
                    age_start: req.body.game.age.min,
                    age_end: req.body.game.age.max,
                    gender: req.body.game.gender,
                    watching: 0
                }, function(err, recordId) {
                    if (err) {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }
                    if (req.body.invites) {
                        req.body.invites.forEach(function(el, ind, arr) {
                            connection.insert('game_invites', {
                                user_id: el.id,
                                game_id: recordId

                            }, function(err, result) {
                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }
                            });
                        })
                    }

                    connection.insert('games_members', {
                        user_id: user_id,
                        game_id: recordId,
                        payment: 0,
                        reserve: 1,
                        members: (req.body.members_price.member) ? req.body.members_price.member : 1,

                    }, function(err, result) {
                        if (err) {
                            return connection.rollback(function() {
                                throw err;
                            });
                        }
                        connection.commit(function(err) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                            console.log('reserved')
                            res.json({
                                status: true,
                                message: 'Game reserved'
                            });

                        });
                    });
                });
            })
        }
        connection.release();
    });
});

router.post('/payReserve', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }

                connection.query("UPDATE `games_members` SET `payment` = '1', `reserve` = '0' WHERE `games_members`.`game_id` = " + req.body.game_id + " AND `games_members`.`user_id` = " + user_id + " AND `games_members`.`reserve` = '1'", function(err, result) {
                    if (err) {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }
                    connection.query('UPDATE user_credits SET balance = balance - ' + req.body.price + '  WHERE user_id = ' + user_id, function(err, results) {
                        if (err) {
                            return connection.rollback(function() {
                                throw err;
                            });
                        }
                        connection.queryRow("select count(*) as count from user_credits WHERE user_id = " + req.body.club_user, function(err, row) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                            if (row.count > 0) {
                                connection.query('UPDATE user_credits SET balance = balance + ' + req.body.price + '  WHERE user_id = ' + req.body.club_user, function(err, results) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    connection.commit(function(err) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }

                                        console.log('success')
                                        res.json({
                                            status: true,
                                            message: 'Game added'
                                        });

                                    });
                                });
                            } else {
                                connection.insert('user_credits', {
                                    user_id: req.body.club_user,
                                    balance: req.body.price,

                                }, function(err, result) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    connection.commit(function(err) {
                                        if (err) {
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        }

                                        console.log('success')
                                        res.json({
                                            status: true,
                                            message: 'Game added'
                                        });

                                    });
                                })
                            }
                        })
                    });
                });
            })
        }
        connection.release();
    });
});

router.post('/joinReserveGame', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }

                if (req.body.invites) {
                    req.body.invites.forEach(function(el, ind, arr) {
                        connection.insert('game_invites', {
                            user_id: el.id,
                            game_id: req.body.game_id

                        }, function(err, result) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }
                        });
                    })
                }

                connection.insert('games_members', {
                    user_id: user_id,
                    game_id: req.body.game_id,
                    payment: 0,
                    reserve: 1,
                    members: (req.body.members_price.member) ? req.body.members_price.member : 1,

                }, function(err, result) {
                    if (err) {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }

                    connection.commit(function(err) {
                        if (err) {
                            return connection.rollback(function() {
                                throw err;
                            });
                        }

                        console.log('join reserved')
                        res.json({
                            status: true,
                            message: 'Game reserved'
                        });

                    });
                });
            })
        }
        connection.release();
    });
});

router.get('/games', function(req, res) {
    var user_id = req.decoded.id,
        game_distance_select = '';

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            if (!!req.query.latitude && !!req.query.longitude && !!req.query.distance) {
                game_distance_select = ', CEILING(' +
                    '3963.1676 * acos ( ' +
                    'cos ( radians(' + req.query.latitude + ') )' +
                    '* cos( radians( c.`latitude` ) )' +
                    '* cos( radians( c.`longitude` ) - radians(' + req.query.longitude + ') )' +
                    '+ sin ( radians(' + req.query.latitude + ') )' +
                    '* sin( radians( c.`latitude` ) ) )' +
                    ') AS distance ';
                game_distance_heving = ' CEILING(distance) <= ' + req.query.distance;
            }

            q = connection.query("SELECT \"joined\" as type_game, g.name, gm.price_hours, gm.price_member, g.number_members, g.gym_sport" + game_distance_select + ", gm.members, gm.id, gm.date, gm.start_time, gm.end_time, (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, c.address, c.name as club_name, c.latitude, c.longitude, c.website, c.email, ctu.phone, CONCAT('[',(SELECT GROUP_CONCAT(  JSON_OBJECT('img', u.avatar,'id',u.id) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count FROM games gm LEFT JOIN gyms g ON g.id = gm.gym_id LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id LEFT JOIN clubs c ON gtc.club_id = c.id LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id WHERE gm.status = 1 AND EXISTS(SELECT * FROM games_members WHERE payment = 1 AND user_id = "+  user_id +" AND game_id = gm.id)" , function(err, rows, fields) {
                if (!err) {
                    if (rows) {
                        var global_count = 0;
                        rows.forEach(function(elem, index, array) {

                            rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                            rows[index].members_count = JSON.parse(elem.members_count);;
                            global_count += elem.members_count;
                            var count = 0;
                            if (rows[index].members_count) {
                                rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                })
                                rows[index].global_members = count;
                            }
                            rows[index].members_range = range.range(0, rows[index].number_members);
                        })

                        res.json({
                            status: true,
                            rows
                        });
                    }
                } else {
                    console.log(err);
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getReserved', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT DISTINCT \"reserve\" as type_game, ctu.user_id AS club_user, gms1.reserve, g.name, gm.price_hours, gm.price_member, g.number_members, g.gym_sport, gm.date, gm.id, gm.start_time, gm.end_time, (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, c.address, c.latitude, c.longitude, c.website, c.email, ctu.phone," +
                " CONCAT('[',(SELECT GROUP_CONCAT(  JSON_OBJECT('img', u.avatar) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id AND gms.user_id = gms1.user_id),']') as members_count " +
                " FROM games_members gms1 " +
                " LEFT JOIN games gm ON gms1.game_id = gm.id " +
                " LEFT JOIN gyms g ON g.id = gm.gym_id " +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id " +
                " LEFT JOIN clubs c ON gtc.club_id = c.id " +
                " LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id " +
                " WHERE gm.status = 1 AND gms1.reserve = 1 AND gms1.user_id = " + user_id,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            var global_count = 0;
                            rows.forEach(function(elem, index, array) {

                                rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                                rows[index].members_count = JSON.parse(elem.members_count);
                                global_count += elem.members_count;
                                var count = 0;
                                if (rows[index].members_count) {
                                    rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                        count += parseInt(i_el.count);
                                    })
                                    rows[index].global_members = count;
                                }
                                rows[index].members_range = range.range(0, rows[index].number_members);
                            })

                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/gamesWatch', function(req, res) {
    var user_id = req.decoded.id,
        game_distance_select = '';
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            if (req.query.latitude && req.query.longitude && req.query.distance) {
                game_distance_select = ', CEILING(' +
                    '3963.1676 * acos ( ' +
                    'cos ( radians(' + req.query.latitude + ') )' +
                    '* cos( radians( c.`latitude` ) )' +
                    '* cos( radians( c.`longitude` ) - radians(' + req.query.longitude + ') )' +
                    '+ sin ( radians(' + req.query.latitude + ') )' +
                    '* sin( radians( c.`latitude` ) ) )' +
                    ') AS distance ';
                game_distance_heving = ' CEILING(distance) <= ' + req.query.distance;
            }



            q = connection.query("SELECT \"watch\" as type_game, g.name, gm.price_hours, gm.price_member, g.number_members " + game_distance_select + ", g.gym_sport, gm.date, gm.id, gm.start_time, gm.end_time, (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, (SELECT COUNT(*) FROM game_watch gw2 WHERE gw2.game_id = gm.id AND gw2.user_id = "+ user_id +" ) AS my_watch, c.address, c.name as club_name, c.latitude, c.longitude, c.website, c.email, ctu.phone," +
                " CONCAT('[',(SELECT GROUP_CONCAT(  JSON_OBJECT('img', u.avatar,'id',u.id) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count " +
                "FROM games gm LEFT JOIN gyms g ON g.id = gm.gym_id" +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id" +
                " LEFT JOIN clubs c ON gtc.club_id = c.id" +
                " LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id" +
                " LEFT JOIN game_watch gw ON gw.game_id = gm.id" +
                " WHERE gm.status = 1 AND gw.user_id = " + user_id + " AND NOT EXISTS(SELECT * FROM games_members WHERE user_id = "+  user_id +" AND game_id = gm.id) ORDER BY gm.date, gm.start_time",
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            var global_count = 0;
                            rows.forEach(function(elem, index, array) {

                                rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                                rows[index].members_count = JSON.parse(elem.members_count);
                                global_count += elem.members_count;
                                var count = 0;
                                if (rows[index].members_count) {
                                  rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                    count += parseInt(i_el.count);
                                  })
                                }
                                rows[index].global_members = count;
                                rows[index].members_range = range.range(0, rows[index].number_members);
                            })
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/gamesInvites', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT \"invite\" as type_game, g.name, gm.price_hours, gm.price_member, g.number_members, g.gym_sport, gm.id, gm.date, gm.start_time, gm.end_time, (SELECT COUNT(*) FROM game_watch gw WHERE gw.game_id = gm.id ) AS watching, c.address, c.latitude,c.name as club_name, c.longitude, c.website, c.email, ctu.phone," +
                " CONCAT('[',(SELECT GROUP_CONCAT(  JSON_OBJECT('img', u.avatar) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar," +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count " +
                "FROM games gm LEFT JOIN gyms g ON g.id = gm.gym_id" +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id" +
                " LEFT JOIN clubs c ON gtc.club_id = c.id" +
                " LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id" +
                " LEFT JOIN game_invites gi ON gi.game_id = gm.id" +
                " WHERE gm.status = 1 AND gi.user_id = " + user_id,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            var global_count = 0;
                            rows.forEach(function(elem, index, array) {

                                rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                                rows[index].members_count = JSON.parse(elem.members_count);;
                                global_count += elem.members_count;
                                var count = 0;
                                console.log('fgfgh', elem.members_count);
                                if (rows[index].members_count) {
                                    rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                        count += parseInt(i_el.count);
                                    })
                                    rows[index].global_members = count;
                                }
                                rows[index].members_range = range.range(0, rows[index].number_members);
                            })
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/watchGame', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow('SELECT COUNT(*) as count FROM game_watch gw WHERE user_id = ' + user_id + " AND game_id = " + req.body.id, function(err, row, fields) {
                if (row.count == 0) {
                    connection.insert('game_watch', {
                        user_id: user_id,
                        game_id: req.body.id
                    }, function(err, result) {
                        if (!err) {
                            res.json({
                                status: 'insert'
                            });
                        }
                    });
                } else {
                    connection.delete('game_watch', {
                        user_id: user_id,
                        game_id: req.body.id
                    }, function(err, result) {
                        if (!err) {
                            res.json({
                                status: 'delete'
                            });
                        }
                    });

                }
            })
            connection.release();
        }

    });
});

router.post('/my_teammates', function(req, res) {
    var user_id = req.decoded.id;
    var team_where = '';
    var team_distance_select = '',
        team_distance_heving = 'ORDER BY t.date_add DESC'
    team_order_by = '';

    if (req.body.tab) {
        var tab_sql = '';
        switch (req.body.tab) {
            case 'recent':
                {
                    team_order_by = ' ORDER BY u.drafted DESC ';
                    break;
                }
            case 'all':
                {
                    team_order_by = '';
                    break;
                }
        }
    }

    if (req.body.filter.latitude && req.body.filter.longitude) {
        team_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.filter.latitude + ') )' +
            '* cos( radians( u.`latitude` ) )' +
            '* cos( radians( u.`longitude` ) - radians(' + req.body.filter.longitude + ') )' +
            '+ sin ( radians(' + req.body.filter.latitude + ') )' +
            '* sin( radians( u.`latitude` ) ) )' +
            ') AS distance ';
        if (req.body.filter.distance != 0) {
            team_distance_heving = 'HAVING CEILING(distance) <= ' + req.body.filter.distance + ' ORDER BY ISNULL(distance), distance ASC, t.date_add DESC';
        } else {
            team_distance_heving = ' ORDER BY ISNULL(distance), distance ASC, t.date_add DESC';

        }
    }

    if (typeof req.body.filter.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball', 'Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.filter.sport) {
            if (req.body.filter.sport[sport_item]) {
                team_where += " AND t.user_id IN (SELECT ts2.user_id FROM teammates ts2 LEFT JOIN gyms g ON ts2.gym_id = g.id WHERE ts2.user_id = t.user_id AND g.gym_sport = '" + sports_types[sport_item - 1] + "')";
            }
        }
    }

    if (req.body.filter.name) {
        team_where += " AND u.first_name LIKE '%" + req.body.filter.name + "%' ";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            q = connection.query("SELECT *, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('icon', g.gym_sport) SEPARATOR ',') FROM teammates ts LEFT JOIN gyms g ON ts.gym_id = g.id  WHERE ts.user_id = t.user_id),']') AS sports, " +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = t.user_id) AS membership ," +
                " (SELECT COUNT(*) FROM teammates t3 WHERE t3.game_id IN (SELECT t4.game_id FROM teammates t4 WHERE t4.user_id = t.user_id) AND t3.user_id <> t.user_id) AS team_count" + team_distance_select +
                " FROM teammates t " +
                " LEFT JOIN users u ON t.user_id = u.id" +
                " WHERE t.game_id IN (SELECT t2.game_id FROM teammates t2 WHERE t2.user_id = " + user_id + ")" + team_where +
                " AND t.user_id <> " + user_id + ' GROUP BY t.user_id  ' + team_distance_heving,
                function(err, rows, fields) {
                    if (!err) {
                        // rows = rows[1]
                        if (rows) {
                            rows.forEach(function(elem, index, array) {
                                var output = [];
                                JSON.parse(elem.sports).forEach(function(e, i, arr) {
                                    if (output.indexOf(e.icon) === -1) {
                                        output.push(e.icon);
                                    }
                                });
                                rows[index].sports = output;
                            })
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/teammate', function(req, res) {
    var user_id = req.decoded.id;
    var team_distance_select = '';

    if (req.body.latitude && req.body.longitude) {
        team_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( u.`latitude` ) )' +
            '* cos( radians( u.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( u.`latitude` ) ) )' +
            ') AS distance ';
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT *, CONCAT('[',(SELECT DISTINCT GROUP_CONCAT( JSON_OBJECT('icon', g.gym_sport) SEPARATOR ',') FROM teammates ts LEFT JOIN gyms g ON ts.gym_id = g.id  WHERE ts.user_id = t.user_id),']') AS sports, " +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = t.user_id) AS membership ," +
                " (SELECT COUNT(*) FROM teammates t3 WHERE t3.game_id IN (SELECT t4.game_id FROM teammates t4 WHERE t4.user_id = t.user_id) AND t3.user_id <> t.user_id) AS team_count" + team_distance_select +
                " FROM teammates t " +
                " LEFT JOIN users u ON t.user_id = u.id" +
                " WHERE t.game_id IN (SELECT t2.game_id FROM teammates t2 WHERE t2.user_id = " + user_id + ")" +
                " AND t.user_id = " + req.body.id + " AND t.user_id <> " + user_id,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            rows.forEach(function(elem, index, array) {
                                var output = [];
                                JSON.parse(elem.sports).forEach(function(e, i, arr) {
                                    if (output.indexOf(e.icon) === -1) {
                                        output.push(e.icon);
                                    }
                                });
                                rows[index].sports = output;
                            })
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});
router.post('/profile', function(req, res) {
    var user_id = req.decoded.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT *, CONCAT('[',(SELECT DISTINCT GROUP_CONCAT( JSON_OBJECT('icon', g.gym_sport) SEPARATOR ',') FROM teammates ts LEFT JOIN gyms g ON ts.gym_id = g.id  WHERE ts.user_id = u.id),']') AS sports, " +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = u.id) AS membership ," +
                " (SELECT COUNT(*) FROM teammates t3 WHERE t3.game_id IN (SELECT t4.game_id FROM teammates t4 WHERE t4.user_id = u.id) AND t3.user_id <> u.id) AS team_count" +
                " FROM  users u " +
                " WHERE u.id = " + req.body.id,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            console.log(rows);
                            rows.forEach(function(elem, index, array) {
                                var output = [];
                                if (JSON.parse(elem.sports)) {
                                    JSON.parse(elem.sports).forEach(function(e, i, arr) {
                                        if (output.indexOf(e.icon) === -1) {
                                            output.push(e.icon);
                                        }
                                    });
                                }
                                rows[index].sports = output;
                            })
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/members', function(req, res) {
    var user_id = req.decoded.id;
    var team_where = '';
    var team_distance_select = '',
        team_distance_heving = ''
    team_order_by = '';

    if (req.body.tab) {
        var tab_sql = '';
        switch (req.body.tab) {
            case 'recent':
                {
                    team_order_by = ' ORDER BY u.drafted DESC ';
                    break;
                }
            case 'all':
                {
                    team_order_by = '';
                    break;
                }
        }
    }

    if (req.body.filter.latitude && req.body.filter.longitude) {
        team_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.filter.latitude + ') )' +
            '* cos( radians( u.`latitude` ) )' +
            '* cos( radians( u.`longitude` ) - radians(' + req.body.filter.longitude + ') )' +
            '+ sin ( radians(' + req.body.filter.latitude + ') )' +
            '* sin( radians( u.`latitude` ) ) )' +
            ') AS distance ';
        team_distance_heving = '';
    }
    if (req.body.filter.sport) {
        team_where = " AND cm.user_id IN (SELECT ts2.user_id FROM teammates ts2 LEFT JOIN gyms g ON ts2.gym_id = g.id WHERE ts2.user_id = cm.user_id AND g.gym_sport = '" + req.body.filter.sport + "')";
    }

    if (req.body.filter.name) {
        team_where = " AND u.first_name LIKE '%" + req.body.filter.name + "%' ";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT u.*, CONCAT('[',(SELECT DISTINCT GROUP_CONCAT( JSON_OBJECT(\"icon\", g.gym_sport) SEPARATOR ',') FROM teammates ts LEFT JOIN gyms g ON ts.gym_id = g.id  WHERE ts.user_id = cm.user_id),']') AS sports, " +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = cm.user_id) AS membership ," +
                " (SELECT COUNT(*) FROM teammates t3 WHERE t3.game_id IN (SELECT t4.game_id FROM teammates t4 WHERE t4.user_id = cm.user_id) AND t3.user_id <> cm.user_id) AS team_count" + team_distance_select +
                " FROM clubs_to_user ctu " +
                " LEFT JOIN clubs c ON ctu.club_id = c.id " +
                " LEFT JOIN clubs_members cm ON cm.club_id = c.id " +
                " LEFT JOIN users u ON u.id = cm.user_id  " +
                " WHERE ctu.user_id = " + user_id + team_where + team_distance_heving,
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            if (Array.isArray([rows])) {
                                rows.forEach(function(elem, index, array) {
                                    var output = [];
                                    if (JSON.parse(elem.sports)) {
                                        JSON.parse(elem.sports).forEach(function(e, i, arr) {
                                            if (output.indexOf(e.icon) === -1) {
                                                output.push(e.icon);
                                            }
                                        });
                                    }
                                    rows[index].sports = output;
                                })
                            }
                            res.json({
                                status: true,
                                rows
                            });
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.post('/teammateGames', function(req, res) {
    var user_id = req.decoded.id;
    var mysql_query = '';
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '',
        game_distance_order_by = '',
        game_orderby = '',
        gym_sport = '';

    if (typeof req.body.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball', 'Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND (g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR g.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
        if(gym_sport != ''){
            gym_sport += ')';
        }
    }

    if (req.body.dt && req.body.dt != '') {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
    }

    var ifdate = "";
    if (typeof date != 'undefined') {
        ifdate = " AND  gm.date = '" + date + "'";
        console.log(ifdate)
    }

    if (req.body.latitude && req.body.longitude) {
        game_distance_select = ', CEILING(' +
            '3963.1676 * acos ( ' +
            'cos ( radians(' + req.body.latitude + ') )' +
            '* cos( radians( c.`latitude` ) )' +
            '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
            '+ sin ( radians(' + req.body.latitude + ') )' +
            '* sin( radians( c.`latitude` ) ) )' +
            ') AS distance ';

        game_distance_heving = ' CEILING(distance) <= ' + req.body.distance;
    }


    switch (req.body.tab) {
        case 'active':
            {
                mysql_query = " SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'id',u.id,'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                " FROM games gm" +
                " LEFT JOIN gyms g ON gm.gym_id = g.id" +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id" +
                " LEFT JOIN clubs c ON c.id = gtc.club_id " +
                " WHERE gm.status = 1 AND gm.user_id = " + req.body.t_id + gym_sport + game_filter_where +
                    ifdate + " AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time))" +
                " HAVING " + game_distance_heving;
                break;
            }
        case 'watched':
            {
                mysql_query = " SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'id',u.id,'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                " FROM game_watch gw" +
                " LEFT JOIN games gm ON gm.id = gw.game_id" +
                " LEFT JOIN gyms g ON gm.gym_id = g.id" +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id" +
                " LEFT JOIN clubs c ON c.id = gtc.club_id " +
                " WHERE gw.user_id = " + req.body.t_id + gym_sport + game_filter_where +
                ifdate + " AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time))" +
                " HAVING " + game_distance_heving;
                break;
            }
        case 'post':
            {
                mysql_query = " SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'id',u.id,'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" + game_distance_select +
                " FROM games gm" +
                " LEFT JOIN gyms g ON gm.gym_id = g.id" +
                " LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id" +
                " LEFT JOIN clubs c ON c.id = gtc.club_id " +
                " WHERE gm.status = 0 AND gm.user_id = " + req.body.t_id + gym_sport + game_filter_where +
                " HAVING " + game_distance_heving;
                break;
            }
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query(mysql_query, function(err, rows, fields) {
                if (!err) {
                    var global_count = 0;
                    rows.forEach(function(elem, index, array) {

                        rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                        rows[index].members_count = JSON.parse(elem.members_count);

                        global_count += rows[index].members_count;
                        var count = 0;
                        var tmp_arr = [];
                        if (rows[index].members_avatar) {
                            rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                for (var i = 0; i < a_e.count; i++) {
                                    tmp_arr.push({
                                        'img': a_e.img,
                                        'id': a_e.id
                                    })
                                }
                            })
                            rows[index].members_avatar = tmp_arr;
                        }
                        if (rows[index].members_count) {
                            rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                count += parseInt(i_el.count);
                            })
                        }
                        rows[index].global_members = count;
                        rows[index].members_range = range.range(0, rows[index].number_members);

                    })

                    res.json({
                        status: true,
                        rows
                    });
                } else {
                    console.log(err);
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/clubAllGames', function(req, res) {
    var user_id = req.decoded.id;
    var mysql_query = '';
    var game_orderby = '';


    if (req.body.sport != '') {
        var gym_sport = " AND g.gym_sport = '" + req.body.sport + "'";
    }

    if (req.body.dt && req.body.dt != '') {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
    }


    switch (req.body.tab) {
        case 'active':
            {

                mysql_query = " SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" +
                " FROM clubs_to_user ctu" +
                " LEFT JOIN clubs c ON c.id = ctu.club_id " +
                " LEFT JOIN gyms_to_club gtc ON gtc.club_id = c.id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON gm.gym_id = g.id" +
                " WHERE gm.status = 1 AND ctu.user_id = " + user_id + gym_sport +
                " AND gm.date = '" + date + "' AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time))";
                break;
            }
        case 'post':
            {
                mysql_query = " SELECT c.address, g.*, DATE_FORMAT(gm.date, '%Y-%m-%d') AS date, gm.id as game_id, gm.start_time, gm.end_time, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('img', u.avatar, 'count', gms.members) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = gm.id),']') as members_avatar, CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('count', gms.members) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_count" +
                " FROM clubs_to_user ctu" +
                " LEFT JOIN clubs c ON c.id = ctu.club_id " +
                " LEFT JOIN gyms_to_club gtc ON gtc.club_id = c.id" +
                " LEFT JOIN gyms g ON gtc.gym_id = g.id" +
                " LEFT JOIN games gm ON gm.gym_id = g.id" +
                " WHERE gm.status = 0 AND ctu.user_id = " + user_id + gym_sport +
                " AND gm.date = '" + date + "' AND ((gm.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.end_time) OR (" + req.body.start_time + " <= gm.start_time AND " + req.body.end_time + " >= gm.start_time AND " + req.body.end_time + " <= gm.end_time) OR (" + req.body.start_time + " >= gm.start_time AND " + req.body.start_time + " <= gm.end_time AND " + req.body.end_time + " >= gm.end_time))";
                break;
            }
    }


    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query(mysql_query, function(err, rows, fields) {
                if (!err) {
                    var global_count = 0;
                    rows.forEach(function(elem, index, array) {

                        rows[index].members_avatar = JSON.parse(elem.members_avatar); //.replace('\\', '/').split(',')
                        rows[index].members_count = JSON.parse(elem.members_count);

                        global_count += rows[index].members_count;
                        var count = 0;
                        var tmp_arr = [];
                        if (rows[index].members_avatar) {
                            rows[index].members_avatar.forEach(function(a_e, a_i, a_arr) {
                                for (var i = 0; i < a_e.count; i++) {
                                    tmp_arr.push({
                                        'img': a_e.img
                                    })
                                }
                            })
                            rows[index].members_avatar = tmp_arr;
                        }
                        if (rows[index].members_count) {
                            rows[index].members_count.forEach(function(i_el, i_i, i_arr) {
                                count += parseInt(i_el.count);
                            })
                        }
                        rows[index].global_members = count;
                        rows[index].members_range = range.range(0, rows[index].number_members);

                    })

                    res.json({
                        status: true,
                        rows
                    });
                } else {
                    console.log(err);
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.post('/clubFilterNA', function(req, res) {
    var user_id = req.decoded.id,
        club_filter_sql = '';

    console.log(req.body);

    if (req.body.business != '') {
        club_filter_sql += " AND c.name LIKE '%" + req.body.business + "%' OR c.address LIKE '%" + req.body.business + "%'";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.query("SELECT c.id, c.status, c.type, c.latitude, c.longitude, c.name, c.address, c.hours, c.website, ctu.email, ctu.phone," +
                " (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id AND cm.user_id = " + user_id + ") as me_member, (SELECT COUNT(*) FROM clubs_members cm WHERE c.id = cm.club_id) as members, (SELECT GROUP_CONCAT(DISTINCT g.gym_sport ORDER BY g.gym_sport ASC SEPARATOR ',') FROM gyms g LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id  WHERE gtc.club_id = c.id) as sports FROM clubs c LEFT JOIN clubs_to_user ctu ON c.id = ctu.club_id WHERE 1 = 1 " + club_filter_sql,
                function(err, rows, fields) {
                    if (!err) {
                        res.json({
                            status: true,
                            rows
                        });
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/notifications', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT n.*, IF(n.n_key = 'manager', (SELECT c.name FROM games g LEFT JOIN gyms gm ON gm.id = g.gym_id LEFT JOIN gyms_to_club gtc ON gtc.gym_id = gm.id LEFT JOIN clubs c ON c.id = gtc.club_id WHERE g.id = n.sender_id), (SELECT c.name FROM users u LEFT JOIN clubs_to_user ctu ON ctu.user_id = u.id LEFT JOIN clubs c ON c.id = ctu.club_id WHERE u.id = n.sender_id)) as sender_type, " +
                " (SELECT COUNT(ur.id) FROM user_rating ur WHERE ur.game_id = n.sender_id AND ur.to_user_id = " + user_id + " AND n.type = 'end_game') AS u_rating, " +
                " (SELECT COUNT(gf.id) FROM game_feedback gf WHERE gf.game_id = n.sender_id AND gf.to_user_id = " + user_id + " AND n.type = 'end_game') AS u_feedback " +
                " FROM notifications n WHERE n.recipient_id = " + user_id + " AND n.n_key = 'user' ORDER BY n.id DESC",
                function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            res.json(rows);
                        }
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});

router.get('/user-transactions', function(req, res) {
    var user_id = req.decoded.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT * FROM user_transactions ut WHERE ut.user_id=" + user_id + " ORDER BY ut.date DESC", function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json(result);
                }
            })
            connection.release();
        }
    });
});
router.put('/must-start-game', function(req, res) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("UPDATE `games` SET `must_start` = '1' WHERE `id` = " + req.body.game_id, function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json(result);
                }
            })
            connection.release();
        }
    });
})
router.post('/viewNotifications', function(req, res) {
    var user_id = req.decoded.id,
        id = req.body.id;
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("UPDATE `notifications` SET `status` = '1' WHERE `id` = " + id, function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json(result);
                }
            })
            connection.release();
        }
    });
});
router.post('/sendAllMembers', function(req, res) {
    var user_id = req.decoded.id;
    console.log('sendAllMembers');

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.beginTransaction(function(err) {
                if (err) {
                    throw err;
                }

                connection.query("SELECT DISTINCT cm.user_id FROM clubs_to_user ctu LEFT JOIN clubs_members cm ON cm.club_id = ctu.club_id WHERE ctu.user_id = " + user_id, function(err, rows, fields) {
                    if (!err) {
                        if (rows) {
                            var curDate = new Date();
                            var time = curDate.getHours() + '.' + curDate.getMinutes().toFixed(0);
                            var date = formatDate(curDate);
                            var notification = new Notification(sock);
                            console.log('time ', time);
                            var keys = Object.keys(users);
                            rows.forEach(function(e, i, arr) {
                                console.log(e.user_id);
                                notification.addNotification(pool, user_id, 'all_members', e.user_id, 'user', req.body.title, req.body.description, time, date).then(
                                    recordId => {
                                        if (keys.indexOf(e.user)) {
                                            users[e.user_id].forEach(function(u_socket) {
                                                console.log('recordId ', recordId);
                                                notification.sendNotification(pool, u_socket, recordId, e.user_id);
                                            })
                                        }
                                    },
                                    error => {
                                        console.log(error);
                                    }
                                );

                            })
                        }
                        connection.insert('manager_notifications', {
                            manager_id: user_id,
                            title: req.body.title,
                            text: req.body.description
                        }, function(err, result) {
                            if (err) {
                                return connection.rollback(function() {
                                    throw err;
                                });
                            }

                            connection.commit(function(err) {
                                if (err) {
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }

                                res.json({
                                    status: true
                                });
                            });
                        });
                    } else {
                        return connection.rollback(function() {
                            throw err;
                        });
                    }
                });
            });
            connection.release();
        }
    });
});

router.get('/getNDescription/:id', function(req, res) {
    var user_id = req.decoded.id,
        id = req.params.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow("SELECT n.text, n.description FROM notifications n WHERE n.id = " + id, function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

router.get('/getHistory', function(req, res) {
    var user_id = req.decoded.id,
        id = req.params.id;

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("SELECT * FROM manager_notifications WHERE manager_id = " + user_id + " ORDER BY id DESC", function(err, row, fields) {
                if (!err) {
                    if (row) {
                        res.json(row);
                    }
                } else {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                }
            });
            connection.release();
        }
    });
});

function uniq(a, param) {
    return a.filter(function(item, pos, array) {
        return array.map(function(mapItem) {
            return mapItem[param];
        }).indexOf(item[param]) === pos;
    })
}

router.post('/rooms', function(req, res) {
    var id = req.decoded.id;
    var user_id = req.decoded.id;
    var game_filter_where = '',
        game_distance_select = '',
        game_distance_heving = '',
        game_distance_order_by = '',
        game_orderby = '',
        date_where = '',
        gym_sport = '';

    if (typeof req.body.sport == 'object') {
        var sports_types = ['Basketball', 'Volleyball', 'Soccer', 'Bubble Soccer', 'Football', 'Tennis', 'Hockey', 'Baseball/Softball', 'Racquetball', 'Zumba', 'Yoga', 'Pilates', 'Boxing', 'Kickboxing', 'Jazzercise', 'Spin'];
        for (sport_item in req.body.sport) {
            if (req.body.sport[sport_item]) {
                if (gym_sport == '') {
                    gym_sport += " AND (gms.gym_sport = '" + sports_types[sport_item - 1] + "'";
                } else {
                    gym_sport += " OR gms.gym_sport = '" + sports_types[sport_item - 1] + "'";
                }
            }
        }
        if (gym_sport != '') gym_sport += ")";
    }

    if (req.body.dt && req.body.dt != '') {
        var day_name = {
            1: 'mon',
            2: 'tue',
            3: 'wed',
            4: 'thu',
            5: 'fri',
            6: 'sat',
            7: 'sun'
        };
        var date = formatDate(req.body.dt);
        var day_index = new Date(date).getDay();
        var gym_day = day_name[day_index];
        date_where = " g.date='" + date + "' AND "
        if (typeof req.body.start_time == "number" && typeof req.body.end_time == "number") {
            game_filter_where += " ";
        }
    }
    console.log(date);
    //
    // if (req.body.latitude != '' && req.body.longitude != '' && req.body.distance != '') {
    //     game_distance_select = ', CEILING(' +
    //         '3963.1676 * acos ( ' +
    //         'cos ( radians(' + req.body.latitude + ') )' +
    //         '* cos( radians( c.`latitude` ) )' +
    //         '* cos( radians( c.`longitude` ) - radians(' + req.body.longitude + ') )' +
    //         '+ sin ( radians(' + req.body.latitude + ') )' +
    //         '* sin( radians( c.`latitude` ) ) )' +
    //         ') AS distance ';
    //     game_distance_heving = ' CEILING(distance) <= ' + req.body.distance;
    // }
    //
    // if (req.body.sort != '') {
    //     switch (req.body.sort) {
    //         case 'Soonest':
    //         {
    //           game_orderby = " ord ASC";
    //             break;
    //         }
    //         case 'Nearest':
    //         {
    //           game_orderby = 'ord ASC';
    //             break;
    //         }
    //     }
    // }
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            q = connection.query("SELECT " +
                " IFNULL((SELECT MAX(m.date) FROM messages m WHERE m.chat_id = g.id),g.date) as ord, " +
                " g.*, " +
                " IF(g.user_id <>" + id + ",(SELECT gm.id FROM games_members gm WHERE gm.game_id = g.id AND gm.user_id=" + id + " ORDER BY gm.id DESC LIMIT 1),-1) as gmid, " +
                " IF(g.user_id <>" + id + ",(SELECT gm.chat_room_status FROM games_members gm WHERE gm.game_id = g.id  AND gm.user_id=" + id + " ORDER BY gm.id DESC LIMIT 1),g.chat_status) as chat_room_status, " +
                " c.address,   " +
                " gms.gym_sport,  gms.name, " +
                " CONCAT('{','\"id\"',':',u.id,',','\"avatar\"',':','\"',u.avatar,'\"','}') as club_owner, " +
                " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('id', u.id, 'avatar', u.avatar) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = g.id),']') as members_avatar " +
                " FROM games g " +
                " LEFT JOIN gyms_to_club gtc  ON gtc.gym_id = g.gym_id   " +
                " LEFT JOIN gyms gms  ON gms.id = g.gym_id   " +
                " LEFT JOIN clubs c  ON c.id = gtc.club_id  " +
                " LEFT JOIN users u  ON g.user_id = u.id  " +
                " WHERE " + date_where + " (g.user_id =" + id + " OR EXISTS(SELECT * FROM games_members gm WHERE gm.game_id = g.id AND gm.user_id =" + id + ")) AND ((g.start_time <= " + req.body.start_time + " AND " + req.body.start_time + " <= g.end_time AND " + req.body.end_time + " >= g.start_time AND " + req.body.end_time + " <= g.end_time) OR (" + req.body.start_time + " <= g.start_time AND " + req.body.end_time + " >= g.end_time) OR (" + req.body.start_time + " <= g.start_time AND " + req.body.end_time + " >= g.start_time AND " + req.body.end_time + " <= g.end_time) OR (" + req.body.start_time + " >= g.start_time AND " + req.body.start_time + " <= g.end_time AND " + req.body.end_time + " >= g.end_time)) " + gym_sport +
                " ORDER BY ord DESC",
                function(err, result) {
                    if (!err) {
                        // result = mergeByProperty(result[0],rusult[1])
                        console.log();
                        res.json({
                            status: true,
                            rows: result
                        });
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                })
            connection.release();
        }
    });
});

router.route('/rooms/:id')
    .get(function(req, res) {
        var id = req.params.id;
        var from = req.query.from;
        var user_id = req.decoded.id;
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                getRoomInfo = "SELECT " +
                    " IFNULL((SELECT MAX(m.date) FROM messages m WHERE m.chat_id = g.id),g.date) as ord, " +
                    " g.*, " +
                    " IF(g.user_id <>" + user_id + ",(SELECT gm.id FROM games_members gm WHERE gm.game_id = g.id AND gm.user_id=" + user_id + " ORDER BY gm.id DESC LIMIT 1),-1) as gmid, " +
                    " IF(g.user_id <>" + user_id + ",(SELECT gm.chat_room_status FROM games_members gm WHERE gm.game_id = g.id  AND gm.user_id=" + user_id + " ORDER BY gm.id DESC LIMIT 1),g.chat_status) as chat_room_status, " +
                    " c.address,   " +
                    " gms.gym_sport,   " +
                    " CONCAT('{','\"id\"',':',u.id,',','\"avatar\"',':','\"',u.avatar,'\"','}') as club_owner, " +
                    " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('id', u.id, 'avatar', u.avatar) SEPARATOR ',') FROM games_members gms LEFT JOIN users u ON gms.user_id = u.id WHERE gms.game_id = g.id),']') as members_avatar " +
                    " FROM games g " +
                    " LEFT JOIN gyms_to_club gtc  ON gtc.gym_id = g.gym_id   " +
                    " LEFT JOIN gyms gms  ON gms.id = g.gym_id   " +
                    " LEFT JOIN clubs c  ON c.id = gtc.club_id  " +
                    " LEFT JOIN users u  ON g.user_id = u.id  " +
                    " WHERE g.id = " + id + " AND  (g.user_id =" + user_id + " OR EXISTS(SELECT * FROM games_members gm WHERE gm.game_id = g.id AND gm.user_id =" + user_id + ")) " +
                    " ORDER BY ord DESC";
                getMessages = "SELECT m.id as message_id,GROUP_CONCAT(a.src) as attachments,m.*,u.*,IF(EXISTS(SELECT * FROM viewed_messages vm WHERE vm.user_id =" + user_id + " AND vm.message_id = m.id) OR m.sender_id = '" + user_id + "',1,0) as viewed FROM messages m " +
                    " LEFT JOIN users u ON m.sender_id = u.id " +
                    " LEFT JOIN attachments a ON a.owner_id = m.id AND a.type = \"message\"" +
                    " WHERE m.chat_id =" + id + " GROUP BY m.id" +
                    " ORDER BY m.id DESC" +
                    " LIMIT " + from + ", 20 "

                connection.query(getRoomInfo + " ; " + getMessages,
                    function(err, result) {
                        if (!err) {
                            var readed = -1;
                            if (result[1].length) {
                                readed = result[1][0].message_id;
                            }
                            if (readed >= 0) {
                                setReaded(readed, user_id, id)
                            }
                            res.json({
                                status: true,
                                rows: result
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    })
                connection.release();
            }
        });
    })
    .post(function(req, res) {
        var id = req.params.id;
        var user_id = req.decoded.id
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query("UPDATE games_members" +
                    " SET chat_room_status=1" +
                    " WHERE id=" + id,
                    function(err, result) {
                        if (!err) {
                            unreadedCount(user_id)
                            res.json({
                                status: true,
                                rows: result
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    })
                connection.release();
            }

        });
    })
    .delete(function(req, res) {
        var id = req.params.id;
        var user_id = req.decoded.id
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query("UPDATE games_members" +
                    " SET chat_room_status=0" +
                    " WHERE id=" + id,
                    function(err, result) {
                        if (!err) {
                            unreadedCount(user_id)
                            res.json({
                                status: true,
                                rows: result
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    })
                connection.release();
            }
        });
    });
router.route('/roomsm/:id')
    .post(function(req, res) {
        var id = req.params.id;
        var user_id = req.decoded.id
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query("UPDATE games" +
                    " SET chat_status=1" +
                    " WHERE id=" + id,
                    function(err, result) {
                        if (!err) {
                            unreadedCount(user_id)
                            res.json({
                                status: true,
                                rows: result
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    })
                connection.release();
            }

        });
    })
    .delete(function(req, res) {
        var id = req.params.id;
        var user_id = req.decoded.id
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query("UPDATE games" +
                    " SET chat_status=0" +
                    " WHERE id=" + id,
                    function(err, result) {
                        if (!err) {
                            unreadedCount(user_id)
                            res.json({
                                status: true,
                                rows: result
                            });
                        } else {
                            console.log(err);
                            res.json({
                                status: false,
                                message: 'Db error',
                                field: 'db'
                            });
                        }
                    })
                connection.release();
            }
        });
    });
router.get('/rooms/:id/members', function(req, res) {
    var user_id = req.decoded.id;
    var room_id = req.params.id;
    var sql = '';
    if (req.query.name != '') {
        sql = " AND (u.first_name like '%" + req.query.name + "%' OR u.last_name like '%" + req.query.name + "%')";
    }

    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            q = connection.query("SELECT u.* FROM games_members gm " +
                " LEFT JOIN users u" +
                " ON  gm.user_id = u.id" +
                " WHERE " +
                " gm.game_id = " + room_id + ' ' + sql + "GROUP BY u.id",
                function(err, rows, fields) {
                    if (!err) {
                        res.json(rows);
                    } else {
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                });
            connection.release();
        }
    });
});
app.use('/api', router);

var notificationUnreaded = function(id) {
    var user_id = id
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow("SELECT count(*) AS count FROM notifications n WHERE n.`status` = 0 AND n.n_key = 'user' AND n.`recipient_id` = " + id + "",
                function(err, result) {
                    if (!err) {
                        console.log(id);
                        console.log(Array.isArray(users[user_id]));
                        if (users[user_id]) {
                            users[user_id].forEach(function(socket) {
                                socket.emit('notificationUnreaded', result)
                            })
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                })
            connection.release();
        }

    });
}

var unreadedCount = function(id) {
    var user_id = id
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            if (typeof user_id == 'undefined') {
                user_id = 0
            }
            q = connection.query("SELECT COUNT(m.id) as unreadedCount, g.id as game_id FROM games g " +
                " LEFT JOIN messages m  ON  m.chat_id = g.id AND m.sender_id <> " + user_id + "  " +
                " WHERE ((g.user_id = " + user_id + "  AND g.chat_status = 1) OR(EXISTS(SELECT * FROM games_members gm WHERE gm.user_id=" + user_id + " AND gm.chat_room_status = 1 AND gm.game_id = g.id)))  AND  NOT EXISTS(SELECT * FROM viewed_messages vm WHERE vm.user_id = " + user_id + " AND vm.message_id = m.id) " +
                " GROUP BY g.id",
                function(err, result) {
                    if (!err) {
                        var unreaded = {}
                        if (result) {
                            result.forEach(function(row) {
                                unreaded[row.game_id] = row.unreadedCount
                            })
                        }
                        if (users[user_id]) {
                            users[user_id].forEach(function(socket) {
                                socket.emit('unreaded count', unreaded)
                            })
                        }
                    } else {
                        console.log(err);
                        res.json({
                            status: false,
                            message: 'Db error',
                            field: 'db'
                        });
                    }
                })
            connection.release();
        }

    });
}
var setReaded = function(message_id, user_id, chat_id) {
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.query("INSERT INTO viewed_messages (user_id,message_id) (SELECT \"" + user_id + "\", m.id FROM messages m WHERE m.chat_id = '" + chat_id + "' AND m.sender_id <> '" + user_id + "' AND m.id <= '" + message_id + "' AND NOT EXISTS(SELECT id FROM viewed_messages WHERE user_id = '" + user_id + "' AND message_id = m.id))",
                    function(err, result) {
                        if (!err) {
                            unreadedCount(user_id)
                        } else {
                            console.log(err);
                        }
                    })
                connection.release();
            }

        });
    }
    // START THE SERVER
    // =============================================================================

var server = https.createServer({
        key: fs.readFileSync('./keys/private/gymventory.com.key').toString(),
        cert: fs.readFileSync('./keys/private/gymventory.com.crt').toString(),
        ca: [fs.readFileSync('./keys/private/sf_bundle1.crt').toString(),
            fs.readFileSync('./keys/private/sf_bundle2.crt').toString(),
            fs.readFileSync('./keys/private/sf_bundle3.crt').toString()
        ],
        passphrase: 'gymventory'
    }, app),
    io = require('socket.io')(server);
var arr_tmp = [42];
server.listen(port, function() {
    /*pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.query("UPDATE `games` SET `status` = '0' WHERE `games`.`date` < '" + formatDate(new Date()) + "'", function(err, result) {
                if (err) {
                    console.log(err);
                }
            })
        }
        connection.release();
    });*/

    /*
     WHERE `date` >= '2016-10-14'
    */
    console.log('Magic happens on port ' + port);
})


var users = {};
var filaments = [];
var sock = {};
io.on('connection', function(socket) {

    sock = socket;
    console.log('socket id-2 ', socket.id);

    // socket.emit('init', {name: 'server', users: "test"});
    socket.on('login', function(data) {
        if (typeof users[data.user_id] == 'undefined') {
            users[data.user_id] = []
            users[data.user_id].push(socket)
        } else {
            users[data.user_id].push(socket)
        }
        Object.keys(users).forEach(function(key) {
            users[key].forEach(function(socket) {
                socket.emit('online', Object.keys(users))
            })
        })
        filaments[socket.id] = data.user_id;
        unreadedCount(data.user_id);

        notificationUnreaded(data.user_id);
    });
    socket.on('logout', function(data) {
        var user_id = filaments[socket.id]
        if (typeof user_id != 'undefined') {
            users[user_id].splice(users[user_id].indexOf(socket), 1)
            delete filaments[socket.id]
            if (users[user_id].length === 0) {
                delete users[user_id]
            }
            Object.keys(users).forEach(function(key) {
                users[key].forEach(function(socket) {
                    socket.emit('online', Object.keys(users))
                })
            })
        }
    });

    socket.on('set readed', function(data) {
        var message_id = data.message_id;
        var chat_id = data.chat_id
        var user_id = filaments[socket.id]
        setReaded(message_id, user_id, chat_id)
    });
    socket.on('get unreaded', function(data) {
        var user_id = filaments[socket.id]
        unreadedCount(user_id)
    });

    socket.on('sendMessage', function(data) {
        var user_id = filaments[socket.id]
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                insertMessage = "INSERT messages " +
                    " SET message='" + data.message +
                    "' , sender_id=" + user_id +
                    " , chat_id=" + data.room_id;
                connection.query(insertMessage,
                    function(err, result) {
                        if (!err) {
                            getChatUsers = "SELECT u.* FROM users u " +
                                " WHERE   " +
                                " EXISTS(SELECT * FROM games g WHERE g.id=" + data.room_id + " AND g.user_id = u.id AND g.chat_status = 1) OR " +
                                " EXISTS(SELECT * FROM games_members gm WHERE gm.game_id = " + data.room_id + " AND gm.user_id = u.id AND gm.chat_room_status = 1) ";
                            getMessage = "SELECT m.id as message_id,GROUP_CONCAT(a.src) as attachments,m.*,u.* ,IF(EXISTS(SELECT * FROM viewed_messages vm WHERE vm.user_id =" + user_id + " AND vm.message_id = m.id) OR m.sender_id = '" + user_id + "',1,0) as viewed FROM messages m " +
                                " LEFT JOIN users u ON m.sender_id = u.id " +
                                " LEFT JOIN attachments a ON a.owner_id = m.id AND a.type = \"message\"" +
                                " WHERE m.id =" + result.insertId + " GROUP BY m.id"
                            insertAttachments = "INSERT INTO attachments (src, owner_id, type) VALUES ?"
                            var values = [];
                            data.attachments.forEach(function(att) {
                                values.push([
                                    att,
                                    result.insertId,
                                    'message'
                                ])
                            })
                            var values = [];
                            var insertAttachments = 'SELECT "foo"'
                            if (data.attachments.length) {
                                insertAttachments = "INSERT INTO attachments (src, owner_id, type) VALUES ?"
                                data.attachments.forEach(function(att) {
                                    values.push([
                                        att,
                                        result.insertId,
                                        'message'
                                    ])
                                })
                            }
                            q = connection.query(getChatUsers + " ; " + insertAttachments + ";" + getMessage, [values], function(err, result) {
                                if (!err) {
                                    result[0].forEach(function(user) {
                                        if (typeof users[user.id] != 'undefined') {
                                            if (user.id == user_id) {
                                                users[user_id].forEach(function(socket) {
                                                    socket.emit('message delivered', result[2][0])
                                                    console.log('6170 message delivered', socket.id, new Date().getMilliseconds());
                                                })
                                            } else {
                                                users[user.id].forEach(function(socket) {
                                                    socket.emit('recive message', result[2][0])
                                                    console.log('6174 recive message', socket.id, new Date().getMilliseconds());
                                                })
                                            }
                                        }
                                    })
                                } else {
                                    console.log(err);
                                    socket.emit('send-message-error', {
                                        message: "Db error"
                                    })
                                }
                            })
                        } else {
                            console.log(err);
                            socket.emit('send-message-error', {
                                message: "Db error"
                            })
                        }
                    })
            }
            connection.release();
        });
    });
    socket.on('typing', function(data) {
        var user_id = filaments[socket.id]
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                getChatUsers = "SELECT u.* FROM users u " +
                    " WHERE   " +
                    " EXISTS(SELECT * FROM games g WHERE g.id=" + data.chat_id + " AND g.user_id = u.id AND g.chat_status = 1) OR " +
                    " EXISTS(SELECT * FROM games_members gm WHERE gm.game_id = " + data.chat_id + " AND gm.user_id = u.id AND gm.chat_room_status = 1) ";
                q = connection.query(getChatUsers, function(err, result) {
                    if (!err) {
                        var typingUser = result[findWithAttr(result, 'id', user_id)]
                        result.forEach(function(user) {
                            if (typeof users[user.id] != 'undefined') {
                                if (user.id == user_id) {} else {
                                    users[user.id].forEach(function(socket) {
                                        socket.emit('typing-response', {
                                            user: typingUser,
                                            chat_id: data.chat_id
                                        })
                                    })
                                }
                            }
                        })
                    } else {
                        console.log(err);
                        socket.emit('typing-error', {
                            message: "Db error"
                        })
                    }
                })
                connection.release();
            } else {
                console.log(err);
                socket.emit('send-message-error', {
                    message: "Db error"
                })
            }
        })
    });
    socket.on('disconnect', function() {
        var user_id = filaments[socket.id]
        if (typeof user_id != 'undefined') {
            users[user_id].splice(users[user_id].indexOf(socket), 1)
            delete filaments[socket.id]
            if (users[user_id].length === 0) {
                delete users[user_id]
            }
            Object.keys(users).forEach(function(key) {
                users[key].forEach(function(socket) {
                    socket.emit('online', Object.keys(users))
                })
            })
        }
    });
});

function change_balance(balance, u_id, t_data, callback) {
    console.log('balance ************************* ', t_data.credits)
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.queryRow('SELECT balance FROM user_credits WHERE user_id ="' + u_id + '"', function(err, results) {
                if (err) {
                    res.json({
                        status: false,
                        message: 'Db error',
                        field: 'db'
                    });
                } else {
                    if (!results) {
                        connection.beginTransaction(function(err) {
                            if (err) {
                                throw err;
                            }
                            connection.insert('user_credits', {
                                user_id: u_id,
                                balance: t_data.credits
                            }, function(err, result) {
                                if (err) {
                                    console.log(err);
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }

                                connection.insert('user_transactions', {
                                    user_id: u_id,
                                    date: t_data.date,
                                    credits: t_data.credits,
                                    card: t_data.card,
                                    payment_id: t_data.payment_id,
                                    fees: t_data.fees,
                                    action: t_data.action
                                }, function(err, result) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    connection.commit(function(err) {
                                        if (err) {
                                            console.log(err);
                                            callback(false);
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        } else {
                                            callback(true);
                                        }
                                    });
                                });
                            });
                        });
                    } else if (results.balance >= 0) {

                        var credit_count = parseFloat(results.balance) + parseFloat(t_data.credits);
                        console.log('results.balance -------------------------- ', parseFloat(results.balance));
                        console.log('credit_count ----------------------------- ', credit_count);
                        connection.beginTransaction(function(err) {
                            if (err) {
                                throw err;
                            }

                            connection.update('user_credits', {
                                balance: credit_count
                            }, {
                                user_id: u_id
                            }, function(err, result) {
                                if (err) {
                                    console.log(err);
                                    return connection.rollback(function() {
                                        throw err;
                                    });
                                }

                                connection.insert('user_transactions', {
                                    user_id: u_id,
                                    date: t_data.date,
                                    credits: t_data.credits,
                                    card: t_data.card,
                                    payment_id: t_data.payment_id,
                                    fees: t_data.fees,
                                    action: t_data.action
                                }, function(err, result) {
                                    if (err) {
                                        return connection.rollback(function() {
                                            throw err;
                                        });
                                    }
                                    connection.commit(function(err) {
                                        if (err) {
                                            console.log(err);
                                            callback(false);
                                            return connection.rollback(function() {
                                                throw err;
                                            });
                                        } else {
                                            callback(true);
                                        }
                                    });
                                });
                            });
                        })
                    }
                }
            });
            connection.release();
        }
    });
}

function CreateGameTask(type, gym_id, game_id, start_time, end_time, date) {
    switch(type){
        case 'game_start': {
            console.log(type, gym_id, game_id, start_time, end_time, date);
            getSettings('notice', 'game_start_time').then(
                result => {
                    return result;
                }
            ).then(
                correction => {
                    console.log('s_game_start_time ', correction);
                    check_task({
                        game_id: game_id,
                        gym_id: gym_id
                    }, start_time, date, correction, 'start', function(data) {
                        console.log('check_task start', data)
                        if (!data.error) {
                            setCron(data);
                        }
                    });
                }
            );

            getSettings('notice', 'game_end_time').then(
                result => {
                    return result;
                }
            ).then(
                correction => {
                    console.log('s_game_end_time ', correction);
                    console.log('s_end_time ', end_time);
                    check_task({
                        game_id: game_id,
                        gym_id: gym_id
                    }, end_time, date, correction, 'end', function(data) {
                        console.log('check_task end', data)
                        if (!data.error) {
                            setCron(data);
                        }
                    });
                }
            );
        }
        case 'game_create': {
            getSettings('notice', 'min_members_time').then(
                result => {
                    return result;
                }
            ).then(
                correction => {
                    console.log('s_min_members_time ', correction);
                    check_task({
                        game_id: game_id,
                        gym_id: gym_id
                    }, start_time, date, correction, 'members', function(data) {
                        console.log('check_task_members', data);
                        console.log('check_task_correction', correction);
                        setCron(data);
                    });
                }
            );
        }
    }


}

function check_task(game, time, date, correction, type, callback) {
    var curDate = new Date(date);
    var task = {
        type: type + '_game',
        game_id: game.game_id,
        gym_id: game.gym_id,
        minute: parseInt(time.toString().split('.')[1]),
        hour: parseInt(time.toString().split('.')[0]),
        day: curDate.getDate(),
        month: (curDate.getMonth() + 1),
        date: date
    }
    if (isNaN(task.minute)) {
        task.minute = 0;
    }

    console.log('task ', task);

    var promise = new Promise((resolve, reject) => {
        switch (type) {
            case 'start':
                {
                    console.log('start_correction ', correction);
                    var minute = parseInt(task.minute) - parseInt(correction);
                    if (minute < 0) {
                        task.hour--;
                        task.minute = 60 + parseInt(minute);
                    } else {
                        task.minute = minute;
                    }
                    resolve(task);
                    break;
                }
            case 'members':
                {
                    console.log('members_correction ', correction);
                    var minute = parseInt(task.minute) - parseInt(correction);
                    if (minute < 0) {
                        task.hour--;
                        task.minute = 60 + parseInt(minute);
                    } else {
                        task.minute = minute;
                    }
                    resolve(task);
                    break;
                }
            case 'end':
                {
                    console.log('end_correction ', correction);
                    var minute = parseInt(task.minute) + parseInt(correction);
                    if (minute >= 60) {
                        task.hour++;
                        task.minute = parseInt(minute) - 60;
                    } else {
                        task.minute = minute;
                    }
                    resolve(task);
                    break;
                }
        }
    })

    promise.then(
        task => {
            pool.getConnection(function(err, connection) {
                if (!err) {
                    mysqlUtilities.upgrade(connection);
                    mysqlUtilities.introspection(connection);

                    connection.queryRow('SELECT g.min_members, ' +
                        ' IFNULL((SELECT SUM(gm.members) FROM games_members gm WHERE gm.game_id = ' + game.game_id + '), 0) AS members, ' +
                        ' (SELECT COUNT(ct.id) FROM cron_tasks ct WHERE ct.type = \'' + task.type + '\' AND ct.minute = ' + task.minute + ' AND ct.hour = ' + task.hour + ' AND ct.month = ' + task.month + ' AND game_id = ' + game.game_id + ') AS task ' +
                        ' FROM gyms g WHERE g.id = ' + game.gym_id,
                        function(err, row) {
                            if (!err) {
                                var membersTmp = 0;
                                console.log('row.min_members ', row.min_members);
                                console.log('row.members ', row.members);
                                if (type != 'members') {
                                    if (row.min_members != null) {
                                        membersTmp = row.members;
                                    }
                                    if (row.min_members <= membersTmp && row.task == 0) {
                                        callback(task);
                                    } else {
                                        callback({
                                            error: 'min members'
                                        });
                                    }
                                } else if (type == 'members') {
                                    if (row.task == 0) {
                                        callback(task);
                                    } else {
                                        callback({
                                            error: 'members'
                                        });
                                    }
                                }
                            }
                        })
                } else {
                    callback.call({
                        error: err
                    });
                }
                connection.release();
            })
        }
    )
}

function setCron(obj) {
    console.log('setCron ', obj)
    console.log('soket id-1 - ', sock.id);
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.insert('cron_tasks', {
                type: obj.type,
                game_id: obj.game_id,
                minute: obj.minute,
                hour: obj.hour,
                month: obj.month
            }, function(err, recordId) {
                if (!err) {
                    cron.schedule('0 ' + obj.minute + ' ' + (obj.hour == 24)?0:obj.hour + ' ' + obj.day + ' ' + obj.month + ' *', function() {
                        var notification = new Notification(sock);
                        switch (obj.type) {
                            case 'start_game':
                                {
                                    notification.startAllPlayers(pool, obj, users).then(
                                        response => {
                                            console.log(response);
                                            removeTask(recordId);
                                        },
                                        errore => {
                                            console.log(errore);
                                        }
                                    );
                                    break;
                                }
                            case 'members_game':
                                {
                                    backMonay(obj.gym_id);
                                    notification.minMembers(pool, obj, users).then(
                                        response => {
                                            console.log(response);
                                            removeTask(recordId);
                                        },
                                        errore => {
                                            console.log(errore);
                                        }
                                    );
                                    break;
                                }
                            case 'end_game':
                                {
                                    notification.endGamePlayers(pool, obj, users).then(
                                        response => {
                                            console.log(response);
                                            removeTask(recordId);
                                        },
                                        errore => {
                                            console.log(errore);
                                        }
                                    );
                                    break;
                                }
                        }

                        console.log('running every ' + obj.hour + ':' + obj.minute);
                    });
                    console.log("Task added!");
                } else {
                    console.log('setCron: ', err);
                }
            })
            connection.release();
        }

    });
}

function getSettings(code, key) {
    var result = {
        time: 0
    };
    return new Promise(function(resolve, reject) {
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.queryRow("SELECT s.s_value from settings s WHERE s.s_code = '" + code + "' AND s.s_key = '" + key + "'", function(err, row, fields) {
                    if (!err) {
                        if (row) {
                            result.time = row.s_value;
                            resolve(result.time)
                        }
                    } else {
                        result.time = false;
                        reject(err)
                    }
                });
                connection.release();
                return result.time;
            }
        });
    });
}

function removeTask(id) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

            connection.delete('cron_tasks', {
                id: id
            }, function(err, affectedRows) {
                console.dir({
                    delete: affectedRows
                });
            });
            connection.release();
        }
    });
}

function backMonay(gym_id) {
    pool.getConnection(function(err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);

                connection.queryRow("SELECT " +
                    "CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('user', gms.user_id,'count', gms.members, 'sum', IF((SELECT COUNT(*) FROM clubs_members cm WHERE cm.user_id = gms.user_id AND cm.club_id = gtc.club_id)>0, gm.price_member*gms.members, gm.price_hours*gms.members)) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as members_json " +
                    "FROM `games` gm " +
                    "LEFT JOIN gyms g ON g.id = gm.gym_id " +
                    "LEFT JOIN gyms_to_club gtc ON gtc.gym_id = g.id " +
                    "WHERE gm.gym_id = " + gym_id + " AND gm.status = 1 AND gm.must_start = 0 ORDER BY gm.`id` DESC",
                    function (err, row) {
                        if (!err) {
                            if(JSON.parse(row.members_json)) {
                                JSON.parse(row.members_json).forEach(function (el, index, arr) {
                                    var t_data = {
                                        date: formatDate(new Date()),
                                        credits: el.sum,
                                        card: '',
                                        payment_id: '',
                                        fees: 0,
                                        action: 'repayment'
                                    }
                                    change_balance(0, el.user, t_data, function (data) {
                                        console.log(data);
                                    });
                                })
                            }
                        } else {
                            return connection.rollback(function () {
                                throw err;
                            });
                        }
                    })
        } else {

        }
        connection.release();
    })
}
function calcPriceGame(t_start, t_end, p_nmember, p_member, callback) {
    var hours = 1,
        p1 = p_nmember,
        p2 = p_member;
    var minutes = 1;
    var minute1 = 0,
        minute2 = 0;
    var nm_to1 = p_nmember / 60;
    var m_to1 = p_member / 60;
    if (t_start) {
        var hourTmp1 = (t_start).toString().split('.');
        if (Array.isArray(hourTmp1) && hourTmp1.length >= 2) {
            minute1 = hourTmp1[1];
        }
    }

    if (t_end) {
        var hourTmp2 = (t_end).toString().split('.');
        if (Array.isArray(hourTmp2) && hourTmp2.length >= 2) {
            minute2 = hourTmp2[1];
        }
    }
    if (minute1 == 0 && minute2 == 0) {
        hours = parseInt(t_end) - parseInt(t_start);
    } else if (minute2 >= minute1) {
        hours = parseInt(t_end) - parseInt(t_start);
        minutes = minute2 - minute1;
    } else if (minute2 <= minute1) {
        hours = (parseInt(t_end) - parseInt(t_start)) - 1;
        minutes = 60 + (minute2 - minute1);
    }
    console.log("hours", hours);
    console.log("minutes", minutes);

    p1 = ((hours * p_nmember) + (minutes * nm_to1)).toFixed(2);
    p2 = ((hours * p_member) + (minutes * m_to1)).toFixed(2);

    callback({
        'p1': p1,
        'p2': p2
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function setCronStartGame(game, date, time) {
    var curDate = new Date(date);
    var task = {
        game_id: game,
        minute: 0,
        hour: time,
        day: curDate.getDate(),
        month: (curDate.getMonth() + 1)
    }

    var hourTmp = (time).toString().split('.');
    if (Array.isArray(hourTmp) && hourTmp.length >= 2) {
        task.minute = hourTmp[1]
        task.hour = hourTmp[0]
    }
    console.log('setCronStartGame', task);

    cron.schedule('0 ' + task.minute + ' ' + task.hour + ' ' + task.day + ' ' + task.month + ' *', function() {
        pool.getConnection(function(err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                console.log('here');
                connection.query("UPDATE `games` SET `status` = '0' WHERE `games`.`id` = " + task.game_id, function(err, result) {
                    if (err) {
                        console.log(err);
                    }
                })
            }
            connection.release();
        });
    });
}
