var mysqlUtilities = require('mysql-utilities');

function Notification(sock){
    this.socket = sock;

}

Notification.prototype.startAllPlayers = function(pool, game, users){
    var socket = this.socket;
    var self = this;
    return new Promise((resolve, reject) => {
        pool.getConnection(function (err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.queryRow("SELECT DISTINCT CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('user', gms.user_id) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as users " +
                    " FROM games gm WHERE gm.id = " + game.game_id, function (err, row) {
                    if (!err) {
                        game_users = JSON.parse(row.users);
                        var keys = Object.keys(users);
                        if(game_users){
                            game_users.forEach(function(e, i, arr){
                                addSystemNotification(pool, game, e.user, 'user', 'Game start!').then(
                                    function(recordId){
                                        if(keys.indexOf(e.user)){
                                            console.log('e.user ', e.user);
                                            users[e.user].forEach(function (u_socket) {
                                                self.sendNotification(pool, u_socket, recordId, e.user);
                                            });
                                        }
                                    },
                                    function(error){
                                        console.log(error)
                                    }
                                )

                            });
                        }
                        resolve('norification start sended');
                    }else{
                        reject(err);
                    }
                })
            } else {
                reject(err);
            }
            connection.release();
        })
    })
};
Notification.prototype.endGamePlayers = function(pool, game, users){
    var socket = this.socket;
    var self = this;
    return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.queryRow("SELECT DISTINCT (SELECT ctu.user_id " +
                    " FROM games gm2 " +
                    " LEFT JOIN gyms_to_club gtc ON gm2.gym_id = gtc.gym_id" +
                    " LEFT JOIN clubs c ON c.id = gtc.club_id" +
                    " LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id" +
                    " WHERE gm2.id = " + game.game_id + ") as manager, " +
                    " CONCAT('[',(SELECT GROUP_CONCAT( JSON_OBJECT('user', gms.user_id) SEPARATOR ',') FROM games_members gms WHERE gms.game_id = gm.id),']') as users " +
                    " FROM games gm WHERE gm.id = " + game.game_id, function (err, row) {
                    if (!err) {
                        game_users = JSON.parse(row.users);
                        var keys = Object.keys(users);
                        if(game_users){
                            game_users.forEach(function(e, i, arr){
                                addSystemNotification(pool, game, e.user, 'user', 'Game over! Leave your comment.').then(
                                    function(recordId){
                                        if(keys.indexOf(e.user)){
                                            console.log('e.user ', e.user);
                                            users[e.user].forEach(function (u_socket) {
                                                self.sendNotification(pool, u_socket, recordId, e.user);
                                                //u_socket.emit('notification', {type: game.type, game_id: game.game_id});
                                            })

                                        }
                                    },
                                    function(error){
                                        console.log(error);
                                    }
                                )

                            });
                        }
                        if(row.manager){
                            addSystemNotification(pool, game, row.manager, 'manager', 'Game over! Leave your comment.').then(
                                function(recordId){
                                    if(keys.indexOf(row.manager)){
                                        console.log('row.manager ', row.manager);
                                        users[row.manager].forEach(function (u_socket) {
                                            self.sendNotification(pool, u_socket, recordId, e.user);
                                            //u_socket.emit('notification', {type: game.type, game_id: game.game_id});
                                        })

                                    }
                                },
                                function(error){
                                    console.log(error);
                                }
                            )

                        }
                        resolve('norification end sended');
                    }else{
                        reject(err);
                    }
                })
            } else {
                reject(err);
            }
            connection.release();
        })
    })
};


Notification.prototype.minMembers = function(pool, game, users){
    var socket = this.socket;
    return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);

                connection.queryRow("SELECT ctu.user_id " +
                    " FROM games gm " +
                    " LEFT JOIN gyms_to_club gtc ON gm.gym_id = gtc.gym_id" +
                    " LEFT JOIN clubs c ON c.id = gtc.club_id" +
                    " LEFT JOIN clubs_to_user ctu ON ctu.club_id = c.id" +
                    " WHERE gm.id = " + game.game_id, function (err, row) {
                    if (!err) {
                        var keys = Object.keys(users);
                        if(row.user_id){
                            addSystemNotification(pool, game, row.user_id, 'manager', 'Minimum players!').then(
                                function(recordId){
                                    if(keys.indexOf(row.user_id)){
                                        console.log('row.user_id ', row.user_id);
                                        users[row.user_id].forEach(function (u_socket) {
                                            self.sendNotification(pool, u_socket, recordId, e.user);
                                        })

                                    }
                                },
                                function(error){
                                    console.log(error);
                                }
                            )

                        }
                        resolve('norification members sended');
                    }else{
                        reject(err);
                    }
                })
            } else {
                reject(err);
            }
            connection.release();
        })
    })
};

Notification.prototype.sendNotification = function(pool, u_socket, id, user_id){
    console.log('sendNotification');
    pool.getConnection(function (err, connection) {
        if (!err) {
            mysqlUtilities.upgrade(connection);
            mysqlUtilities.introspection(connection);
            connection.queryRow("SELECT n.*, " +
                " (SELECT COUNT(ur.id) FROM user_rating ur WHERE ur.game_id = n.sender_id AND ur.to_user_id = " + user_id + " AND n.type = 'end_game') AS u_rating, " +
                " (SELECT COUNT(gf.id) FROM game_feedback gf WHERE gf.game_id = n.sender_id AND gf.to_user_id = " + user_id + " AND n.type = 'end_game') AS u_feedback " +
                " FROM notifications n WHERE n.id = " + id, function (err, row, fields) {
                if (!err) {
                    if (row) {
                        u_socket.emit('notification', row)
                    }
                } else {
                    console.log(err);
                }
            });
            connection.release();
        }
    });
};

Notification.prototype.addNotification = function(pool, sender, type, user, key, text, description, time, date){
    return new Promise(function(resolve, reject){
        pool.getConnection(function (err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.insert('notifications', {
                    notification: 'game',
                    type: type,
                    text: text,
                    description: description,
                    n_key: key,
                    sender_id: sender,
                    recipient_id: user,
                    time: time,
                    date: date
                }, function (err, recordId) {
                    if (!err) {
                        resolve(recordId);
                    } else {
                        console.log(err);
                        reject(err);
                    }
                });
                connection.release();
            }

        });
    })
}
addSystemNotification = function(pool, game, user, key, text){
    return new Promise(function(resolve, reject){
        pool.getConnection(function (err, connection) {
            if (!err) {
                mysqlUtilities.upgrade(connection);
                mysqlUtilities.introspection(connection);
                connection.insert('notifications', {
                    notification: 'game',
                    type: game.type,
                    text: text,
                    n_key: key,
                    sender_id: game.game_id,
                    recipient_id: user,
                    time: game.hour+'.'+game.minute,
                    date: game.date
                }, function (err, recordId) {
                    if (!err) {
                        console.log("Task added!");
                        resolve(recordId);
                    } else {
                        console.log(err);
                        reject(err);
                    }
                });
                connection.release();
            }

        });
    })
};

module.exports = Notification;