controllers

    .controller('paypal_callbackCtrl', function($scope, $rootScope, $location, $state, $timeout, paypalFactory, $cookies,  clubFactory) {
        $scope.paypal_url = $location.search().paypal_url;

        $scope.loading_spiner = false;
        $scope.success_text = false;

        $timeout(function () {
            switch($scope.paypal_url){
                case 'success': {
                    $scope.success_text = false;
                    $scope.loading_spiner = true;
                    var data = {
                        credits: paypalFactory.getCredits(),
                        payment_id:  $location.search().paymentId,
                        type: 'pay_credit'
                    };
                    //console.log(data);
                    paypalFactory.successPayment(data).then(
                        function(response){
                            if(response.status){
                                $scope.success_text = true;
                                $scope.loading_spiner = false;
                                $scope.inform_text = 'Payment successful!';
                                $scope.$emit('setBalance', response.credits);
                                $timeout(function(){
                                    $state.go('app.home');
                                }, 1000);
                            }
                            console.log(response);
                        }
                    );
                    break;
                }
                case 'game_success': {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    if($cookies.get('pp_id') == $location.search().paymentId){
                        var data_tmp = $location.search();
                        data_tmp.type = 'pay_game';
                        delete data_tmp.token;
                        paypalFactory.successPayment(data_tmp).then(
                            function(response){
                                if(response.status){
                                    $scope.success_text = true;
                                    $scope.loading_spiner = false;
                                    $scope.inform_text = 'Payment game successful!';
                                    $scope.$emit('resetBalance');
                                    $timeout(function(){
                                        $state.go('app.home');
                                    }, 1000);
                                }
                                console.log(response);
                            }
                        );
                    }
                    console.log($location.search().PayerID);
                    break;
                }
                case 'club_success': {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    if($cookies.get('pp_id') == $location.search().paymentId){
                        var data_tmp = $location.search();
                        delete data_tmp.token;

                        clubFactory.addMember(data_tmp).then(
                            function(response){
                                if(response.status){
                                    $scope.success_text = true;
                                    $scope.loading_spiner = false;
                                    $scope.inform_text = 'Payment game successful!';
                                    $timeout(function(){
                                        $state.go('app.search');
                                    }, 1000);
                                }
                            }
                        );
                    }
                    console.log($location.search().PayerID);
                    break;
                }
                case 'cancel': {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment declined, try again later!';
                    $timeout(function(){
                        $state.go('app.home');
                    }, 3000);
                    break;
                }
            }
        });
    });