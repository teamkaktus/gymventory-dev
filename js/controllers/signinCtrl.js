controllers

    .controller('signinCtrl', function($scope, $rootScope, $state, AuthFactory, Facebook, ngDialog) {
        // This is the success callback from the login method
        // $ionicLoading.hide()

        $scope.user = {};

        // Defining user logged status
        $scope.logged = false;

        // And some fancy flags to display messages upon user status change
        $scope.byebye = false;
        $scope.salutation = false;

        /**
         * Watch for Facebook to be ready.
         * There's also the event that could be used
         */
        $scope.$watch(
            function() {
                return Facebook.isReady();
            },
            function(newVal) {
                if (newVal)
                    $scope.facebookReady = true;
            }
        );

        var userIsConnected = false;

        Facebook.getLoginStatus(function(response) {
            if (response.status == 'connected') {
                userIsConnected = true;
            }
        });

        /**
         * IntentLogin
         */
        $scope.IntentLogin = function() {
            if(!userIsConnected) {
                $scope.login();
            }
        };

        /**
         * Login
         */
        $scope.login = function() {
            Facebook.login(function(response) {
                if (response.status == 'connected') {
                    $scope.logged = true;
                    $scope.me();
                }else{
                    //$ionicLoading.hide();
                    AuthFactory.removeToken();
                }
                console.log(response);

            },{
                auth_type: 'rerequest',
                scope: 'email, user_friends'
            });
        };

        /**
         * me
         */
        $scope.me = function() {
            Facebook.api('/me?fields=email,name,id', function(response) {
                /**
                 * Using $scope.$apply since this happens outside angular framework.
                 */
                $scope.$apply(function() {
                    $scope.user = response;
                    console.log(response);

                    if(response.error){
                        //$ionicLoading.hide();
                        alert(response.error.message);

                    }else {
                        AuthFactory.authenticateSocial(response)
                            .then(
                                function (response) {
                                    $scope.error = [];
                                    if (response.data.status) {
                                        console.log(response.data.token);
                                        //$ionicLoading.hide();
                                        AuthFactory.setToken(response.data.token);
                                        console.log(response.data.token);
                                        $state.go('app.home', {}, {reload: true});
                                    } else {
                                        $scope.error[response.data.field] = response.data.message;
                                        //$ionicLoading.hide();
                                    }
                                },
                                function (error) {
                                    /*$ionicLoading.hide();*/
                                    console.log('error');
                                }
                            );
                        $scope.logout();
                    }

                });

            });
        };

        $scope.logout = function() {
            Facebook.logout(function() {
                $scope.$apply(function() {
                    $scope.user   = {};
                    $scope.logged = false;
                });
            });
        }

        $scope.facebookSignIn = function() {
            /*$ionicLoading.show({
             template: 'Logging in...'
             });*/

            $scope.login();
            // Ask the permissions you need. You can learn more about
            // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4

        };

        $scope.clickToOpen = function () {
            $scope.blurBox = {
                "filter": "blur(3px)",
                "-webkit-filter": "blur(3px)",
                "-ms-filter": "blur(3px)"
            };
            var dialog = ngDialog.open({
                template: 'popupTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'signinCtrl'
            });
            dialog.closePromise.then(function () {
                $scope.blurBox = {};
            });
        };

        $scope.ForgotOpen = function () {
            $scope.blurBox = {
                "filter": "blur(3px)",
                "-webkit-filter": "blur(3px)",
                "-ms-filter": "blur(3px)"
            };
            var forgot = ngDialog.open({
                template: 'forgotTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'forgotCtrl'
            });
            forgot.closePromise.then(function (data) {
                $scope.blurBox = {};
            });
        }
    });
