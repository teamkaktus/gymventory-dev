controllers
    .controller('paymentJoinCtrl', function ($scope, GamesFactory, $timeout, CreditsFactory, $state, $stateParams, ngDialog) {

        $scope.loading_spiner = false;
        $scope.success_text = false;
        var timer = null;

        $scope.paymentGame = function () {
            var data = {};

            data.invites = GamesFactory.getGameInvites();
            data.members_price = GamesFactory.getGameMemberPrice();
            data.game_id = $stateParams.id_game;
            data.club_user = $scope.game.club_user;
            data.gym_id = $scope.game.gym_id;

            $timeout.cancel(timer);
            $scope.success_text = false;
            $scope.loading_spiner = true;

            GamesFactory.joinGame(data).then(
                function (response) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;

                    if(response.data.status) {
                        $scope.inform_text = 'Game successfully created!';
                        CreditsFactory.getBalance().then(
                            function (response) {
                                var balance = response.data.balance,
                                    payment = GamesFactory.getGameMemberPrice().price;
                                //$("#balance_user").html(balance-payment);
                            }
                        )
                    }else{
                        $scope.inform_text = 'When creating a game fails, try again!';
                    }
                    timer = $timeout(function () {
                        $scope.success_text = false;
                        ngDialog.close();
                        GamesFactory.removeGameInvites();
                        GamesFactory.removeGameMemberPrice();
                        GamesFactory.removeGameParams();
                        $state.go('app.home');
                    }, 5000);
                }
            )
        }
        $scope.closePayment = function () {
            ngDialog.closeAll();
        }
    });
