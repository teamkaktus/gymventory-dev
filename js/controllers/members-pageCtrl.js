controllers
    .controller('membersCtrl', function ($scope, $rootScope, $timeout, $state, FiltersFactory, myGeocode, TeammatesFactory, MembersFactory) {
        $scope.tab_team = 'all';
        $scope.filter = {
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            name: ''
        };



        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }


        /*FiltersFactory.getProfileAddress().then(
            function (obj) {
                myGeocode.getAddressCoordinate(obj.data.address).then(
                    function (response) {
                        if (response.data.status != "ZERO_RESULTS") {
                            var coords = response.data.results[0].geometry.location;

                            $scope.filter.address = obj.data.address;

                            $scope.filter.latitude = coords['lat'];
                            $scope.filter.longitude = coords['lng'];

                            $scope.distanceDisabled = false;
                            $scope.distanceClass = '';
                            $scope.filter.distance = 5;

                            $scope.findMembers();
                        } else {
                            $scope.distanceDisabled = true;
                            $scope.distanceClass = 'disabled';
                            $scope.filter.distance = '';

                            $scope.filter.latitude = '';
                            $scope.filter.longitude = '';

                            $scope.findMembers();
                        }
                    },
                    function (error) {
                    }
                );
            },
            function (error) {
                console.log(error);
            }
        );

        $scope.setAddress = function () {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latitude = position.coords.latitude,
                    longitude = position.coords.longitude;

                $scope.filter.latitude = latitude;
                $scope.filter.longitude = longitude;

                myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(
                    function (response) {
                        $scope.filter.address = response.data.results[0].formatted_address;
                        $scope.distanceDisabled = false;
                        $scope.distanceClass = '';
                        $scope.filter.distance = 5;

                        $scope.findMembers();
                    }
                );
            });
        };
        var timer = {};

        $scope.changeAddress = function ($event) {
            $timeout.cancel(timer);

            timer = $timeout(function () {
                if ($scope.filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.filter.distance = 5;
                    if ($scope.filter.address != null) {
                        if ($scope.filter.address.formatted_address) {
                            $scope.filter.address = $scope.filter.address.formatted_address;
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.filter.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.filter.latitude = coords['lat'];
                                $scope.filter.longitude = coords['lng'];

                                $scope.findMembers();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.filter.distance = '';

                                $scope.filter.latitude = '';
                                $scope.filter.longitude = '';

                                $scope.findMembers();
                            }
                        },
                        function (error) {
                        }
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.filter.distance = '';

                    $scope.filter.latitude = '';
                    $scope.filter.longitude = '';
                }
            }, 15);
        };*/

        $scope.tab_teamChange = function () {
            MembersFactory.getMembers({filter: $scope.filter, tab: $scope.tab_team}).then(
                function (response) {
                    $scope.teammates_arr = [];
                    if($scope.tab_team == 'online') {
                        response.data.rows.forEach(function(e, i, arr){
                            console.log('e.user_id ' + e.user_id);
                            console.log('$scope.users.online', $scope.users.online);
                            console.log($scope.users.online.indexOf(e.user_id));
                            if($scope.users.online.indexOf(e.user_id) != -1){
                                console.log('tab online', e);
                                $scope.teammates_arr.push(e);
                            }
                        })
                    }else{
                        $scope.teammates_arr = response.data.rows;
                    }
                    //console.log($scope.teammates_arr);
                }
            )
        };

        $rootScope.$on('online',function (event, data) {
            $scope.users.online = data;
            $scope.$apply();
        });

        $scope.findMembers = function () {
            MembersFactory.getMembers({filter: $scope.filter, tab: $scope.tab_team}).then(
                function (response) {
                    $scope.teammates_arr = response.data.rows;
                    console.log($scope.teammates_arr);
                }
            )
        }
        $scope.findMembers();

        $scope.show_user = function (id) {
            $state.go('app.teammate-page', {t_id: id});
        }

    });