controllers
    .controller('createWithdrawalCtrl', function ($scope, $state, CreditsFactory, configFactory, ngDialog, $timeout) {
        $scope.withdrawal = {
            all_credits: 0,
            percent: 0,
            minimum: 0,
            avaible: 0,
            paypal_email: ''
        }

        $scope.credits = 0;

        configFactory.get({code: 'config', key: 'payout_limit'}).then(
            function (response) {
                $scope.withdrawal.minimum = response.data.row.s_value;
            },
            function (error) {
                console.log(error);
            }
        );

        CreditsFactory.getBalance().then(
            function (response) {
                $scope.withdrawal.all_credits = response.data.balance;
                $scope.withdrawal.percent = response.data.percent;
                $scope.withdrawal.avaible = ($scope.withdrawal.all_credits).toFixed(2);
                $scope.credits = $scope.withdrawal.avaible;
            },
            function (error) {
                console.log(error);
            }
        );

        $scope.loading_spiner = false;
        $scope.success_text = false;

        $scope.create_withdraw = function () {
            if($scope.withdrawal.minimum <= $scope.withdrawal.avaible && $scope.withdrawal.avaible <= $scope.credits) {
                $scope.success_text = false;
                $scope.loading_spiner = true;
                CreditsFactory.create_withdraw({amount: $scope.withdrawal.avaible, email: $scope.withdrawal.paypal_email, percent: ($scope.withdrawal.all_credits * ($scope.withdrawal.percent / 100))}).then(
                    function (response) {
                        if (response.data.status) {
                            $scope.success_text = true;
                            $scope.loading_spiner = false;
                            $scope.inform_text = 'Withdrawal successful!';
                        } else {
                            $scope.success_text = true;
                            $scope.loading_spiner = false;
                            $scope.inform_text = 'Withdrawal declined, try again later!';
                        }
                        timer = $timeout(function () {
                            $scope.success_text = false;
                            ngDialog.close();
                            $state.reload();
                        }, 2000);
                    },
                    function (error) {
                        $scope.success_text = true;
                        $scope.inform_text = 'There was an error when withdrawal!';
                        $scope.loading_spiner = false;
                        timer = $timeout(function () {
                            $scope.success_text = false;
                            ngDialog.close();
                            $state.reload();
                        }, 2000);
                    }
                )
            }
        }
    });