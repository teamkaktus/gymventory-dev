controllers.controller('club-courts_itemCtrl', function ($scope, $rootScope, ngDialog, $stateParams, GamesFactory, clubFactory, gymFactory, $timeout, $state) {
    var court_id = $stateParams.id;
    console.log(court_id);
    
    $scope.check_club = false;
    $scope.inform_text = false;

    $rootScope.club = {
        create: false
    };

    $scope.gameList = {};

    clubFactory.countClub().then(
        function (response) {
            if (parseInt(response.data.count) >= 1) {
                $rootScope.club.create = false;
                console.log("count club", $rootScope.club.create)
            } else {
                $rootScope.club.create = true;
                if($rootScope.club.create){
                    $state.go('app.createMyClub');
                }
            }
        },
        function (error) {
            console.log(error);
        }
    );

    gymFactory.getGym(court_id).then(
        function (response) {
            console.log('getGym ', response)
            $scope.gymsObj = response.data;
        },
        function (error) {
            console.log(error);
        }
    )

    $scope.update_gym = function (data, id) {
        data.id = id;

        var loading_spiner = true;
        success_text = false;

        $scope.loading_spiner = loading_spiner;
        $scope.success_text = success_text;

        gymFactory.updateGym(data).then(
            function (response) {
                $scope.success_text = true;
                $scope.loading_spiner = false;
                $scope.inform_text = 'This gym updated!';
                timer = $timeout(function() {
                    $scope.success_text = false;
                    delete(timer);
                }, 5000);
                console.log(response);
            },
            function (error){
                $scope.success_text[id] = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner = false;
                timer = $timeout(function() {
                    $scope.success_text = false;
                    delete(timer);
                }, 5000);
                console.log(error);
            }
        )
    };

    GamesFactory.getManagerGames(court_id).then(
        function(response){
            $scope.gameList = response.data.items;
        }
    )

    $rootScope.$on('game_added', function(event, obj){
        $state.reload();
    });

    $scope.createGame = function(){
        ngDialog.open({
            width: 900,
            templateUrl: './templates/game/newGameManager.html',
            className: 'ngdialog-theme-default',
            controller: 'newGameManagerCtrl'
        });
    };
    $scope.deleteGame = function (id) {
        GamesFactory.ManagerDeleteGame(id).then(
            function(response){
                console.log('deleteGame', response.data);
                $state.reload();
            },
            function(error){
                console.log('deleteGame', error);
            }
        )
    }
});
