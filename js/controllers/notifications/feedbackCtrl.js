controllers.controller('feedbackCtrl', function($scope, $rootScope, GamesFactory, ngDialog, $timeout) {
    GamesFactory.getGame($scope.game_id).then(
        function(response){
            console.log(response);
            $scope.game = response.data.row;
        }
    );

    $scope.bad = function(){
        $scope.closeThisDialog();
        $rootScope.$emit('openBad', {game_id: $scope.game_id});
    }

    $scope.sweet = function(){
        $scope.closeThisDialog();
        $rootScope.$emit('openSweet', {game_id: $scope.game_id});
    }
});
