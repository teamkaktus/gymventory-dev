controllers.controller('b_notificationsCtrl', function($scope, $rootScope, notificationFactory, $sce) {
    $scope.show_add = false;

    $scope.notification = {
        title: '',
        description: ''
    };

    $scope.yourModel = {
        customMenu: [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['format-block'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['link']
        ]
    };
    function history(){
        $scope.history = [];
        notificationFactory.getHistory().then(
            function (response) {
                console.log(response);
                response.data.forEach(function (e, i, arr) {
                    $scope.history.push({
                      'title': $sce.trustAsHtml(e.title),
                      'text':$sce.trustAsHtml(e.text),
                      'time':e.created_at
                    });
                })
            }
        )
    }
    history();

    $scope.sendNotifications = function(){
        notificationFactory.sendAllMembers($scope.notification).then(
            function (response) {
                $scope.notification = {
                    title: '',
                    description: ''
                };
                $rootScope.$emit('not-update', 1);
                $scope.show_add = false;
                history();
            }
        );

    }
});
