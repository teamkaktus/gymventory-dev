controllers.controller('badCtrl', function($scope, $rootScope, notificationFactory) {
    $scope.badArray = [];
    $scope.badQuest = {
        value: ''
    }

    console.log('$scope.game_id ', $scope.game_id);

    notificationFactory.getQuetions().then(
        function (response) {
            $scope.badArray = response.data;
            $scope.badQuest.value = response.data[0].id;
        }
    );

    $scope.badSubmit = function (obj){
        notificationFactory.setBad({quest: obj.value, game_id: $scope.game_id}).then(
            function (response){
                $rootScope.$emit('addFeedback', '');
            }
        )
    };
});
