controllers.controller('show_textCtrl', function($scope, notificationFactory, $sce, $parse) {
    $scope.text = {
        text: '',
        description: ''
    };

    var id = $scope.notification_id;

    notificationFactory.getDescription(id).then(
        function(response){
            $scope.text.text = response.data.text;
            $scope.text.description = $sce.trustAsHtml(response.data.description);
            console.log($scope.text.description);
        }
    );
});
