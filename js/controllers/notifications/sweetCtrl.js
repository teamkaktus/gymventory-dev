controllers.controller('sweetCtrl', function($scope, $rootScope, notificationFactory, ngDialog) {
    $scope.rate = 1;
    $scope.max = 5;
    $scope.isReadonly = false;

    $scope.rate = {
        stars: {}
    }

    $scope.hoveringOver = function(value) {
        $scope.overStar = value;
        $scope.percent = 100 * (value / $scope.max);
    };

    $scope.sweetArray = [];

    notificationFactory.getMembersGame($scope.game_id).then(
        function (response) {
            $scope.sweetArray = response.data;
        },
        function(error){
            console.log(error);
        }
    )

    $scope.sweetSubmit = function(obj){
        obj.game_id = $scope.game_id;
        notificationFactory.setRating(obj).then(
            function(response){
                $scope.closeThisDialog();
                $rootScope.$emit('addFeedback', '');
            }
        )
    }
});
