controllers
    .controller('show_gameCtrl', function ($scope, ConfigApi, userProfile, AuthFactory, $stateParams, GamesFactory, $timeout, TeammatesFactory, Socket, ngDialog, CreditsFactory) {
        var id_game = $scope.game_id;

        GamesFactory.getGame(id_game).then(
            function(response){
                $scope.game = response.data.row;
            }
        );
    });

