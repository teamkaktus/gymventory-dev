controllers

    .controller('lobbyCtrl', function ($scope, $rootScope, $location, $cookies, $state, $filter,$timeout, GamesFactory, FiltersFactory, myGeocode, ngDialog) {
        $timeout.cancel();


        $scope.games_arr = [];
        $scope.tab_lobby = 'all';
        $scope.configSBar = {
            autoHideScrollbar: false,
            theme: 'light',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 200,
            scrollInertia: 0
        };


        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };

        $scope.marker = {
            id: 0,
            coords: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            options: {
                icon: marker_url
            }
        };
        $scope.game_filter = {
          latitude:null,
          longitude:null,
          address:''
        }
        $scope.game = [];

        $scope.set_timers = function (data) {
            if(data) {
                data.forEach(function (el, i, arr) {
                    var startDate = new Date(el.date);
                    var dd = startDate.getDate();
                    var mm = startDate.getMonth() + 1; //January is 0!
                    var yyyy = startDate.getFullYear();
                    var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                    function getTimeRemaining(endtime) {

                        var t = Date.parse(endtime) - Date.parse(new Date());
                        var seconds = Math.floor((t / 1000) % 60);
                        var minutes = Math.floor((t / 1000 / 60) % 60);
                        var hours = Math.floor(t / (1000 * 60 * 60));


                        return {
                            'total': t,
                            'hours': hours,
                            'minutes': minutes,
                            'seconds': seconds
                        };
                    }

                    $scope.clock = [];
                    $scope.clock[el.id] = "00:00:00";
                    var tick = function () {
                        var t = getTimeRemaining(endtime);
                        $scope.clock[el.id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                        $timeout(tick, 1000); // reset the timer
                    }
                    $timeout(tick, 1000);
                })
            }
        }

        // GamesFactory.getGames().then(
        //     function (response) {
        //         $timeout.cancel();
        //         $scope.games_arr = $scope.games_arr.concat(response.data.rows);
        //         $scope.set_timers(response.data.rows);
        //     }
        // )
        //
        // GamesFactory.getGamesReserved().then(
        //     function (response) {
        //         $timeout.cancel();
        //         $scope.games_arr = $scope.games_arr.concat(response.data.rows);
        //         $scope.set_timers(response.data.rows);
        //     }
        // )
        //
        // GamesFactory.getGamesWatch().then(
        //     function (response) {
        //         $timeout.cancel();
        //         $scope.games_arr = $scope.games_arr.concat(response.data.rows);
        //         $scope.set_timers(response.data.rows);
        //     }
        // )
        // GamesFactory.getGamesInvites().then(
        //     function (response) {
        //         $timeout.cancel();
        //         $scope.games_arr = $scope.games_arr.concat(response.data.rows);
        //         $scope.set_timers(response.data.rows);
        //     }
        // )
        var timer = {};

        $scope.changeAddress = function ($event) {
          console.log('here2');
            $timeout.cancel(timer);
            timer = $timeout(function () {
                if ($scope.game_filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.game_filter.distance = 1000000;
                    if ($scope.game_filter.address != null) {
                        if ($scope.game_filter.address.formatted_address) {
                            $scope.game_filter.address = $scope.game_filter.address.formatted_address;
                            $cookies.put('address', $scope.game_filter.address)
                            console.log($scope.game_filter.address);
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.game_filter.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.game_filter.latitude = coords['lat'];
                                $scope.game_filter.longitude = coords['lng'];

                                $scope.tab_lobbyChange();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.game_filter.distance = '';

                                $scope.game_filter.latitude = '';
                                $scope.game_filter.longitude = '';

                                $scope.tab_lobbyChange();
                            }
                        },
                        function (error) {
                        }
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.game_filter.distance = '';

                    $scope.game_filter.latitude = '';
                    $scope.game_filter.longitude = '';

                    $scope.tab_lobbyChange();
                }
            }, 15);
        };
        $scope.setAddress = function (myloc) {
          if (!myloc) {myloc=false}
          if ((typeof $cookies.get('address') != 'undefined') && (!myloc)) {
              console.log('here1');
              $scope.game_filter.address = $cookies.get('address')
              $scope.changeAddress()
          } else if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var latitude = position.coords.latitude,
                            longitude = position.coords.longitude;

                        $scope.game_filter.latitude = latitude;
                        $scope.game_filter.longitude = longitude;

                        myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(
                            function (response) {
                                $scope.game_filter.address = response.data.results[0].formatted_address;
                                $cookies.put('address', $scope.game_filter.address)
                                $scope.distanceDisabled = false;
                                $scope.distanceClass = '';
                                $scope.game_filter.distance = 1000000;

                                $scope.tab_lobbyChange();
                            },
                            function (error) {
                              $scope.tab_lobbyChange();
                                // getProfleAddress();
                            }
                        );
                    }, function (geo) {
                      $scope.tab_lobbyChange();
                        console.log(geo.message);
                        // getProfleAddress();
                    });
                }else {
                  $scope.tab_lobbyChange();
                }
        };
        $scope.setAddress()
        $scope.tab_lobbyChange = function () {
            switch ($scope.tab_lobby) {
                case 'all':
                {
                    $scope.games_arr = [];
                    GamesFactory.getGames($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                            $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                        }
                    )
                    GamesFactory.getGamesReserved($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                            $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                        }
                    )
                    GamesFactory.getGamesWatch($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                            console.log(response.data.rows);
                            $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                        }
                    )
                    GamesFactory.getGamesInvites($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                            $scope.games_arr = $filter('orderBy')($scope.games_arr, 'date')
                        }
                    )
                    break;
                }
                case 'reserved':
                {
                    $scope.games_arr = [];
                    GamesFactory.getGamesReserved($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = response.data.rows;
                        }
                    )
                    break;
                }
                case 'watch':
                {
                    $scope.games_arr = [];
                    GamesFactory.getGamesWatch($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = response.data.rows;
                        }
                    )
                    break;
                }
                case 'joined':
                {
                    $scope.games_arr = [];
                    GamesFactory.getGames($scope.game_filter).then(
                        function (response) {
                            console.log(response.data.rows);
                            $scope.games_arr = response.data.rows;
                        }
                    )
                    break;
                }
                case 'invites':
                {
                    $scope.games_arr = [];
                    GamesFactory.getGamesInvites($scope.game_filter).then(
                        function (response) {
                            $scope.games_arr = response.data.rows;
                        }
                    )
                    break;
                }
            }
        }

        $scope.payReserve = function (card) {
            $scope.game_id = card.id;
            $scope.club_user = card.club_user;
            $scope.game_price = (card.global_members*card.price);

            $scope.dialog = ngDialog.open({
                templateUrl: './templates/credits-paymentTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'paymentReserveCtrl',
                scope: $scope
            });

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $("#game-cost").html(card.global_members*card.price);
            });

        }

        $scope.joinGame = function (id) {
            console.log(id);
            $state.go('app.join-game', {id_game: id});
        }
    });
