controllers
    .controller('create-gameCtrl', function ($scope, ConfigApi, userProfile, AuthFactory, $stateParams, GamesFactory, $timeout, TeammatesFactory, Socket, ngDialog, CreditsFactory) {
        var id_gym = $stateParams.id_gym;
        var gameParams = GamesFactory.getGameParams();

        GamesFactory.removeGameInvites();

        console.log(gameParams);
        $scope.added_members = {
            0: true
        };
        $scope.createGame = true;

        $scope.adding_invaite = {};

        $scope.exist_credits = true;

        $scope.game = {
            global_members: 1,
            date: gameParams.dt,
            watching: 0,
            game_invites: []
        };
        
        $scope.clock = "00:00:00";
       // }; // initialise the time variable
        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };

        $scope.marker = {
            id: 0,
            coords: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            options: {
                icon: marker_url
            }
        };


        $scope.urlBase = GamesFactory.urlBase;

        GamesFactory.getGym(id_gym).then(
            function(response){
                console.log(response);
                $scope.gym = response.data;
                $scope.gym.user_avatar = $scope.gym.user_avatar.replace('\\', '/');

                $scope.marker.coords.latitude = $scope.map.center.latitude = $scope.gym.latitude;
                $scope.marker.coords.longitude = $scope.map.center.longitude = $scope.gym.longitude;
                $scope.cost_game = response.data.price;

                var startDate = new Date(gameParams.dt);
                var dd = startDate.getDate();
                var mm = startDate.getMonth()+1; //January is 0!
                var yyyy = startDate.getFullYear();
                
                var endtime = yyyy+'-'+mm+'-'+dd+' '+gameParams.start_time.toString().replace('.', ':')+':00';
                console.log(endtime);

                function getTimeRemaining(endtime){

                    var t = Date.parse(endtime) - Date.parse(new Date());
                    var seconds = Math.floor( (t/1000) % 60 );
                    var minutes = Math.floor( (t/1000/60) % 60 );
                    var hours = Math.floor( (t/(1000*60*60)) );

                    return {
                        'total': t,
                        'hours': hours,
                        'minutes': minutes,
                        'seconds': seconds
                    };
                }

                $scope.clock = "00:00:00";
                var tick = function() {
                    var t = getTimeRemaining(endtime);
                    $scope.clock = t.hours+':'+t.minutes+':'+t.seconds;
                    $timeout(tick, 1000); // reset the timer
                }
                $timeout(tick, 1000);

                CreditsFactory.getBalance().then(
                    function (response) {
                        if(response.data.balance < $scope.cost_game){
                            $scope.exist_credits = false;
                        }
                    }
                )
                GamesFactory.addGameMemberPrice(JSON.stringify({member: 1, price: $scope.gym.price}));
            }
        );

        TeammatesFactory.getTeammates('').then(
            function(response){
                $scope.teammates_arr = response.data;
                $scope.$broadcast('rebuild:scroll');
            }

        );

        $scope.searchTeammate = function (search_name) {
            TeammatesFactory.getTeammates(search_name).then(
                function(response){
                    $scope.teammates_arr = response.data;
                    $scope.$broadcast('rebuild:scroll');
                }
            )
        };

        $scope.add_member = function($event, index){
            $scope.game.global_members += 1;
            $scope.added_members[index] = true;
            $scope.cost_game = $scope.gym.price * $scope.game.global_members;
            GamesFactory.addGameMemberPrice(JSON.stringify({member: $scope.game.global_members, price: $scope.cost_game}));
            CreditsFactory.getBalance().then(
                function (response) {
                    if(response.data.balance < $scope.cost_game){
                        $scope.exist_credits = false;
                    }
                }
            )
        };

        $scope.set_invite = function(id, img){
            if(img == ''){
                img = '/img/no_avatar_vopros.jpg';
            }
            $scope.adding_invaite[id] = true;
            $scope.game.game_invites.push({id: id, img: img});
            GamesFactory.addGameInvites($scope.game.game_invites);
        };

        $scope.remove_invite = function(id){
            $scope.game.game_invites.forEach(function (el, i, arr) {
                if(el.id == id){
                   // $scope.adding_invaite[id] = false;
                    delete($scope.adding_invaite[id]);
                    $scope.game.game_invites.splice(i, 1);
                    GamesFactory.addGameInvites($scope.game.game_invites);
                }
            })
        };

        $scope.buyCredits = function(){
            var dialog = ngDialog.open({
                templateUrl: './templates/pp-cc-paymentTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'pp_paymentCtrl',
                scope: $scope,
                preCloseCallback: function(value) {
                    CreditsFactory.getBalance().then(
                        function (response) {
                            if(response.data.balance < $scope.cost_game){
                                $scope.exist_credits = false;
                            }else{
                                $scope.exist_credits = true;
                                $scope.createMatch();
                            }
                        }
                    )
                }
            });
        };

        $scope.reserve = function(){
            $scope.dialog = ngDialog.open({
                templateUrl: './templates/dialog-reservationsTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'dialog-reservationsCtrl',
                scope: $scope
            });

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $("#game-cost").html($scope.game.global_members);
            });
        };

        $scope.createMatch = function(fees){
            $scope.fees = fees;
            $scope.dialog = ngDialog.open({
                templateUrl: './templates/credits-paymentTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'paymentGameCtrl',
                scope: $scope
            });

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $("#game-cost").html(GamesFactory.getGameMemberPrice().price);
            });
        }
    });

