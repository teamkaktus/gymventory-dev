controllers
    .controller('membersCtrl', function ($scope, $rootScope, $timeout, $state, FiltersFactory, myGeocode, TeammatesFactory, MembersFactory) {
        $scope.tab_team = 'all';
        $scope.filter = {
            sport: 'Basketball',
            name: '',
            sort: 'Soonest',
            address: '',
            distance: '',
            latitude: '',
            longitude: ''
        };



        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        $scope.startStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 am', '12:15 pm', '12:30 pm', '12:45 pm', '13:00 pm', '13:15 pm', '13:30 pm', '13:45 pm', '14:00 pm', '14:15 pm', '14:30 pm', '14:45 pm', '15:00 pm', '15:15 pm', '15:30 pm', '15:45 pm', '16:00 pm', '16:15 pm', '16:30 pm', '16:45 pm', '17:00 pm', '17:15 pm', '17:30 pm', '17:45 pm', '18:00 pm', '18:15 pm', '18:30 pm', '18:45 pm', '19:00 pm', '19:15 pm', '19:30 pm', '19:45 pm', '20:00 pm', '20:15 pm', '20:30 pm', '20:45 pm', '21:00 pm', '21:15 pm', '21:30 pm', '21:45 pm', '22:00 pm', '22:15 pm', '22:30 pm', '22:45 pm', '23:00 pm'];
        $scope.startTime = {
            id: 's_time',
            value: '08:00 am',
            options: {
                stepsArray: $scope.startStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var tmp_val = modelValue.split(' ');
                    var time = parseFloat(tmp_val[0].replace(':', '.'));
                    console.log(typeof  time, time);
                    $scope.filter.start_time = time;
                    $scope.findMembers();
                    var i = $scope.startStepArray.indexOf(modelValue);
                    $scope.endTime.value = $scope.endStepArray[i];
                    console.log($scope.startStepArray.indexOf(modelValue));
                }
            }
        };
        $scope.endStepArray = ['02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 am', '12:15 pm', '12:30 pm', '12:45 pm', '13:00 pm', '13:15 pm', '13:30 pm', '13:45 pm', '14:00 pm', '14:15 pm', '14:30 pm', '14:45 pm', '15:00 pm', '15:15 pm', '15:30 pm', '15:45 pm', '16:00 pm', '16:15 pm', '16:30 pm', '16:45 pm', '17:00 pm', '17:15 pm', '17:30 pm', '17:45 pm', '18:00 pm', '18:15 pm', '18:30 pm', '18:45 pm', '19:00 pm', '19:15 pm', '19:30 pm', '19:45 pm', '20:00 pm', '20:15 pm', '20:30 pm', '20:45 pm', '21:00 pm', '21:15 pm', '21:30 pm', '21:45 pm', '22:00 pm', '22:15 pm', '22:30 pm', '22:45 pm', '23:00 pm', '23:15 pm', '23:30 pm', '23:45 pm', '24:00 pm'];
        $scope.endTime = {
            id: 'e_time',
            value: '10:00 am',
            options: {
                stepsArray: $scope.endStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var tmp_val = modelValue.split(' ');
                    var time = parseFloat(tmp_val[0].replace(':', '.'));
                    $scope.filter.end_time = time;

                    var i = $scope.endStepArray.indexOf(modelValue);
                    $scope.startTime.value = $scope.startStepArray[i];
                    $scope.findMembers();
                }
            }
        };


        FiltersFactory.getProfileAddress().then(
            function (obj) {
                myGeocode.getAddressCoordinate(obj.data.address).then(
                    function (response) {
                        if (response.data.status != "ZERO_RESULTS") {
                            var coords = response.data.results[0].geometry.location;

                            $scope.filter.address = obj.data.address;

                            $scope.filter.latitude = coords['lat'];
                            $scope.filter.longitude = coords['lng'];

                            $scope.distanceDisabled = false;
                            $scope.distanceClass = '';
                            $scope.filter.distance = 5;

                            $scope.findMembers();
                        } else {
                            $scope.distanceDisabled = true;
                            $scope.distanceClass = 'disabled';
                            $scope.filter.distance = '';

                            $scope.filter.latitude = '';
                            $scope.filter.longitude = '';

                            $scope.findMembers();
                        }
                    },
                    function (error) {
                    }
                );
            },
            function (error) {
                console.log(error);
            }
        );

        $scope.setAddress = function () {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latitude = position.coords.latitude,
                    longitude = position.coords.longitude;

                $scope.filter.latitude = latitude;
                $scope.filter.longitude = longitude;

                myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(
                    function (response) {
                        $scope.filter.address = response.data.results[0].formatted_address;
                        $scope.distanceDisabled = false;
                        $scope.distanceClass = '';
                        $scope.filter.distance = 5;

                        $scope.findMembers();
                    }
                );
            });
        };
        var timer = {};

        $scope.changeAddress = function ($event) {
            $timeout.cancel(timer);

            timer = $timeout(function () {
                if ($scope.filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.filter.distance = 5;
                    if ($scope.filter.address != null) {
                        if ($scope.filter.address.formatted_address) {
                            $scope.filter.address = $scope.filter.address.formatted_address;
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.filter.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.filter.latitude = coords['lat'];
                                $scope.filter.longitude = coords['lng'];

                                $scope.findMembers();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.filter.distance = '';

                                $scope.filter.latitude = '';
                                $scope.filter.longitude = '';

                                $scope.findMembers();
                            }
                        },
                        function (error) {
                        }
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.filter.distance = '';

                    $scope.filter.latitude = '';
                    $scope.filter.longitude = '';
                }
            }, 15);
        };

        $scope.tab_teamChange = function () {
            MembersFactory.getMembers({filter: $scope.filter, tab: $scope.tab_team}).then(
                function (response) {
                    $scope.teammates_arr = [];
                    if($scope.tab_team == 'online') {
                        response.data.rows.forEach(function(e, i, arr){
                            console.log('e.user_id ' + e.user_id);
                            console.log('$scope.users.online', $scope.users.online);
                            console.log($scope.users.online.indexOf(e.user_id));
                            if($scope.users.online.indexOf(e.user_id) != -1){
                                console.log('tab online', e);
                                $scope.teammates_arr.push(e);
                            }
                        })
                    }else{
                        $scope.teammates_arr = response.data.rows;
                    }
                    //console.log($scope.teammates_arr);
                }
            )
        };

        $rootScope.$on('online',function (event, data) {
            $scope.users.online = data;
            $scope.$apply();
        });

        $scope.findMembers = function () {
            MembersFactory.getMembers({filter: $scope.filter, tab: $scope.tab_team}).then(
                function (response) {
                    $scope.teammates_arr = response.data.rows;
                    console.log($scope.teammates_arr);
                }
            )
        }

        $scope.show_user = function (id) {
            $state.go('app.teammate-page', {t_id: id});
        }

    });