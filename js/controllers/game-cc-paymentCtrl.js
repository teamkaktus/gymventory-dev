controllers.controller('game_paymentCtrl', ['$scope', '$timeout','CreditsFactory', 'ngDialog', 'paypalFactory', '$cookies', '$state', function($scope, $timeout, CreditsFactory, ngDialog, paypalFactory, $cookies, $state) {
    $scope.cc = {
        type : 'visa',
        payment_type: 'credit_card',
        number: '4032035691990857',
        expire_month_year: '09/2021',
        cvv2: '123',
        first_name: 'Joe',
        last_name: 'Shopper',
        credits: 1,
        balance: '',
        game_cost: '',
        game_id: '',
        gym_id: '',
        member: 1,
        start_time: 0,
        end_time: 11,
        date: '',
        join: 1,
        reserve: 0,
        fees: 0,
        gym_id: 0,
        club_user: 0
    };

    paypalFactory.removeCredits();

    $scope.loading_spiner = false;
    $scope.success_text = false;

    var timer = null;
    $scope.make_payment = function() {
        $timeout.cancel(timer);
        $scope.success_text = false;
        $scope.loading_spiner = true;

        CreditsFactory.paymentGame($scope.cc).then(
            function (response) {
                console.log(response);
                if(response.data.payment.state == 'approved') {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment successful!';
                    $scope.$emit('resetBalance', response.data.credits);
                    $timeout(function(){
                        $state.go('app.home');
                    }, 1000);
                }else if(response.data.payment.state == 'created'){
                    response.data.payment.links.forEach(function(e){
                        $cookies.remove('pp_id');
                        $cookies.put('pp_id', response.data.payment.id);
                        if(e.rel == 'approval_url' && e.method == 'REDIRECT'){
                            window.location.replace(e.href);
                        }
                    })
                }else{
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'Payment declined, try again later!';
                }
                timer = $timeout(function () {
                    $scope.success_text = false;
                    ngDialog.close();
                }, 5000);
            },
            function (error) {
                $scope.success_text = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner = false;
                timer = $timeout(function () {
                    $scope.success_text = false;
                }, 5000);
            }
        )
    }

    $scope.$on('myCustomEvent', function (event, data) {
        $scope.cc.credits = data.game_cost;
        $scope.cc.game_cost = data.game_cost;
        $scope.cc.balance = data.balance;
        $scope.cc.game_id = data.game_id;
        $scope.cc.gym_id = data.gym_id;
        $scope.cc.member = data.member;
        $scope.cc.start_game = data.start_time;
        $scope.cc.end_game = data.end_time;
        $scope.cc.date = data.date;
        $scope.cc.fees = data.fees;
        $scope.cc.club_user = data.club_user;
        $scope.cc.gym_id = data.gym_id;
        $scope.cc.join = data.join;
        $scope.cc.reserve = data.reserve;
    });

    console.log($scope.cc);
}]);