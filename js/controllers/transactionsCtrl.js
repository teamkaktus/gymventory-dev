controllers
    .controller('transactionsCtrl', function ($scope, $rootScope, userProfile, configFactory, ngDialog) {
        userProfile.getUserTransactions().then(
            function (response) {
                $scope.transactions = response.data;
            }
        )

    });