controllers
    .controller('join-gameCtrl', function ($scope, ConfigApi, ngDialog, userProfile, AuthFactory, $stateParams, GamesFactory, $timeout, TeammatesFactory, Socket, CreditsFactory) {
        var id_game = $stateParams.id_game;
        
        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };
        $scope.createGame = false;


        GamesFactory.removeGameInvites();

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };

        $scope.marker = {
            id: 0,
            coords: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            options: {
                icon: marker_url
            }
        };


        $scope.urlBase = GamesFactory.urlBase;

        GamesFactory.getGame(id_game).then(
            function(response){

                $scope.game = response.data.row;

                console.log('getGame - ', response.data.row);

                var startDate = new Date($scope.game.date);
                var dd = startDate.getDate();
                var mm = startDate.getMonth()+1; //January is 0!
                var yyyy = startDate.getFullYear();
                var endtime = yyyy+'-'+mm+'-'+dd+' '+$scope.game.start_time.toString().replace('.', ':')+':00';
                console.log(endtime);

                function getTimeRemaining(endtime){

                    var t = Date.parse(endtime) - Date.parse(new Date());
                    var day_hours = Math.floor( (t/(1000*60*60*24) % 24)*24 );
                    var seconds = Math.floor( (t/1000) % 60 );
                    var minutes = Math.floor( (t/1000/60) % 60 );
                    var hours = Math.floor( (t/(1000*60*60)) % 24 );

                    console.log(day_hours);

                    if(day_hours != 0){
                        hours += day_hours;
                    }

                    return {
                        'total': t,
                        'hours': hours,
                        'minutes': minutes,
                        'seconds': seconds
                    };
                }

                $scope.clock = "00:00:00";
                var tick = function() {
                    var t = getTimeRemaining(endtime);
                    $scope.clock = t.hours+':'+t.minutes+':'+t.seconds;
                    $timeout(tick, 1000); // reset the timer
                }
                $timeout(tick, 1000);

                $scope.marker.coords.latitude = $scope.map.center.latitude = $scope.game.latitude;
                $scope.marker.coords.longitude = $scope.map.center.longitude = $scope.game.longitude;

                $scope.join_members = 1;
                $scope.game_invites = [];

                $scope.game.global_members += 1;

                $scope.cost_game = parseFloat(($scope.game.c_member >= 1)?$scope.game.price_member:$scope.game.price_hours).toFixed(2);

                console.log('$scope.cost_game - ', $scope.cost_game);

                CreditsFactory.getBalance().then(
                    function (response) {
                        if(response.data.balance < $scope.cost_game){
                            $scope.exist_credits = false;
                        }
                        $scope.$balance = response.data.balance;
                    }
                )
                GamesFactory.addGameMemberPrice(JSON.stringify({member: 1, price: $scope.game.price}));
            }
        );

        TeammatesFactory.getTeammates('').then(
            function(response){
                $scope.teammates_arr = response.data;
                $scope.$broadcast('rebuild:scroll');
            }
        );

        $scope.searchTeammate = function (search_name) {
            TeammatesFactory.getTeammates(search_name).then(
                function(response){
                    $scope.teammates_arr = response.data;
                    $scope.$broadcast('rebuild:scroll');
                }
            )
        };



        $scope.added_members = {
            0: true
        };

        $scope.adding_invaite = {};

        $scope.exist_credits = true;

        /*$scope.add_member = function($event, index){
            //$event.css({background: 'red'});
            console.log($scope.join_members);
            $scope.game.global_members += 1;
            $scope.join_members = $scope.join_members + 1;
            $scope.added_members[index] = true;
            $scope.cost_game = $scope.game.price * ($scope.join_members);

            GamesFactory.addGameMemberPrice(JSON.stringify({member: $scope.join_members, price: $scope.game.price * $scope.join_members}));
            CreditsFactory.getBalance().then(
                function (response) {
                    if(response.data.balance < $scope.cost_game){
                        $scope.exist_credits = false;
                    }
                }
            )
        };*/

        $scope.set_invite = function($event, id, img){
            angular.element($event.currentTarget).attr("data-ng-click", "");
            if(img == ''){
                img = '/img/no_avatar_vopros.jpg';
            }
            $scope.adding_invaite[id] = true;
            $scope.game_invites.push({id: id, img: img});
            GamesFactory.addGameInvites($scope.game_invites);
        }

        $scope.remove_invite = function(id){
            $scope.game_invites.forEach(function (el, i, arr) {
                if(el.id == id){
                    // $scope.adding_invaite[id] = false;
                    delete($scope.adding_invaite[id]);
                    $scope.game_invites.splice(i, 1);
                    GamesFactory.addGameInvites($scope.game.game_invites);
                }
            })
        }

        $scope.buyCredits = function(){
            var dialog = ngDialog.open({
                templateUrl: './templates/game-cc-paymentTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'game_paymentCtrl',
                scope: $scope
            });

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $scope.$broadcast('myCustomEvent', {
                    game_cost: parseFloat(($scope.game.c_member >= 1)?$scope.game.price_member:$scope.game.price_hours).toFixed(2),
                    balance: $scope.$balance,
                    game_id: id_game,
                    gym_id: $scope.game.gym_id,
                    start_game: $scope.game.start_time,
                    end_game: $scope.game.end_time,
                    date: $scope.game.date,
                    member: 1,
                    fees: $scope.game.fees,
                    club_user: $scope.game.club_user,
                    gym_id: $scope.game.gym_id,
                    join: 1,
                    reserve: 0
                });

            });
        }

        /*$scope.reserve = function(){
            $scope.dialog = ngDialog.open({
                templateUrl: './templates/dialog-reservationsTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'dialog-reservationsCtrl',
                scope: $scope
            });

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $("#game-cost").html($scope.join_members);
            });
        };*/

        $scope.joinMatch = function () {
            $scope.dialog = ngDialog.open({
                templateUrl: './templates/credits-paymentTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'paymentJoinCtrl',
                scope: $scope
            });

            console.log(GamesFactory.getGameMemberPrice());

            $scope.$on('ngDialog.opened', function (e, $dialog) {
                $("#game-cost").html(parseFloat(($scope.game.c_member >= 1)?$scope.game.price_member:$scope.game.price_hours).toFixed(2));
            });
        }
    });

