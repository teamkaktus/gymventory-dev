controllers
    .controller('teammate-pageCtrl', function ($scope, $rootScope, $state, $timeout, $stateParams, FiltersFactory, myGeocode, TeammatesFactory, GamesFactory) {
        var id_t = $stateParams.t_id;
        $scope.filter = {
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            address: '',
            distance: '',
            start_time: 8,
            end_time: 10,
            latitude: '',
            longitude: '',
            team_tab : 'active'
        };




        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        $scope.startStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 am', '12:15 pm', '12:30 pm', '12:45 pm', '13:00 pm', '13:15 pm', '13:30 pm', '13:45 pm', '14:00 pm', '14:15 pm', '14:30 pm', '14:45 pm', '15:00 pm', '15:15 pm', '15:30 pm', '15:45 pm', '16:00 pm', '16:15 pm', '16:30 pm', '16:45 pm', '17:00 pm', '17:15 pm', '17:30 pm', '17:45 pm', '18:00 pm', '18:15 pm', '18:30 pm', '18:45 pm', '19:00 pm', '19:15 pm', '19:30 pm', '19:45 pm', '20:00 pm', '20:15 pm', '20:30 pm', '20:45 pm', '21:00 pm', '21:15 pm', '21:30 pm', '21:45 pm', '22:00 pm', '22:15 pm', '22:30 pm', '22:45 pm', '23:00 pm'];
        $scope.startTime = {
            id: 's_time',
            value: '08:00 am',
            options: {
                stepsArray: $scope.startStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var tmp_val = modelValue.split(' ');
                    var time = parseFloat(tmp_val[0].replace(':', '.'));
                    $scope.filter.start_time = time;
                    $scope.filterGames();
                    var i = $scope.startStepArray.indexOf(modelValue);
                    $scope.endTime.value = $scope.endStepArray[i];
                }
            }
        };
        $scope.endStepArray = ['02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 am', '12:15 pm', '12:30 pm', '12:45 pm', '13:00 pm', '13:15 pm', '13:30 pm', '13:45 pm', '14:00 pm', '14:15 pm', '14:30 pm', '14:45 pm', '15:00 pm', '15:15 pm', '15:30 pm', '15:45 pm', '16:00 pm', '16:15 pm', '16:30 pm', '16:45 pm', '17:00 pm', '17:15 pm', '17:30 pm', '17:45 pm', '18:00 pm', '18:15 pm', '18:30 pm', '18:45 pm', '19:00 pm', '19:15 pm', '19:30 pm', '19:45 pm', '20:00 pm', '20:15 pm', '20:30 pm', '20:45 pm', '21:00 pm', '21:15 pm', '21:30 pm', '21:45 pm', '22:00 pm', '22:15 pm', '22:30 pm', '22:45 pm', '23:00 pm', '23:15 pm', '23:30 pm', '23:45 pm', '24:00 pm'];
        $scope.endTime = {
            id: 'e_time',
            value: '10:00 am',
            options: {
                stepsArray: $scope.endStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var tmp_val = modelValue.split(' ');
                    var time = parseFloat(tmp_val[0].replace(':', '.'));
                    $scope.filter.end_time = time;

                    var i = $scope.endStepArray.indexOf(modelValue);
                    $scope.startTime.value = $scope.startStepArray[i];
                    $scope.filterGames();
                }
            }
        };


        FiltersFactory.getProfileAddress().then(
            function (obj) {
                myGeocode.getAddressCoordinate(obj.data.address).then(
                    function (response) {
                        if (response.data.status != "ZERO_RESULTS") {
                            var coords = response.data.results[0].geometry.location;

                            $scope.filter.address = obj.data.address;

                            $scope.filter.latitude = coords['lat'];
                            $scope.filter.longitude = coords['lng'];

                            $scope.distanceDisabled = false;
                            $scope.distanceClass = '';
                            $scope.filter.distance = 5;

                            $scope.filterGames();
                        } else {
                            $scope.distanceDisabled = true;
                            $scope.distanceClass = 'disabled';
                            $scope.filter.distance = '';

                            $scope.filter.latitude = '';
                            $scope.filter.longitude = '';

                            $scope.filterGames();
                        }
                    },
                    function (error) {
                    }
                );
            },
            function (error) {
                console.log(error);
            }
        );

        $scope.setAddress = function () {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latitude = position.coords.latitude,
                    longitude = position.coords.longitude;

                $scope.filter.latitude = latitude;
                $scope.filter.longitude = longitude;

                myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(
                    function (response) {
                        $scope.filter.address = response.data.results[0].formatted_address;
                        $scope.distanceDisabled = false;
                        $scope.distanceClass = '';
                        $scope.filter.distance = 5;

                        $scope.filterGames();
                    }
                );
            });
        };
        var timer = {};

        $scope.changeAddress = function ($event) {
            $timeout.cancel(timer);

            timer = $timeout(function () {
                if ($scope.filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.filter.distance = 5;
                    if ($scope.filter.address != null) {
                        if ($scope.filter.address.formatted_address) {
                            $scope.filter.address = $scope.filter.address.formatted_address;
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.filter.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.filter.latitude = coords['lat'];
                                $scope.filter.longitude = coords['lng'];

                                $scope.filterGames();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.filter.distance = '';

                                $scope.filter.latitude = '';
                                $scope.filter.longitude = '';

                                $scope.filterGames();
                            }
                        },
                        function (error) {
                        }
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.filter.distance = '';

                    $scope.filter.latitude = '';
                    $scope.filter.longitude = '';
                }
            }, 15);
        };

        $scope.today = function() {
            $scope.filter.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.filter.dt = null;
        };

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        $scope.dateOptions = {
            /*dateDisabled: disabled,*/
            formatYear: 'yyyy',
            maxDate: new Date(2030, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            showWeeks: false
        };

        $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.filter.dt = new Date(year, month, day);
            $scope.filterGames();
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $rootScope.$on('online',function (event, data) {
            $scope.users.online = data;
            $scope.$apply();
        });

        var data = {
            id: id_t,
            latitude: $scope.filter.latitude,
            longitude: $scope.filter.longitude
        };

        TeammatesFactory.getTeammate(data).then(
            function (response) {
                $scope.teammates_arr = response.data.rows;
                console.log('teammate', response.data.rows);
                $scope.$broadcast('rebuild:scroll');
            }
        )

        $scope.team_games = {};

        $scope.filterGames = function () {
            var data = $scope.filter;
            data.t_id = id_t;
            data.tab = $scope.filter.team_tab;
            console.log('filterGames', data);
            TeammatesFactory.getGames(data).then(
                function (response) {
                    console.log(response.data.rows);
                    $scope.team_games = response.data.rows;
                }
            )
        };

        $scope.change_tab = function () {
            $scope.filterGames();
        }

        $scope.rightSidebarInvite = '';

        $scope.gamesArray = [];
        $scope.loading_spiner = true;

        $scope.adding_invaite = {};
        GamesFactory.getBookGames(id_t).then(
            function (response) {
                $scope.gamesArray = response.data.result;
                console.log('Book games: ', $scope.gamesArray);
                $scope.loading_spiner = false;
                $scope.$broadcast('rebuild:scroll');
            },
            function (error) {
                console.log(error);
            }
        )

        $scope.toggleInviteP = function($event, className) {
            className = className || 'toggledM';
            if ($scope.rightSidebarInvite == undefined || $scope.rightSidebarInvite == '') {
                $scope.rightSidebarInvite = className;
            } else {
                $scope.rightSidebarInvite = '';
            }
        };

        $scope.set_invite = function($event, id){
            $scope.adding_invaite[id] = true;
            console.log(id, id_t)

            GamesFactory.msgInviteUser({game_id: id, user_id: id_t}).then(
                function(response){
                    console.log(response);
                },
                function(error){
                    console.log(error)
                }
            )
        };

    });
