controllers
    .controller('create-matchCtrl', function($scope, $cookies, $state, myGeocode, $timeout, FiltersFactory, GamesFactory, configFactory, $filter) {
        var timer = {};
        $scope.toDay = new Date();


        $scope.dates_arr = [];
        $scope.en_time = $filter('filterClockSlider')((new Date).getHours());
        $scope.set_timers = function(data) {
            var date_tmp = '';
            if (data) {
                data.forEach(function(el, i, arr) {
                    if (date_tmp == '') {
                        if (el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        } else {
                            $scope.dates_arr[0] = $scope.game_filter.dt;
                            date_tmp = $scope.game_filter.dt;
                        }
                    }
                    if (el.date) {
                        if (date_tmp != el.date && el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }
                        var startDate = new Date(el.date);
                        var dd = startDate.getDate();
                        var mm = startDate.getMonth() + 1; //January is 0!
                        var yyyy = startDate.getFullYear();
                        var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                        function getTimeRemaining(endtime) {

                            var t = Date.parse(endtime) - Date.parse(new Date());
                            var seconds = Math.floor((t / 1000) % 60);
                            var minutes = Math.floor((t / 1000 / 60) % 60);
                            var hours = Math.floor(t / (1000 * 60 * 60));


                            return {
                                'total': t,
                                'hours': hours,
                                'minutes': minutes,
                                'seconds': seconds
                            };
                        }

                        $scope.clock = [];
                        $scope.clock[el.game_id] = "00:00:00";
                        var tick = function() {
                            var t = getTimeRemaining(endtime);
                            $scope.clock[el.game_id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                            timer = $timeout(tick, 1000); // reset the timer
                        }
                        timer = $timeout(tick, 1000);
                    }
                })
            }
        }


        $scope.games_arr = [];

        $scope.game_filter = {
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false
            },
            access: 'All',
            start_time: 0, //(new Date).getHours(),
            end_time: 23,
            age: {
                min: 20,
                max: 60
            },
            dt: null,
            gender: 'All',
            sort: 'Soonest',
            address: '',
            distance: '',
            latitude: '',
            longitude: '',
            limit_start: 0,
            limit_end: 3,
            type: 'Gym'
        };

        configFactory.get({
            code: 'config',
            key: 'view_limit'
        }).then(

            function(response) {
                $scope.limit_view = parseInt(response.data.row.s_value);
                $scope.game_filter.limit_end = $scope.limit_view;
            },
            function(error) {
                console.log(error);
            }
        );

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }
        $scope.clearFil = function() {
            $scope.game_filter.sport = {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false
            };
            $scope.game_filter.access = 'All';
            $scope.game_filter.start_time = 0 // (new Date).getHours();
            $scope.game_filter.end_time = 23.45;
            $scope.game_filter.age = {
                min: 20,
                max: 60
            };
            $scope.game_filter.gender = 'All';
            $scope.game_filter.sort = 'Soonest';


            $scope.game_filter.dt = null; //new Date();

            setSliders();
            $scope.findGames();
        };


        var setSliders = function(date_val) {

            $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
            $scope.startTime = {
                id: 's_time',
                value: "12:00 am",
                options: {
                    stepsArray: $scope.startStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    draggableRange: true,
                    onEnd: function(sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);
                        if (time >= 24) {
                            time -= 24
                        }

                        $scope.game_filter.start_time = time;
                        $scope.en_time = modelValue;
                        $scope.findGames();


                    }
                }
            };
            $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
            $scope.endTime = {
                id: 'e_time',
                value: '11:45 pm',
                options: {
                    stepsArray: $scope.endStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    onEnd: function(sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);

                        $scope.game_filter.end_time = time;
                        $scope.findGames();
                    }
                }
            };
        }
        setSliders();

        $scope.ageSlider = {
            minValue: $scope.game_filter.age.min,
            maxValue: $scope.game_filter.age.max,
            options: {
                hideLimitLabels: true,
                showSelectionBar: true,
                floor: 10,
                ceil: 70,
                step: 1,
                onEnd: function(sliderId, minValue, maxValue) {
                    $scope.game_filter.age.min = minValue;
                    $scope.game_filter.age.max = maxValue;
                    $scope.findGames();
                }
            }
        }
        $scope.hintS = '';
        $scope.changeAddress = function($event) {
            console.log('here2');
            $timeout.cancel(timer);
            timer = $timeout(function() {
                if ($scope.game_filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.game_filter.distance = 1000000;
                    if ($scope.game_filter.address != null) {
                        if ($scope.game_filter.address.formatted_address) {
                            $scope.game_filter.address = $scope.game_filter.address.formatted_address;
                            $cookies.put('address', $scope.game_filter.address)
                            console.log($cookies.get('address'));
                            console.log($scope.game_filter.address);
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.game_filter.address).then(

                        function(response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.game_filter.latitude = coords['lat'];
                                $scope.game_filter.longitude = coords['lng'];

                                $scope.findGames();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.game_filter.distance = '';

                                $scope.game_filter.latitude = '';
                                $scope.game_filter.longitude = '';

                                $scope.findGames();
                            }
                        },
                        function(error) {}
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.game_filter.distance = '';

                    $scope.game_filter.latitude = '';
                    $scope.game_filter.longitude = '';

                    $scope.findGames();
                }
            }, 15);
        };
        $scope.setAddress = function(myloc) {
            if (!myloc) {
                myloc = false
            }
            var hintShowed = $cookies.get('hintShowed');
            if (hintShowed) {
                $scope.hintS = parseInt(hintShowed);
                console.log($cookies.get('address'));
                if ((typeof $cookies.get('address') != 'undefined') && (!myloc)) {
                    console.log('here1');
                    $scope.game_filter.address = $cookies.get('address')
                    $scope.changeAddress()
                } else if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var latitude = position.coords.latitude,
                            longitude = position.coords.longitude;

                        $scope.game_filter.latitude = latitude;
                        $scope.game_filter.longitude = longitude;

                        myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(

                            function(response) {
                                $scope.game_filter.address = response.data.results[0].formatted_address;
                                $cookies.put('address', $scope.game_filter.address)

                                $scope.distanceDisabled = false;
                                $scope.distanceClass = '';
                                $scope.game_filter.distance = 1000000;

                                $scope.findGames();
                            },
                            function(error) {
                                getProfleAddress();
                            }
                        );
                    }, function(geo) {
                        console.log(geo.message);
                        getProfleAddress();
                    });
                } else {
                    getProfleAddress();
                }
            } else {
                $cookies.put('hintShowed', '0');
                $scope.hintS = 0;
                console.log($scope.hintS)
            }
        };

        var getProfleAddress = function() {
            FiltersFactory.getProfileAddress().then(

                function(obj) {
                    myGeocode.getAddressCoordinate(obj.data.address).then(

                        function(response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.game_filter.address = obj.data.address;
                                $cookies.put('address', $scope.game_filter.address)
                                $scope.game_filter.latitude = coords['lat'];
                                $scope.game_filter.longitude = coords['lng'];

                                console.log($scope.game_filter.latitude, ', ', $scope.game_filter.longitude);

                                $scope.distanceDisabled = false;
                                $scope.distanceClass = '';
                                $scope.game_filter.distance = 1000000;

                                $scope.findGames();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.game_filter.distance = '';

                                $scope.game_filter.latitude = '';
                                $scope.game_filter.longitude = '';

                                $scope.findGames();
                            }
                        },
                        function(error) {}
                    );
                },
                function(error) {
                    console.log(error);
                }
            );
        }
        $scope.setAddress();

        var timer;

        $scope.today = function() {
            $scope.game_filter.dt = new Date();
            setSliders($scope.game_filter.dt);
        };
        // $scope.today();

        $scope.clear = function() {
            $scope.game_filter.dt = null;
        };

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        function disabled(data) {
            var date = data.date,
                today = new Date();
            now = new Date(today.getFullYear(), today.getMonth(), today.getDate());
            var status = true;
            if (date >= now)
                status = false;
            return status;
        }

        $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yyyy',
            min: new Date(),
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            showWeeks: false
        };

        $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.game_filter.dt = new Date(year, month, day);
            setSliders(new Date(year, month, day));
            $scope.findGames();
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [{
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $scope.findGames = function() {
            var hintShowed = $cookies.get('hintShowed');
            if (parseInt(hintShowed) > 2) {
                $timeout.cancel(timer);
                $scope.game_filter.limit_start = 0;
                $scope.game_filter.limit_end = parseInt($scope.limit_view);
                $scope.games_arr = [];
                $scope.$broadcast('rebuild:scroll');
                console.log('distanceDisabled ', !$scope.distanceDisabled);
                if (!$scope.distanceDisabled) {
                    GamesFactory.findGames($scope.game_filter).then(

                        function(response) {
                            console.log(response);
                            $scope.games_arr = response.data.rows;
                            console.log($scope.games_arr);
                            $scope.set_timers(response.data.rows);
                            $scope.urlBase = GamesFactory.urlBase;
                            if (response.data.rows) {
                                if (!(response.data.rows.length >= $scope.limit_view)) {
                                    $scope.game_filter.limit_start = 0;
                                    $scope.game_filter.limit_end = parseInt($scope.limit_view);
                                    $scope.infiniti = false;
                                    $scope.after_infiniti = true;
                                }
                            }
                        }
                    )

                    $scope.busy = false;
                    $scope.infiniti = true;
                    $scope.after_infiniti = false;


                    $scope.infinitiGames = function() {
                        if ($scope.busy) return;
                        console.log('infinitiGames - ');
                        if ($scope.infiniti) {
                            $scope.busy = true;
                            $scope.game_filter.limit_start = parseInt($scope.game_filter.limit_start) + parseInt($scope.limit_view);
                            $scope.game_filter.limit_end = parseInt($scope.game_filter.limit_end) + parseInt($scope.limit_view);
                            GamesFactory.findGames($scope.game_filter).then(

                                function(response) {
                                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                                    $scope.set_timers(response.data.rows);
                                    console.log('$scope.after_infiniti', !(response.data.rows.length >= $scope.limit_view));
                                    if (!(response.data.rows.length >= $scope.limit_view)) {
                                        $scope.game_filter.limit_start = -4;
                                        $scope.game_filter.limit_end = 0;
                                        $scope.infiniti = false;
                                        $scope.after_infiniti = true;
                                    }
                                    $scope.busy = false;
                                }
                            )
                        }

                        console.log('$scope.after_infiniti', $scope.infiniti);

                        if ($scope.after_infiniti) {
                            $scope.busy = true;
                            $scope.game_filter.limit_start = parseInt($scope.game_filter.limit_start) + parseInt($scope.limit_view);
                            $scope.game_filter.limit_end = parseInt($scope.game_filter.limit_end) + parseInt($scope.limit_view);
                            $scope.game_filter.typeS = 'Gym';
                            GamesFactory.findAfterGames($scope.game_filter).then(

                                function(response) {
                                    console.log('findAfterGames: ', response);
                                    $scope.games_arr = $scope.games_arr.concat(response.data.rows);
                                    $scope.set_timers(response.data.rows);
                                    if (!(response.data.rows.length >= $scope.limit_view)) {
                                        $scope.after_infiniti = false;
                                    }
                                    $scope.busy = false;
                                }
                            )
                        }
                    }
                }
            } else {
                $cookies.put('hintShowed', '1');
                $scope.hintS = 1;
                $('.pac-container').hide()
            }
        }

        $scope.joinGame = function(id) {
            $state.go('app.join-game', {
                id_game: id
            });
        }
        $scope.reserveGame = function(id) {
            $state.go('app.reserve-game', {
                id_game: id
            });
        }

        $scope.createGame = function(id) {
            console.log('$scope.game_filter', $scope.game_filter);
            GamesFactory.sendGameParams($scope.game_filter);
            $state.go('app.create-game', {
                id_gym: id
            });
        }

        $scope.watchGame = function(obj, id, val) {
            obj.watching = val;
            console.log('val ', val);
            GamesFactory.setWatchGame({
                id: id,
                value: val
            }).then(

                function(response) {
                    console.log(response);
                }
            )
        }

        $scope.$watch('game_filter.dt', function() {
            setSliders($scope.game_filter.dt)
            console.log('watch date');
        });
        // $scope.clearFil();
        $scope.hintHide = function() {
            $cookies.put('hintShowed', '3');
            $scope.hintS = 3;
            $scope.findGames();
        }

    });
