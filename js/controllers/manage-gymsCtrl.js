controllers.controller('manage-gymsCtrl', function ($scope, $state, clubFactory, gymFactory, $timeout) {
    $scope.filter_query = '';
    clubFactory.getTypeHours().then(
        function (response) {
            var data = response.data;
            $scope.gym = {
                club_id: data.id,
                name: '',
                type: data.type,
                gym_type: 'basketball',
                price: '',
                members: '',
                gym_sport: 'basketball',
                day: {
                    mon: {
                        start: 8,
                        end: 10
                    },
                    tue: {
                        start: 8,
                        end: 10
                    },
                    wed: {
                        start: 8,
                        end: 10
                    },
                    thu: {
                        start: 8,
                        end: 10
                    },
                    fri: {
                        start: 8,
                        end: 10
                    },
                    sat: {
                        start: 8,
                        end: 10
                    },
                    sun: {
                        start: 8,
                        end: 10
                    }
                }
            };

            $scope.gym_day = {mon: true, tue: true, wed: true, thu: true, fri: true, sat: false, sun: false};
            if (response.data.hours) {
                $scope.gym_day = {mon: false, tue: false, wed: false, thu: false, fri: false, sat: false, sun: false};
                var work_hours = JSON.parse(response.data.hours);
                for (var key in work_hours) {
                    $scope.gym_day[key] = true;
                    $scope.gym.day[key] = work_hours[key];
                }
            }
        },
        function (error) {

        }
    );


    function get24clock() {
        var array = [];
        for (var i = 1; i <= 24; i++) {
            array.push(i);
        }
        return array;
    }

    $scope.g_hours = get24clock();

    $scope.add_gym = function (data) {
        if (data.day) {
            for (var key in $scope.gym_day) {
                console.log($scope.gym_day[key]);
                if ($scope.gym_day[key] == false) {
                    if (data.day[key])
                        delete(data.day[key]);
                }
            }
        }
        gymFactory.createGym(data).then(
            function (response) {
                $state.reload();
                console.log(response)
            },
            function (error) {
                console.log(error)
            }
        )
    };


    $scope.update_gym = function (data) {
        console.log(data);
    };
    gymFactory.getAllGyms().then(
        function (response) {
            $scope.gymsObj = response.data;
            var data = response.data;
            var gym_up = [];
            var day = [];
            for (key in data) {
                gym_up[data[key].id] = {
                    name: data[key].name,
                    type: data[key].type,
                    gym_type: data[key].gym_type,
                    price: data[key].price,
                    members: data[key].number_members,
                    gym_sport: data[key].gym_sport,
                    day: {
                        mon: {
                            start: 8,
                            end: 10
                        },
                        tue: {
                            start: 8,
                            end: 10
                        },
                        wed: {
                            start: 8,
                            end: 10
                        },
                        thu: {
                            start: 8,
                            end: 10
                        },
                        fri: {
                            start: 8,
                            end: 10
                        },
                        sat: {
                            start: 8,
                            end: 10
                        },
                        sun: {
                            start: 8,
                            end: 10
                        }
                    }
                };
                day[data[key].id] = {mon: false, tue: false, wed: false, thu: false, fri: false, sat: false, sun: false};
                var work_hours = JSON.parse(data[key].hours);
                for (var day_key in work_hours) {
                    day[data[key].id][day_key] = true;
                    gym_up[data[key].id].day[day_key] = work_hours[day_key];
                }

            }
            $scope.gym_up = gym_up;
            $scope.gym_up_day = day;

            //console.log('gym_up_day',$scope.gym_up_day);
        },
        function (error) {
            console.log(error);
        }
    );

    $scope.update_gym = function (data, id) {
        if (data.day) {
            for (var key in $scope.gym_up_day[id]) {
                if ($scope.gym_up_day[id][key] == false) {
                    if (data.day[key])
                        delete(data.day[key]);
                }
            }
        }
        data.id = id;

        var loading_spiner = [],
            success_text = [];

        loading_spiner[id] = true;
        success_text[id] = false;

        $scope.loading_spiner = loading_spiner;
        $scope.success_text = success_text;
        
        gymFactory.updateGym(data).then(
            function (response) {
                $scope.success_text[id] = true;
                $scope.loading_spiner[id] = false;
                $scope.inform_text = 'This gym/class updated!';
                timer = $timeout(function() {
                    $scope.success_text[id] = false;
                    delete(timer);
                }, 5000);
                console.log(response);
            },
            function (error){
                $scope.success_text[id] = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner[id] = false;
                timer = $timeout(function() {
                    $scope.success_text[id] = false;
                    delete(timer);
                }, 5000);
                console.log(error);
            }
        )
    };

});
