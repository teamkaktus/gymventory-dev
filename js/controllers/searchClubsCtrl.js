controllers
    .controller('searchClubsCtrl', function ($scope, $cookies, $state, myGeocode, $timeout, FiltersFactory, ngDialog, clubFactory, $filter, toaster) {
        var timer;
        $scope.filter = {
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            start_time: 8,
            end_time: 10,
            address: '',
            business_name: '',
            distance: '',
            latitude: '',
            longitude: ''
        };
        $scope.clearFil = function () {
          $scope.filter = {
              sport: {
                  1: false,
                  2: false,
                  3: false,
                  4: false,
                  5: false,
                  6: false,
                  7: false,
                  8: false,
                  9: false,
                  10: false,
                  11: false,
                  12: false,
                  13: false,
                  14: false,
                  15: false,
                  16: false
              },
              start_time: 01,
              end_time: 11,
              business_name: '',
              distance: '',
          };
          $scope.changeAddress();
        }

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        $scope.club_cards = [];


        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        $scope.addFavorite = function(club_id,index) {
          clubFactory.addFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards[index].favorite = 1
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Max count of faivorite",
                  description:"You can add only 5 faivorite clubs"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            alert('err')
            console.log(err);

          })
        }
        $scope.removeFavorite = function(club_id,index) {
          clubFactory.removeFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards[index].favorite = 0
                    // $scope.club_cards.push($scope.club_favorite_cards[index]);
                    // $scope.club_favorite_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Error",
                  description:"Db Error"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            $scope.text = {
              text:"Error",
              description:"Db Error"
            }
            var dialog = ngDialog.open({
                templateUrl: './templates/notification/show_textTplS.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

          })
        }
        $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
        $scope.startTime = {
            id: 's_time',
            value: "08:00 am",
            options: {
                stepsArray: $scope.startStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                draggableRange: true,
                onEnd: function (sliderId, modelValue) {
                    var time = $filter('convertTime')(modelValue);
                    if (time>=24) {
                        time-=24
                    }

                    $scope.filter.start_time = time;
                    $scope.setFilter();


                },
                onChange: function (sliderId, modelValue, highValue, pointerType){
                    $scope.en_time = modelValue;
                    var start = $filter('convertTime')(modelValue);
                    var end = $filter('convertTime')($scope.endTime.value);

                    if(start >= end){
                        var i = $scope.startStepArray.indexOf(modelValue);
                        $scope.endTime.value = $scope.endStepArray[i];
                    }
                }
            }
        };
        $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
        $scope.endTime = {
            id: 'e_time',
            value: "10:00 pm",
            options: {
                stepsArray: $scope.endStepArray,
                precision: 2,
                minLimit: 0,
                maxLimit: 100,
                hideLimitLabels: true,
                showSelectionBar: true,
                onEnd: function (sliderId, modelValue) {
                    var time = $filter('convertTime')(modelValue);

                    $scope.filter.end_time = time;
                    $scope.setFilter();
                },
                onChange: function (sliderId, modelValue, highValue, pointerType){
                    var end = $filter('convertTime')(modelValue);
                    var start = $filter('convertTime')($scope.startTime.value);

                    if(start >= end){
                        var i = $scope.endStepArray.indexOf(modelValue);
                        $scope.startTime.value = $scope.startStepArray[i];
                    }
                }
            }
        };

        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };
        $scope.changeAddress = function ($event) {
            $timeout.cancel(timer);

            timer = $timeout(function () {
                if ($scope.filter.address != '') {
                    $scope.distanceDisabled = false;
                    $scope.distanceClass = '';
                    $scope.filter.distance = 1000000;
                    if ($scope.filter.address != null) {
                        if ($scope.filter.address.formatted_address) {
                            $scope.filter.address = $scope.filter.address.formatted_address;
                            $cookies.put('address',$scope.filter.address)
                        }
                    }

                    myGeocode.getAddressCoordinate($scope.filter.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.filter.latitude = coords['lat'];
                                $scope.filter.longitude = coords['lng'];

                                $scope.map.center.latitude = coords['lat'];
                                $scope.map.center.longitude = coords['lng'];

                                $scope.setFilter();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.filter.distance = '';

                                $scope.filter.latitude = '';
                                $scope.filter.longitude = '';

                                $scope.setFilter();
                            }
                        },
                        function (error) {
                        }
                    )
                } else {
                    $scope.distanceDisabled = true;
                    $scope.distanceClass = 'disabled';
                    $scope.filter.distance = '';

                    $scope.filter.latitude = '';
                    $scope.filter.longitude = '';

                    $scope.setFilter();
                }
            }, 10);
        };
        $scope.hintCl = '';
        $scope.setAddress = function (myloc) {
          if (!myloc) {myloc=false}

            var hintClub = $cookies.get('hintClub');
            console.log(hintClub);
            if(hintClub){
                $scope.hintCl = parseInt(hintClub);
                if ((typeof $cookies.get('address') !='undefined')&&(!myloc)) {
                  $scope.filter.address = $cookies.get('address')
                  $scope.changeAddress()
                }else if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var latitude = position.coords.latitude,
                            longitude = position.coords.longitude;

                        $scope.filter.latitude = latitude;
                        $scope.filter.longitude = longitude;

                        $scope.map.center.latitude = latitude;
                        $scope.map.center.longitude = longitude;

                        myGeocode.coordinateToAddress(latitude + ', ' + longitude).then(
                            function (response) {
                                $scope.filter.address = response.data.results[0].formatted_address;
                                $cookies.put('address',$scope.filter.address)
                                $scope.distanceDisabled = false;
                                $scope.distanceClass = '';
                                $scope.filter.distance = 1000000;

                                $scope.setFilter();
                            },
                            function (error) {
                                getProfleAddress();
                            }
                        );
                    }, function (geo) {
                        getProfleAddress();
                    });
                } else {
                    getProfleAddress();
                }
            }else{
                $cookies.put('hintClub', '0');
                $scope.hintCl = 0;
                console.log($scope.hintCl)
            }

        };

        var getProfleAddress = function () {
            FiltersFactory.getProfileAddress().then(
                function (obj) {
                    myGeocode.getAddressCoordinate(obj.data.address).then(
                        function (response) {
                            if (response.data.status != "ZERO_RESULTS") {
                                var coords = response.data.results[0].geometry.location;

                                $scope.filter.address = obj.data.address;
                                $cookies.put('address',$scope.filter.address)
                                $scope.filter.latitude = coords['lat'];
                                $scope.filter.longitude = coords['lng'];

                                $scope.map.center.latitude = coords['lat'];
                                $scope.map.center.longitude = coords['lng'];

                                console.log($scope.filter.latitude, ', ', $scope.filter.longitude);

                                $scope.distanceDisabled = false;
                                $scope.distanceClass = '';
                                $scope.filter.distance = 1000000;

                                $scope.setFilter();
                            } else {
                                $scope.distanceDisabled = true;
                                $scope.distanceClass = 'disabled';
                                $scope.filter.distance = '';

                                $scope.filter.latitude = '';
                                $scope.filter.longitude = '';

                                $scope.setFilter();
                            }
                        },
                        function (error) {
                        }
                    );
                },
                function (error) {
                    console.log(error);
                }
            );
        }

        $scope.setAddress();

        var timer;

        $scope.setFilter = function () {
            var hintClub = $cookies.get('hintClub');
            if(parseInt(hintClub) > 2){
              console.log($scope.filter);
                $scope.$broadcast('rebuild:scroll');
                FiltersFactory.clubFilters($scope.filter).then(
                    function (response) {
                        var markers = {};
                        for(var card in response.data.rows){
                            response.data.rows[card].hours = JSON.parse(response.data.rows[card].hours);
                            markers[card] = {
                                id: response.data.rows[card].id,
                                coords: {
                                    latitude: response.data.rows[card].latitude,
                                    longitude: response.data.rows[card].longitude
                                },
                                options: {
                                    icon: marker_url
                                }
                            }
                        }
                        $scope.markers = markers;
                        $scope.club_cards = response.data.rows;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }else{
                $cookies.put('hintClub', '1');
                $scope.hintCl = 1;
                $('.pac-container').hide()
            }
        };

        $scope.requestMembership = function (id, index, price, name, type) {
            if(price > 0){
                ngDialog.open({
                    templateUrl: './templates/club-cc-paymentTpl.html',
                    className: 'ngdialog-theme-default',
                    controller: 'club_paymentCtrl',
                    scope: $scope
                });

                $scope.$on('ngDialog.opened', function (e, $dialog) {
                    $scope.$broadcast('clubPayParams', {
                        club: id,
                        price: price,
                        credits: price,
                        name: name,
                        index: index
                    });

                });
            }else {
                if(type == 'private') {
                    clubFactory.managerRequest({club_id: id}).then(
                        function (response) {
                            if (response.data.status) {
                                toaster.pop({type: 'note', title: "Request Membership", body: "Sent club manager"});
                            } else {
                                toaster.pop({type: 'error', title: "Request Membership", body: "Not send!"});
                            }
                        }
                    )
                }else{
                    clubFactory.addMember({club_id: id}).then(
                        function (response) {
                            if(response.status == 200) {
                                $scope.club_cards[index].me_member = 1;
                                $scope.club_cards[index].members += 1;
                            }
                        }
                    );
                }
            }
        };
        $scope.removeMembership = function (id, index) {
            clubFactory.deleteMember({club_id: id}).then(
                function (response) {
                    if(response.status == 200) {
                        $scope.club_cards[index].me_member = 0;
                        $scope.club_cards[index].members -= 1;
                    }
                }
            );
        }

        $scope.hintHide = function(){
            $cookies.put('hintClub', '3');
            $scope.hintCl = 3;
            $scope.setFilter();
        }
        $scope.$on('requestClubPay', function (index) {
            $state.reload();

        })

    });
