controllers.controller('become-partnerCtrl', function ($scope, $state, userProfile, AuthFactory, clubFactory, $timeout, ngDialog) {

    $scope.loading_spiner = false;
    $scope.success_text = false;

    if($scope.partner && $scope.partner.name && $scope.partner.address){
        $scope.disabled_name = true;
        $scope.disabled_address = true;
    }

    userProfile.getUserData().then(
        function (response) {
            var data = response.data;
            $scope.become = {
                first_name: data.first_name,
                last_name: data.last_name,
                dial_code: data.dial_code,
                phone: data.phone,
                email: data.email,
                club_name: ($scope.partner.name)?$scope.partner.name:null,
                club_address: ($scope.partner.address)?$scope.partner.address:null,
                coordinates: null
            };
        },
        function (error) {
            console.log('error');
        }
    );

    $scope.check = true;
    $scope.chekSpiner = false;
    $scope.chekText = '';

    $scope.checkName = function (name) {
        var timer = {};
        $scope.chekSpiner = true;
        $scope.chekText = '';
        timer = $timeout(function () {
            clubFactory.uniqueClubs(name).then(
                function (response) {
                    if (response.data.status == true && response.data.url == 'no-isset') {
                        $scope.chekText = 'Success!';
                        $scope.check = true;
                        $scope.slug = response.data.slug;
                    } else {
                        $scope.chekText = 'Name used!';
                        $scope.check = false;
                    }
                    $scope.chekSpiner = false;
                },
                function (error) {
                    $scope.chekSpiner = false;
                    $scope.check = false;
                    console.log(error);
                }
            )
            delete(timer);
        }, 100);
    }

    AuthFactory.telephone()
        .then(
            function (response) {
                $scope.telephone = response.data;
            },
            function (error) {
                console.log('error');
            }
        );
    $scope.createClub = function (become) {

        if (become.club_address != null) {
            if (become.club_address.formatted_address) {
                become.club_address = become.club_address.formatted_address;
            }
        } else {
            become.club_address = '';
        }

        var valid = true,
            timer;

        $scope.ph = {
            first_name: 'First Name',
            last_name: 'Last Name',
            phone: 'Phone Number',
            email: 'Email Address',
            club_name: 'Club Name',
            club_address: 'Club Address'
        };
        if ($scope.become.first_name == '') {
            valid = false;
            $scope.ph.first_name = "First Name can't be blank";
        }
        if ($scope.become.last_name == '') {
            valid = false;
            $scope.ph.last_name = "Last Name can't be blank";
        }
        if ($scope.become.phone == '') {
            valid = false;
            $scope.ph.phone = "Phone Number can't be blank";
        }
        if ($scope.become.email == '') {
            valid = false;
            $scope.ph.email = "Email Address can't be blank";
        }
        if ($scope.become.club_name == '' || $scope.become.club_name == null) {
            valid = false;
            $scope.ph.club_name = "Club Name can't be blank";
        }
        if ($scope.become.club_address == '' || $scope.become.club_address == null) {
            valid = false;
            $scope.ph.club_address = "Club Address can't be blank";
        }
        if (valid) {
            $scope.loading_spiner = false;
            $scope.loading_spiner = true;
            clubFactory.getAddressCoordinate($scope.become.club_address).then(
                function (response) {
                    $scope.become.coordinates = response.data.results[0].geometry.location;
                    if($scope.disabled_name && $scope.disabled_address){

                        become.id = $scope.partner.id;
                        clubFactory.requestOwnership(become).then(
                            function(response){
                                console.log(response);
                                $scope.success_text = true;
                                $scope.loading_spiner = false;
                                $scope.inform_text = 'Request for the establishment of the club added!';
                                timer = $timeout(function () {
                                    $scope.success_text = false;
                                    ngDialog.closeAll(true);
                                    delete(timer);
                                    $state.go('app.manage_clubs');
                                }, 5000);
                            },
                            function(error){
                                $scope.success_text = true;
                                $scope.inform_text = 'There was an error when saving!';
                                $scope.loading_spiner = false;
                                timer = $timeout(function () {
                                    $scope.success_text = false;
                                    ngDialog.closeAll(true);
                                    delete(timer);
                                }, 5000);
                            }
                        )
                    }else {
                        if($scope.check) {
                            become.slug = $scope.slug;
                            clubFactory.createClub(become).then(
                                function (response) {
                                    $scope.success_text = true;
                                    $scope.loading_spiner = false;
                                    $scope.inform_text = 'Request for the establishment of the club added!';
                                    timer = $timeout(function () {
                                        $scope.success_text = false;
                                        ngDialog.closeAll(true);
                                        delete(timer);
                                        $state.go('app.manage_clubs');
                                    }, 5000);
                                },
                                function (error) {
                                    $scope.success_text = true;
                                    $scope.inform_text = 'There was an error when saving!';
                                    $scope.loading_spiner = false;
                                    timer = $timeout(function () {
                                        $scope.success_text = false;
                                        ngDialog.closeAll(true);
                                        delete(timer);
                                    }, 5000);
                                }
                            );
                        }
                    }
                },
                function (error) {
                    $scope.success_text = true;
                    $scope.inform_text = 'Not valid address!';
                    $scope.loading_spiner = false;
                    timer = $timeout(function () {
                        $scope.success_text = false;
                        ngDialog.closeAll(true);
                        delete(timer);
                    }, 5000);
                }
            );
        }
    }


});