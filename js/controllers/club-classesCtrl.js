controllers.controller('club-classesCtrl', function ($scope, $rootScope, clubFactory, gymFactory, $timeout, $state) {
    $scope.create_new = false;
    $rootScope.club = {
        create: false
    };

    $scope.add_new = function () {
        $scope.create_new = true;
    };

    $scope.check_club = false;
    $scope.inform_text = false;

    clubFactory.countClub().then(
        function (response) {
            if (parseInt(response.data.count) >= 1) {
                $rootScope.club.create = false;
                console.log("count club", $rootScope.club.create)
            } else {
                $rootScope.club.create = true;
                if($rootScope.club.create){
                    $state.go('app.createMyClub');
                }
            }
        },
        function (error) {
            console.log(error);
        }
    );

    clubFactory.getTypeHours().then(
        function (response) {
            var data = response.data;
            $scope.gym = {
                club_id: data.id,
                name: '',
                type: data.type,
                add_gym_type: 'Class',
                gym_type: 'basketball',
                price: '',
                members: '',
                min_members: '',
                gym_sport: 'basketball',
            };
        },
        function (error) {

        }
    );

    $scope.add_gym = function (data) {
        gymFactory.createGym(data).then(
            function (response) {
                $state.reload();
                console.log(response)
            },
            function (error) {
                console.log(error)
            }
        )
    };

    gymFactory.getAllGyms("Class").then(
        function (response) {

            $scope.gymsObj = response.data;
        },
        function (error) {
            console.log(error);
        }
    );

    $scope.update_gym = function (data, id) {
        if (data.day) {
            for (var key in $scope.gym_up_day[id]) {
                if ($scope.gym_up_day[id][key] == false) {
                    if (data.day[key])
                        delete(data.day[key]);
                }
            }
        }
        data.id = id;

        var loading_spiner = [],
            success_text = [];

        loading_spiner[id] = true;
        success_text[id] = false;

        $scope.loading_spiner = loading_spiner;
        $scope.success_text = success_text;

        gymFactory.updateGym(data).then(
            function (response) {
                $scope.success_text[id] = true;
                $scope.loading_spiner[id] = false;
                $scope.inform_text = 'This gym/class updated!';
                timer = $timeout(function() {
                    $scope.success_text[id] = false;
                    delete(timer);
                }, 5000);
                console.log(response);
            },
            function (error){
                $scope.success_text[id] = true;
                $scope.inform_text = 'There was an error when saving!';
                $scope.loading_spiner[id] = false;
                timer = $timeout(function() {
                    $scope.success_text[id] = false;
                    delete(timer);
                }, 5000);
                console.log(error);
            }
        )
    };

    $scope.manage = function(id){
        $state.go('app.classesItem', {id: id});
    }

});
