controllers
    .controller('club-pageCtrl',  function ($scope, $state, $rootScope, ConfigApi, userProfile, $filter, ngDialog, AuthFactory, toaster, $stateParams, GamesFactory, $timeout, FiltersFactory, myGeocode, clubFactory) {
        var club_id = $stateParams.club_id;
        var timer;

        $scope.dates_arr = [];
        $scope.addFavorite = function(club_id,index) {
          clubFactory.addFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 1
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTpl.html',
                    className: 'ngdialog-theme-default',
                    controller: 'show_textCtrl',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            $scope.text = {
              text:"Error",
              description:"Db Error"
            }
            var dialog = ngDialog.open({
                templateUrl: './templates/notification/show_textTplS.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

          })
        }
        $scope.removeFavorite = function(club_id,index) {
          clubFactory.removeFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 0
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Error",
                  description:"Db Error"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            $scope.text = {
              text:"Error",
              description:"Db Error"
            }
            var dialog = ngDialog.open({
                templateUrl: './templates/notification/show_textTplS.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

          })
        }
        $scope.set_timers = function (data) {
            var date_tmp = '';
            if(data) {
                data.forEach(function (el, i, arr) {
                    if(date_tmp == ''){
                        if(el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }else{
                            $scope.dates_arr[0] = $scope.filter.dt;
                            date_tmp = $scope.filter.dt;
                        }
                    }
                    if (el.date) {
                        if(date_tmp != el.date && el.date && el.game_id){
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }
                        var startDate = new Date(el.date);
                        var dd = startDate.getDate();
                        var mm = startDate.getMonth() + 1; //January is 0!
                        var yyyy = startDate.getFullYear();
                        var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                        function getTimeRemaining(endtime) {

                            var t = Date.parse(endtime) - Date.parse(new Date());
                            var seconds = Math.floor((t / 1000) % 60);
                            var minutes = Math.floor((t / 1000 / 60) % 60);
                            var hours = Math.floor(t / (1000 * 60 * 60));


                            return {
                                'total': t,
                                'hours': hours,
                                'minutes': minutes,
                                'seconds': seconds
                            };
                        }

                        $scope.clock = [];
                        $scope.clock[el.game_id] = "00:00:00";
                        var tick = function () {
                            var t = getTimeRemaining(endtime);
                            $scope.clock[el.game_id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                            timer = $timeout(tick, 1000); // reset the timer
                        }
                        timer = $timeout(tick, 1000);
                    }
                })
            }
        }

        $scope.en_time = $filter('filterClockSlider')((new Date).getHours());
        $scope.tab_club_page = "Gym";

        $scope.filter = {
            club_id: club_id,
            sport: {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                12: false,
                13: false,
                14: false,
                15: false,
                16: false
            },
            access: 'All',
            start_time : 0,
            end_time : 23.45,
            sort: 'Soonest',
            age: {
                min: 20,
                max: 60
            },
            dt: new Date(),
            gender: 'All'
        };
        //console.log("online", $rootScope.users.online);

        $scope.clearFil = function () {
            $scope.filter.sport = {
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false
            };
            $scope.filter.access = 'All';
            $scope.filter.start_time = 0;
            $scope.filter.end_time = 23.45;
            $scope.filter.age = {
                min: 20,
                max: 60
            };
            $scope.filter.gender = 'All';
            $scope.filter.sort = 'Soonest';

            setSliders();

            $scope.filter.dt = new Date();
            $scope.setFilter();
        };

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        $scope.club_cards = [];


        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }

        var set_time = function (arg, arr, date_val) {
            console.log('date_val', date_val);
            var to_hour = (new Date).getHours();
            var limit = 0,
                start = "08:00 am",
                end = "11:00 pm";
            switch(arg){
                case "start":{
                    if((new Date).getMonth() == (date_val).getMonth() && (new Date).getDate() == (date_val).getDate()){
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour+1));
                        start = $filter('filterClockSlider')(to_hour+1);
                    }else{
                        limit = 0;
                    }

                    return {start: start, limit: limit};
                }
                case "end":{
                    if((new Date).getMonth() == ($scope.filter.dt).getMonth() && (new Date).getDate() == ($scope.filter.dt).getDate()){
                        limit = arr.indexOf($filter('filterClockSlider')(to_hour+2));
                        //end = $filter('filterClockSlider')(23);
                    }else{
                        limit = 0;
                    }
                    return {end: end, limit: limit};
                }
            }
        }
        var setSliders = function(date_val){
            $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
            $scope.startTime = {
                id: 's_time',
                value: "12:00 am",
                options: {
                    stepsArray: $scope.startStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    draggableRange: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);
                        if (time>=24) {
                            time-=24
                        }

                        $scope.game_filter.start_time = time;
                        $scope.en_time = modelValue;
                        $scope.setFilter();


                    }
                }
            };
            $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
            $scope.endTime = {
                id: 'e_time',
                value: '11:45 pm',
                options: {
                    stepsArray: $scope.endStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);

                        $scope.game_filter.end_time = time;
                        $scope.findGames();
                    }
                }
            };
        }
        setSliders();

        $scope.ageSlider = {
            minValue: $scope.filter.age.min,
            maxValue: $scope.filter.age.max,
            options: {
                hideLimitLabels: true,
                showSelectionBar: true,
                floor: 10,
                ceil: 70,
                step: 1,
                onEnd: function (sliderId, minValue, maxValue) {
                    $scope.filter.age.min = minValue;
                    $scope.filter.age.max = maxValue;
                    $scope.setFilter();
                }
            }
        };

        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        $scope.options = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: stylesArray
        };


        clubFactory.getClub(club_id).then(
            function (response) {
                if(response.data.hours) {
                    $scope.club_cards = response.data;

                    response.data.hours = JSON.parse(response.data.hours);
                    $scope.marker = {
                        id: 1,
                        coords: {
                            latitude: response.data.latitude,
                            longitude: response.data.longitude
                        },
                        options: {
                            icon: marker_url
                        }
                    }
                    $scope.map.center.latitude = $scope.marker.coords.latitude;
                    $scope.map.center.longitude = $scope.marker.coords.longitude;
                    $scope.$broadcast('rebuild:scroll');
                }
                console.log($scope.club_cards);
            }
        );



        $scope.today = function() {
            $scope.filter.dt = new Date();
            setSliders($scope.filter.dt);
        };
        $scope.today();

        $scope.clear = function() {
            $scope.filter.dt = null;
        };

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        function disabled(data) {
            var date = data.date,
                today = new Date();
            now = new Date(today.getFullYear(),today.getMonth(),today.getDate());
            var status = true;
            if(date >= now)
                status = false;
            return status;
        }

        $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yyyy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            showWeeks: false
        };

        $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.filter.dt = new Date(year, month, day);
            setSliders($scope.filter.dt);
            $scope.setFilter();
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $scope.setFilter = function () {
            $scope.games_arr = [];
            $timeout.cancel(timer);
            clubFactory.getClubGames({filter: $scope.filter, id: club_id}).then(
                function (response) {
                    $scope.games_arr = response.data.rows;
                    $scope.set_timers(response.data.rows);
                    console.log('games ', response)
                }
            );
        }

        $scope.setFilter();

        $scope.joinGame = function(id){
            $state.go('app.join-game', {id_game: id});
        }

        $scope.createGame = function(id){
            GamesFactory.sendGameParams($scope.filter);
            $state.go('app.create-game', {id_gym: id});
        }

        $scope.watchGame = function (id) {
            GamesFactory.setWatchGame({id: id}).then(
                function (response) {
                    console.log(response);
                }
            )
        }
        $scope.addFavorite = function(club_id) {
          clubFactory.addFavorite(club_id).then(function (res) {
            if (res.data.status) {
              if (res.data.rows.affectedRows) {
                    $scope.club_cards.favorite = 1
                    // $scope.club_favorite_cards.push($scope.club_cards[index]);
                    // $scope.club_cards.splice(index,1);
                $
              }else {
                $scope.text = {
                  text:"Max count of faivorite",
                  description:"You can add only 5 faivorite clubs"
                }
                var dialog = ngDialog.open({
                    templateUrl: './templates/notification/show_textTplS.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
              }
            }
            console.log(res);
          },function (err) {
            alert('err')
            console.log(err);

          })
        }
        $scope.$watch('filter.dt', function(){
            setSliders($scope.filter.dt)
        });
        $scope.requestMembership = function (id, price, name, type) {
            if(price > 0){
                ngDialog.open({
                    templateUrl: './templates/club-cc-paymentTpl.html',
                    className: 'ngdialog-theme-default',
                    controller: 'club_paymentCtrl',
                    scope: $scope
                });

                $scope.$on('ngDialog.opened', function (e, $dialog) {
                    $scope.$broadcast('clubPayParams', {
                        club: id,
                        price: price,
                        credits: price,
                        name: name
                    });

                });
            }else {
                if(type == 'private') {
                    clubFactory.managerRequest({club_id: id}).then(
                        function (response) {
                            if (response.data.status) {
                                toaster.pop({type: 'note', title: "Request Membership", body: "Sent club manager"});
                            } else {
                                toaster.pop({type: 'error', title: "Request Membership", body: "Not send!"});
                            }
                        }
                    )
                }else{
                    clubFactory.addMember({club_id: id}).then(
                        function (response) {
                            if(response.status == 200) {
                                $scope.club_cards.me_member = 1;
                                $scope.club_cards.members += 1;
                            }
                        }
                    );
                }
            }
        };

        $scope.removeMembership = function (id, index) {
            clubFactory.deleteMember({club_id: id}).then(
                function (response) {
                    if(response.status == 200) {
                        $scope.club_cards.me_member = 0;
                        $scope.club_cards.members -= 1;
                    }
                }
            );
        }

        $scope.$on('requestClubPay', function (index) {
            $state.reload();

        })
    });
