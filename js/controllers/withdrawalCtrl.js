controllers
    .controller('withdrawalCtrl', function ($scope, $rootScope, CreditsFactory, configFactory, ngDialog) {
        $scope.withdrawal = {
            all_credits: 0,
            percent: 0,
            minimum: 0,
            avaible: 0,
            disable: false,
            showBtn: false
        };

        configFactory.get({code: 'config', key: 'payout_limit'}).then(
            function (response){
                $scope.withdrawal.minimum = response.data.row.s_value;
            },
            function (error){
                console.log(error);
            }
        );

        $scope.history = []; 

        CreditsFactory.getPayoutsHistory().then(
            function(response){
                $scope.history = response.data.rows;
            },
            function(error){
                console.log(error);
            }
        )

        CreditsFactory.getBalance().then(
            function (response){
                $scope.withdrawal.all_credits = response.data.balance;
                $scope.withdrawal.percent = response.data.percent;
                $scope.withdrawal.avaible = ($scope.withdrawal.all_credits).toFixed(2);

                if( parseFloat($scope.withdrawal.minimum) <= parseFloat($scope.withdrawal.avaible) ){
                    $scope.withdrawal.showBtn = true;
                }else{
                    $scope.withdrawal.showBtn = false;
                }
            },
            function (error){
                console.log(error);
            }
        );

        $rootScope.$on('enabledBtn', function(event, data){
            $scope.$apply(); 
        })

        $scope.createWithdrawal = function(){
            $scope.withdrawal.disable = true;
            $scope.dialog = ngDialog.open({
                templateUrl: './templates/create-withdrawalTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'createWithdrawalCtrl',
                scope: $scope
            });
        }

    });