controllers
    .controller('findToCreateClubCtrl', function ($scope, ngDialog, clubFactory) {
        console.info('findToCreateClubCtrl');

        $scope.club_cards = [];
        $scope.empty_cards = false;

        $scope.become_partner = function (obj) {
            $scope.partner = {};
            if(obj){
                $scope.partner = obj
            }

            var bp = ngDialog.open({
                template: '/templates/become-partnerTpl.html',
                className: 'ngdialog-theme-default',
                controller: 'become-partnerCtrl',
                scope: $scope
            });
            bp.closePromise.then(function (data) {
                //alert('close');
            });
        };

        clubFactory.filterClubNA({business: ''}).then(
            function (response) {
                for(var card in response.data.rows){
                    response.data.rows[card].hours = JSON.parse(response.data.rows[card].hours);
                }
                $scope.club_cards = response.data.rows;
                console.log($scope.club_cards);
            }
        )

        $scope.changeFind = function () {
            clubFactory.filterClubNA({business: $scope.business}).then(
                function (response) {
                    for(var card in response.data.rows){
                        response.data.rows[card].hours = JSON.parse(response.data.rows[card].hours);
                    }
                    $scope.club_cards = response.data.rows;
                    console.log(response.data.rows.length);
                    if($scope.club_cards.length == 0){
                        $scope.empty_cards = true;
                    }else{
                        $scope.empty_cards = false;
                    }
                }
            )
        }

    });