controllers
    .controller('messagesCtrl', function($scope, $filter, $rootScope, myGeocode, $timeout, $state, ngDialog, FiltersFactory, GamesFactory, MessagesFactory, TeammatesFactory) {
        $scope.$broadcast('rebuild:scroll');
        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }
        $scope.games_arr = [];
        $scope.filter = 'all';
        var timer = {};
        $scope.toDay = new Date();


        $scope.dates_arr = [];
        $scope.en_time = $filter('filterClockSlider')((new Date).getHours());
        $scope.set_timers = function (data) {
            var date_tmp = '';
            if(data) {
                data.forEach(function (el, i, arr) {
                    if(date_tmp == ''){
                        if(el.date && el.game_id) {
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }else{
                            $scope.dates_arr[0] = $scope.game_filter.dt;
                            date_tmp = $scope.game_filter.dt;
                        }
                    }
                    if (el.date) {
                        if(date_tmp != el.date && el.date && el.game_id){
                            $scope.dates_arr[el.game_id] = el.date;
                            date_tmp = el.date;
                        }
                        var startDate = new Date(el.date);
                        var dd = startDate.getDate();
                        var mm = startDate.getMonth() + 1; //January is 0!
                        var yyyy = startDate.getFullYear();
                        var endtime = yyyy + '-' + mm + '-' + dd + ' ' + el.start_time.toString().replace('.', ':') + ':00';


                        function getTimeRemaining(endtime) {

                            var t = Date.parse(endtime) - Date.parse(new Date());
                            var seconds = Math.floor((t / 1000) % 60);
                            var minutes = Math.floor((t / 1000 / 60) % 60);
                            var hours = Math.floor(t / (1000 * 60 * 60));


                            return {
                                'total': t,
                                'hours': hours,
                                'minutes': minutes,
                                'seconds': seconds
                            };
                        }

                        $scope.clock = [];
                        $scope.clock[el.game_id] = "00:00:00";
                        var tick = function () {
                            var t = getTimeRemaining(endtime);
                            $scope.clock[el.game_id] = t.hours + ':' + t.minutes + ':' + t.seconds;
                            timer = $timeout(tick, 1000); // reset the timer
                        }
                        timer = $timeout(tick, 1000);
                    }
                })
            }
        }


        $scope.games_arr = [];

        $scope.game_filter = {
          sport: {
              1: false,
              2: false,
              3: false,
              4: false,
              5: false,
              6: false,
              7: false,
              8: false,
              9: false,
              10: false,
              11: false,
              12: false,
              13: false,
              14: false,
              15: false,
              16: false
          },
            access: 'All',
            start_time: 0,
            end_time: 23.45,
            age: {
                min: 20,
                max: 60
            },
            gender: 'All',
            sort: 'Soonest',
            address: '',
            distance: '',
            latitude: '',
            longitude: '',
            limit_start: 0,
            limit_end: 3,
            type: 'Gym'
        };

        $scope.myPosition = true;
        $scope.myAddress = true;
        $scope.distanceDisabled = true;
        $scope.distanceClass = 'disabled';

        if (!navigator.geolocation) {
            $scope.myPosition = false;
            return false;
        }


        var setSliders = function(date_val){

            $scope.startStepArray = ['12:00 am', '12:15 am', '12:30 am', '12:45 am', '01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm'];
            $scope.startTime = {
                id: 's_time',
                value: "12:00 am",
                options: {
                    stepsArray: $scope.startStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    draggableRange: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);
                        if (time>=24) {
                          time-=24
                        }
                        $scope.game_filter.start_time = time;
                        console.log($scope.game_filter.start_time);
                        $scope.en_time = modelValue;
                        $scope.getChatRooms();


                    }
                }
            };
            $scope.endStepArray = ['01:00 am', '01:15 am', '01:30 am', '01:45 am', '02:00 am', '02:15 am', '02:30 am', '02:45 am', '03:00 am', '03:15 am', '03:30 am', '03:45 am', '04:00 am', '04:15 am', '04:30 am', '04:45 am', '05:00 am', '05:15 am', '05:30 am', '05:45 am', '06:00 am', '06:15 am', '06:30 am', '06:45 am', '07:00 am', '07:15 am', '07:30 am', '07:45 am', '08:00 am', '08:15 am', '08:30 am', '08:45 am', '09:00 am', '09:15 am', '09:30 am', '09:45 am', '10:00 am', '10:15 am', '10:30 am', '10:45 am', '11:00 am', '11:15 am', '11:30 am', '11:45 am', '12:00 pm', '12:15 pm', '12:30 pm', '12:45 pm', '01:00 pm', '01:15 pm', '01:30 pm', '01:45 pm', '02:00 pm', '02:15 pm', '02:30 pm', '02:45 pm', '03:00 pm', '03:15 pm', '03:30 pm', '03:45 pm', '04:00 pm', '04:15 pm', '04:30 pm', '04:45 pm', '05:00 pm', '05:15 pm', '05:30 pm', '05:45 pm', '06:00 pm', '06:15 pm', '06:30 pm', '06:45 pm', '07:00 pm', '07:15 pm', '07:30 pm', '07:45 pm', '08:00 pm', '08:15 pm', '08:30 pm', '08:45 pm', '09:00 pm', '09:15 pm', '09:30 pm', '09:45 pm', '10:00 pm', '10:15 pm', '10:30 pm', '10:45 pm', '11:00 pm', '11:15 pm', '11:30 pm', '11:45 pm'];
            $scope.endTime = {
                id: 'e_time',
                value: '11:45 pm',
                options: {
                    stepsArray: $scope.endStepArray,
                    precision: 2,
                    minLimit: 0,
                    maxLimit: 100,
                    hideLimitLabels: true,
                    showSelectionBar: true,
                    onEnd: function (sliderId, modelValue) {
                        var time = $filter('convertTime')(modelValue);

                        $scope.game_filter.end_time = time;
                        $scope.getChatRooms();
                    }
                }
            };
        }

        $scope.ageSlider = {
            minValue: $scope.game_filter.age.min,
            maxValue: $scope.game_filter.age.max,
            options: {
                hideLimitLabels: true,
                showSelectionBar: true,
                floor: 10,
                ceil: 70,
                step: 1,
                onEnd: function (sliderId, minValue, maxValue) {
                    $scope.game_filter.age.min = minValue;
                    $scope.game_filter.age.max = maxValue;
                    $scope.getChatRooms();
                }
            }
        }
        $scope.today = function () {
            $scope.game_filter.dt = new Date();
            setSliders($scope.game_filter.dt);
        };

        $scope.clear = function () {
            $scope.game_filter.dt = null;
        };
        $scope.clearFil = function () {
            $scope.game_filter = {
                sport: {
                    1: false,
                    2: false,
                    3: false,
                    4: false,
                    5: false,
                    6: false,
                    7: false,
                    8: false,
                    9: false,
                    10: false,
                    11: false,
                    12: false,
                    13: false,
                    14: false,
                    15: false,
                    16: false
                },
                access: 'All',
                start_time: 0,
                end_time: 23.45,
                age: {
                    min: 20,
                    max: 60
                },
                gender: 'All',
                sort: 'Soonest',
                address: '',
                distance: '',
                latitude: '',
                longitude: '',
                limit_start: 0,
                limit_end: 3,
                type: 'Gym'
            };
            setSliders()
            $scope.getChatRooms()
            $scope.game_filter.dt = null;
        };
        setSliders()

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
        function disabled(data) {
            var date = data.date,
                today = new Date();
            now = new Date(today.getFullYear(),today.getMonth(),today.getDate());
            var status = true;
            if(date >= now)
                status = false;
            return status;
        }

        $scope.dateOptions = {
            formatYear: 'yyyy',
            min: new Date(2000, 5, 22),
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(2000, 5, 22),
            startingDay: 1,
            showWeeks: false
        };

        $scope.toggleMin = function () {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.game_filter.dt = new Date(year, month, day);
            setSliders(new Date(year, month, day));
            $scope.getChatRooms();
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }
            // Chat Rooms:
        $scope.rooms = [];

        $scope.getChatRooms = function() {
          console.log($scope.game_filter);
            MessagesFactory.getChatRooms($scope.game_filter)
                .then(
                    function(response) {
                        if (response.data.status) {
                            $scope.unsorted_rooms = response.data.rows
                            $scope.unsorted_rooms.forEach(function(room) {
                                if (room.club_owner != null) {
                                    room.club_owner = JSON.parse(room.club_owner);
                                }
                                if (room.members_avatar != null) {
                                    room.members_avatar = JSON.parse(room.members_avatar);
                                }
                            })
                            $scope.rooms = $scope.unsorted_rooms;
                            $scope.rooms = $filter('orderBy')($scope.rooms,'-ord')
                            console.log(  $scope.rooms);
                        } else {
                            /*$scope.dialog = ngDialog.open({
                             template: response.data.message,
                             plain: true,
                             className: 'ngdialog-theme-default'
                         });*/
                            console.log('db error');
                        }
                    },
                    function(error) {
                        console.log('db error');
                    }
                );
        }
        $scope.acceptChatRoom = function(room) {
          if (room.gmid == -1) {
            MessagesFactory.acceptChatRoomM(room.id)
            .then(
              function(response) {
                if (response.data.status) {
                  console.log('OK');
                  $scope.rooms[findWithAttr($scope.rooms, 'id', room.id)].chat_room_status = 1
                  $scope.unsorted_rooms[findWithAttr($scope.unsorted_rooms, 'id', room.id)].chat_room_status = 1
                  $scope.filterChange()
                } else {
                  $scope.dialog = ngDialog.open({
                    template: response.data.message,
                    plain: true,
                    className: 'ngdialog-theme-default',
                  });
                }
              },
              function(error) {
                console.log(error);
                $scope.dialog = ngDialog.open({
                  template: error.data,
                  plain: true,
                  className: 'ngdialog-theme-default',
                });
              });
          }else{
            MessagesFactory.acceptChatRoom(room.gmid)
            .then(
              function(response) {
                if (response.data.status) {
                  console.log('OK');
                  $scope.rooms[findWithAttr($scope.rooms, 'gmid', room.gmid)].chat_room_status = 1
                  $scope.unsorted_rooms[findWithAttr($scope.unsorted_rooms, 'gmid', room.gmid)].chat_room_status = 1
                  $scope.filterChange()
                } else {
                  $scope.dialog = ngDialog.open({
                    template: response.data.message,
                    plain: true,
                    className: 'ngdialog-theme-default',
                  });
                }
              },
              function(error) {
                console.log(error);
                $scope.dialog = ngDialog.open({
                  template: error.data,
                  plain: true,
                  className: 'ngdialog-theme-default',
                });
              });
          }
        }
        $scope.leaveChatRoom = function(room) {
          if (room.gmid == -1) {
            MessagesFactory.leaveChatRoomM(room.id)
                .then(
                    function(response) {
                        if (response.data.status) {
                            console.log('OK');
                            $scope.rooms[findWithAttr($scope.rooms, 'id', room.id)].chat_room_status = 0
                            $scope.unsorted_rooms[findWithAttr($scope.unsorted_rooms, 'id', room.id)].chat_room_status = 0
                            $scope.filterChange()
                        } else {
                            $scope.dialog = ngDialog.open({
                                template: response.data.message,
                                plain: true,
                                className: 'ngdialog-theme-default',
                            });
                        }
                    },
                    function(error) {
                        console.log(error);
                        $scope.dialog = ngDialog.open({
                            template: error.data,
                            plain: true,
                            className: 'ngdialog-theme-default',
                        });
                    });
          }else{
            MessagesFactory.leaveChatRoom(room.gmid)
                .then(
                    function(response) {
                        if (response.data.status) {
                            console.log('OK');
                            $scope.rooms[findWithAttr($scope.rooms, 'gmid', room.gmid)].chat_room_status = 0
                            $scope.unsorted_rooms[findWithAttr($scope.unsorted_rooms, 'gmid', room.gmid)].chat_room_status = 0
                            $scope.filterChange()
                        } else {
                            $scope.dialog = ngDialog.open({
                                template: response.data.message,
                                plain: true,
                                className: 'ngdialog-theme-default',
                            });
                        }
                    },
                    function(error) {
                        console.log(error);
                        $scope.dialog = ngDialog.open({
                            template: error.data,
                            plain: true,
                            className: 'ngdialog-theme-default',
                        });
                    });
                  }
        }
        $scope.filterChange = function() {
            switch ($scope.filter) {
                case 'all':
                    {
                        $scope.rooms = $scope.unsorted_rooms
                        break;
                    }
                case 'lobby':
                    {
                        $scope.rooms = $filter('filter')($scope.unsorted_rooms, {
                            chat_room_status: '1'
                        })
                        break;
                    }
                case 'invites':
                    {
                        $scope.rooms = $filter('filter')($scope.unsorted_rooms, {
                            chat_room_status: '0'
                        })
                        break;
                    }
            }
        }
        $rootScope.$on('recive-message', function(event, data) {
          console.log(data.date);
          console.log($scope.rooms);
            if ($state.current.name == 'messages') {
                var index = findWithAttr($scope.rooms, 'id', data.chat_id)
                $scope.rooms[index].ord = data.date//new Date(data.date);
            }
            $scope.rooms = $filter('orderBy')($scope.rooms,'-ord')
        })
        $scope.getChatRooms()
        $rootScope.$on('update-unreaded', function() {
            $scope.$apply()
        })

        $scope.game_id_invite = '';

        $scope.rightMessageInvite = '';


        $scope.toggleMInvite = function($event, className, id) {
            $scope.inviteBtn = [];
            //$scope.inviteBtn[id] = true;
            $scope.adding_invaite = {};
            $scope.game_invites = {};
            $scope.game_id_invite = id;
            $scope.game_invites[id] = [];
            if(angular.element($event.toElement).hasClass('active_invite')){
                angular.element($event.toElement).removeClass('active_invite');
                $scope.rightMessageInvite = '';
            }else{
                angular.element('.inviteBtn').removeClass('active_invite');
                angular.element($event.toElement).addClass('active_invite');
            }


            if(!angular.element(".messages-box").hasClass('toggledM')) {
                className = className || 'toggledM';
                if ($scope.rightMessageInvite == undefined || $scope.rightMessageInvite == '') {
                    $scope.rightMessageInvite = className;
                } else {
                    $scope.rightMessageInvite = '';
                }
            }
        };

        $scope.set_invite = function($event, id, img){
            angular.element($event.currentTarget).attr("data-ng-click", "");
            if(img == ''){
                img = '/img/no_avatar_vopros.jpg';
            }
            $scope.adding_invaite[id] = true;

            GamesFactory.msgInviteUser({game_id: $scope.game_id_invite, user_id: id}).then(
                function(response){
                    console.log(response);
                },
                function(error){
                    console.log(error)
                }
            )
        };

        $scope.remove_invite = function(id){
            $scope.game_invites[$scope.game_id_invite].forEach(function (el, i, arr) {
                if(el.id == id){
                    // $scope.adding_invaite[id] = false;
                    delete($scope.adding_invaite[id]);
                    $scope.game_invites[$scope.game_id_invite].splice(i, 1);
                    //GamesFactory.addGameInvites($scope.game.game_invites);
                }
            })
        }

        TeammatesFactory.getTeammates('').then(
            function(response){
                console.log('getTeammates', response);
                $scope.teammates_arr = response.data;
                $scope.$broadcast('rebuild:scroll');
            }
        );

        $scope.searchTeammate = function (search_name) {
            TeammatesFactory.getTeammates(search_name).then(
                function(response){
                    $scope.teammates_arr = response.data;
                    $scope.$broadcast('rebuild:scroll');
                }
            )
        };
    });
