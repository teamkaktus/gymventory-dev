controllers
    .controller('forgotCtrl', function($scope, $state, AuthFactory, ngDialog) {
        $scope.data = {
            email : '',
            error: '',
            success:false
        }
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        $scope.send = function (form) {
            if(form.$valid){
                AuthFactory.forgotpsw($scope.data.email).then(
                    function (response) {
                        if (response.data.status) {
                            $scope.data.success = true;
                            ngDialog.closeAll();
                        }else {
                            $scope.data.error = response.data.message;
                            ngDialog.closeAll();
                        }
                    },
                    function (error) {
                        $scope.data.error = 'Db Error, Try later';
                        ngDialog.closeAll();
                    });
            }else{
                angular.forEach(form.$error, function (field) {
                    ngDialog.closeAll();
                    angular.forEach(field, function(errorField){
                        errorField.$setTouched();
                    })
                });
            }
        }
    })