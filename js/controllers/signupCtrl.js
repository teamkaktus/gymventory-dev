controllers

    .controller('signupCtrl', function ($scope, $state,ConfigApi, AuthFactory, ngDialog) {
        $scope.loading_spiner = false;

        $scope.daysInMonth = function (month, year) {
            return new Date(year, month, 0).getDate();
        };

        $scope.heightData = {
            ft: "1'",
            in: '5"'
        };

        $scope.userData = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: 'img/avatar_icon.png',
            tel: '',
            telCode: '+1',
            gender: 'Female',
            height: $scope.heightData.ft + $scope.heightData.in,
            birth: ''
        };
        $scope.dob = {
            day: [],
            dayS: 1,
            month: [],
            monthS: 1,
            year: [],
            yearS: 1990
        };
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        $scope.error = {
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            img: '',
            gender: '',
            height: '',
            birth: ''
        };
        AuthFactory.telephone()
            .then(
                function (response) {
                    $scope.telephone = response.data;
                },
                function (error) {
                    console.log('error');
                }
            );
        $scope.dateBuild = function () {
            $scope.dob.day = []
            for (var i = 1; i <= $scope.daysInMonth($scope.dob.monthS, $scope.dob.yearS); i++) {
                $scope.dob.day.push(i);
            }
            if (!$scope.dob.month.length) {
                for (var i = 1; i <= 12; i++) {
                    $scope.dob.month.push(i);
                }
            }
            if (!$scope.dob.year.length) {
                for (var i = 1950; i <= new Date().getFullYear(); i++) {
                    $scope.dob.year.push(i);
                }
            }
            $scope.userData.dob = $scope.dob.monthS + '-' + $scope.dob.dayS + '-' + $scope.dob.yearS
        };
        $scope.dateBuild();
        $scope.checkForm = function (form) {
            if (form.$invalid) {
                angular.forEach(form.$error, function (field) {
                    angular.forEach(field, function (errorField) {
                        errorField.$setTouched();
                    })
                });
                return false;
            } else {
                return true;
            }
        };
        $scope.signUp = function (form) {

            if ($scope.checkForm(form)) {
                $scope.loading_spiner = true;
                console.log($scope.userData);
                AuthFactory.signUp($scope.userData).then(
                    function (response) {
                        if (response.data.status) {
                            AuthFactory.authenticate($scope.userData.email, $scope.userData.password).then(
                                function (response) {
                                    if (response.data.status) {
                                        AuthFactory.setToken(response.data.token);
                                        ngDialog.closeAll();
                                        $state.go('app.home', {}, {reload: true});
                                    } else {
                                        ngDialog.closeAll();
                                        alert('Some error');
                                    }
                                },
                                function (error) {
                                    ngDialog.closeAll();
                                    console.log('error');
                                });
                        } else {
                            if (typeof response.data.errors != 'undefined') {
                                console.log("here");
                                ngDialog.closeAll();
                                angular.forEach(response.data.errors, function (error) {
                                    $scope.error[error.field] = error.message;
                                })
                            }
                        }
                    },
                    function (error) {
                        ngDialog.closeAll();
                        alert('Db Error, Try later')
                    }
                );
            }
        };
        $scope.file_uploaded = function ($file, $message, $flow) {
            $scope.userData.img = JSON.parse($message)['file'].path;
        };
    });
