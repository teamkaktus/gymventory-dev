controllers
    .controller('paymentReserveCtrl', function ($scope, GamesFactory, $timeout, CreditsFactory, $state, $stateParams, ngDialog) {

        $scope.loading_spiner = false;
        $scope.success_text = false;
        var timer = null;

        $scope.paymentGame = function () {
            var data = {};
            data.game_id = $scope.game_id;
            data.club_user = $scope.club_user;
            data.price = $scope.game_price;

            $timeout.cancel(timer);
            $scope.success_text = false;
            $scope.loading_spiner = true;

            GamesFactory.payReserve(data).then(
                function (response) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;

                    if(response.data.status) {
                        $scope.inform_text = 'Game successfully created!';

                    }else{
                        $scope.inform_text = 'When creating a game fails, try again!';
                    }
                    timer = $timeout(function () {
                        $scope.success_text = false;
                        ngDialog.close();
                        $state.reload();
                    }, 1000);
                }
            )
        }
        $scope.closePayment = function () {
            ngDialog.closeAll();
        }
    });
