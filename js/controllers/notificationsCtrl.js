controllers.controller('notificationsCtrl', function($scope, $rootScope, $state, notificationFactory, ngDialog, toaster) {
    $scope.notifications = [];
    notificationFactory.getAll().then(
        response => {
            console.log(response);
            $scope.notifications = response.data;
        }
    )
    
    $scope.viewGame = function(id, game_id, obj) {
        notificationFactory.view({id: id}).then(
            function( response ){
                if(response.data.changedRows > 0){
                    console.log('$rootScope.unreadedN ', $rootScope.unreadedN);
                    $rootScope.$emit('not-update', -1);
                }
            }
        );
        obj.status = 1;
        $scope.game_id = game_id;
        var dialog = ngDialog.open({
            templateUrl: './templates/notification/start_gameTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'show_gameCtrl',
            scope: $scope
        });
    }
    $scope.viewText = function(id, game_id, obj) {
        notificationFactory.view({id: id}).then(
            function( response ){
                if(response.data.changedRows > 0){
                    console.log('$rootScope.unreadedN ', $rootScope.unreadedN);
                    $rootScope.$emit('not-update', -1);
                }
            }
        );
        obj.status = 1;
        $scope.game_id = game_id;
        $scope.notification_id = id;
        var dialog = ngDialog.open({
            templateUrl: './templates/notification/show_textTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'show_textCtrl',
            scope: $scope
        });
    }

    $rootScope.$on('openBad', (event, data) => {
        $scope.game_id = data.game_id;
        ngDialog.open({
            templateUrl: './templates/notification/badTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'badCtrl',
            scope: $scope
        });
    });

    $rootScope.$on('openSweet', (event, data)=>{
        $scope.game_id = data.game_id;
        ngDialog.open({
            templateUrl: './templates/notification/sweetTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'sweetCtrl',
            scope: $scope
        });
    });

    $scope.feedback = function(id, game_id, obj) {
        notificationFactory.view({id: id}).then(
            function( response ){
                if(response.data.changedRows > 0){
                    console.log('$rootScope.unreadedN ', $rootScope.unreadedN);
                    $rootScope.$emit('not-update', -1);
                }
            }
        );
        $scope.game_id = game_id;
        obj.status = 1;
        $scope.dialog1 = ngDialog.open({
            templateUrl: './templates/notification/feedbackTpl.html',
            className: 'ngdialog-theme-default',
            controller: 'feedbackCtrl',
            scope: $scope
        });
    };

    $rootScope.$on('addFeedback', function(){
        $state.reload();
    });

    $rootScope.$on('new_notification', function(event, data){
        notificationFactory.getAll().then(
            response => {
                $scope.notifications = response.data;
                $rootScope.$emit('not-update', 1);
            }
        )
    });
});
