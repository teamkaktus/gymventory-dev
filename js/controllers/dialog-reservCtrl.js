controllers.controller('dialog-reservationsCtrl', function ($scope, $rootScope, $timeout, GamesFactory, $state, $stateParams, ngDialog) {
    $scope.loading_spiner = false;
    $scope.success_text = false;
    var timer = null;
    $scope.reserveGame = function () {
        var data = {};

        $timeout.cancel(timer);
        $scope.success_text = false;
        $scope.loading_spiner = true;

        if ($scope.createGame) {
            
            data.invites = GamesFactory.getGameInvites();
            data.game = GamesFactory.getGameParams();
            data.members_price = GamesFactory.getGameMemberPrice();
            data.gym_id = $stateParams.id_gym;
            data.club_user = $scope.gym.club_user;

            GamesFactory.addReserveGame(data).then(
                function (response) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;

                    $scope.inform_text = 'Game successfully reserved!';
                    timer = $timeout(function () {
                        $scope.success_text = false;
                        ngDialog.close();
                        GamesFactory.removeGameInvites();
                        GamesFactory.removeGameMemberPrice();
                        GamesFactory.removeGameParams();
                        $state.go('app.home');
                    }, 5000);
                },
                function (error) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'When reserve a game fails, try again!';
                }
            )
        }

        if (!$scope.createGame) {

            data.invites = GamesFactory.getGameInvites();
            data.members_price = GamesFactory.getGameMemberPrice();
            data.game_id = $stateParams.id_game;
            data.club_user = $scope.game.club_user;
            data.gym_id = $scope.game.gym_id;

            GamesFactory.joinReserveGame(data).then(
                function (response) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;

                    $scope.inform_text = 'Game successfully reserved!';
                    timer = $timeout(function () {
                        $scope.success_text = false;
                        ngDialog.close();
                        GamesFactory.removeGameInvites();
                        GamesFactory.removeGameMemberPrice();
                        GamesFactory.removeGameParams();
                        $state.go('app.home');
                    }, 5000);
                },
                function (error) {
                    $scope.success_text = true;
                    $scope.loading_spiner = false;
                    $scope.inform_text = 'When reserve a game fails, try again!';
                }
            )
        }
    };

    $scope.closePayment = function () {
        ngDialog.closeAll();
    }
});
