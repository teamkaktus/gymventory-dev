var directives = angular.module('app.directives', []);
var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};

directives
    .directive("compareTo", compareTo)
    .directive("repeatEnd", function() {
      return function(scope, element, attrs) {
        if (scope.$last){
          scope.$eval(attrs.repeatEnd);
        }
      };
    })
    .directive('upwardsScoll', function($timeout) {
        return {
            link: function(scope, elem, attr, ctrl) {
                var raw = elem[0];

                elem.bind('scroll', function() {
                    if (raw.scrollTop <= 0) {
                        var sh = raw.scrollHeight;
                        scope.$apply(attr.upwardsScoll);

                        $timeout(function() {
                            elem.animate({
                                scrollTop: raw.scrollHeight - sh
                            }, 500);
                        }, 0);
                    }
                });

                //scroll to bottom
                $timeout(function() {
                    scope.$apply(function() {
                        console.log(elem);
                        elem.scrollTop(raw.scrollHeight);
                    });
                }, 0);
            }
        }
    })
    .directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.myEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });