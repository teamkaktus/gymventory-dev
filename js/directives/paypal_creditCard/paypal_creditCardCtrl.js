(function(angular) {
    'use strict';
    angular.module('paypal.payment', [])
        .controller('paypalCtrl', ['$scope', '$timeout', '$http', 'ConfigApi', 'AuthFactory' , '$attrs', function($scope, $timeout, $http, ConfigApi, AuthFactory, $attrs) {
            console.log('$', $attrs.price);
           
            $scope.cc = {
                type : 'visa',
                number: '4032035691990857',
                expire_month_year: '09/2021',
                cvv2: '123',
                first_name: 'Joe',
                last_name: 'Shopper'
            };

            $scope.test = function () {
                console.log('change');
            }
            $scope.loading_spiner = false;
            $scope.success_text = false;
            var timer;
            $scope.make_payment = function(){
                var urlBase = ConfigApi.serverAddress + '/';
                $scope.success_text = false;
                $scope.loading_spiner = true;

                $http.post(urlBase + 'buyCredits', $scope.cc, {headers: {'X-Access-Token': AuthFactory.getToken()}}).then(
                    function(response){
                        console.log(response);

                        $scope.success_text = true;
                        $scope.loading_spiner = false;
                        $scope.inform_text = 'You profile saved!';
                        timer = $timeout(function () {
                            $scope.success_text = false;
                        }, 5000);
                    },
                    function (error) {
                        $scope.success_text = true;
                        $scope.inform_text = 'There was an error when saving!';
                        $scope.loading_spiner = false;
                        timer = $timeout(function () {
                            $scope.success_text = false;
                        }, 5000);
                    }
                );


                /*$scope.success_text = true;
                $scope.loading_spiner = false;
                $scope.inform_text = 'You profile saved!';
                timer = $timeout(function () {
                    $scope.success_text = false;
                    //delete(timer);
                }, 5000);*/
            }
        }])
        .directive('paypalPayment', function() {
            return {
                controller: 'paypalCtrl',
                restrict: 'E',
                scope: {
                    customerInfo: '=info'
                },
                templateUrl: 'js/directives/paypal_creditCard/paypal_creditCardTpl.html'
        };
        });
})(window.angular);


