angular.module('app.routes', [])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
        /*$ocLazyLoadProvider.config({
            'debug': true, // For debugging 'true/false'
            'events': true, // For Event 'true/false'
            'modules': [{ // Set modules initially
                name : 'oc-bookingCtrl', // State1 module
                files: ['js/controllers/bookingCtrl.js']
            }]
        });*/

        $stateProvider
            .state('app', {
                templateUrl: 'templates/left-sidebarTpl.html',
                controller: 'asideCtrl',
                abstract:true
            })
            .state('app.home', {
                url: '/',
                views: {
                  'app': {
                    templateUrl: 'templates/lobbyTpl.html',
                    controller: 'lobbyCtrl'
                  }
                }
            })
            .state('app.notifications', {
                url: '/notifications',
                views: {
                    'app': {
                        templateUrl: 'templates/notificationsTpl.html',
                        controller: 'notificationsCtrl'
                    }
                }
            })
            .state('app.b_notifications', {
                url: '/business-notifications',
                views: {
                    'app': {
                        templateUrl: 'templates/notification/business_notificationTpl.html',
                        controller: 'b_notificationsCtrl'
                    }
                }
            })
            .state('app.paypal', {
                url: '/paypal',
                views: {
                    'app': {
                        templateUrl: '/templates/paypal_callbackTpl.html',
                        controller: 'paypal_callbackCtrl'
                    }
                }
            })
            .state('app.profile', {
                url: '/profile',
                views: {
                    'app': {
                        templateUrl: '/templates/edit-profileTpl.html',
                        controller: 'edit-profileCtrl'
                    }
                }
            })
            .state('app.withdrawal', {
                url: '/withdrawal',
                views: {
                    'app': {
                        templateUrl: '/templates/withdrawalTpl.html',
                        controller: 'withdrawalCtrl'
                    }
                }
            })
            .state('app.transactions', {
                url: '/transactions',
                views: {
                    'app': {
                        templateUrl: '/templates/transactionsTpl.html',
                        controller: 'transactionsCtrl'
                    }
                }
            })
            .state('app.manage_clubs', {
                url: '/manage_clubs',
                views: {
                    'app': {
                        templateUrl: '/templates/manage-clubs-gymsTpl.html',
                        controller: 'manage-clubsCtrl'
                    }
                }
            })
            .state('app.search', {
                url: '/search',
                views: {
                    'app': {
                        templateUrl: 'templates/searchClubsTpl.html',
                        controller: 'searchClubsCtrl'
                    }
                }
            })
            .state('app.last-visited', {
                url: '/last-visited',
                views: {
                    'app': {
                        templateUrl: 'templates/last-visitedClubsTpl.html',
                        controller: 'last-visitedClubsCtrl'
                    }
                }
            })
            .state('app.create-match', {
                url: '/create-match',
                views: {
                    'app': {
                        templateUrl: 'templates/create-matchTpl.html',
                        controller: 'create-matchCtrl'
                    }
                }
            })

            .state('app.messages', {
                url: '/massages',
                views: {
                    'app': {
                        templateUrl: 'templates/messagesTpl.html',
                        controller: 'messagesCtrl'
                    }
                }
            })
            .state('app.chat', {
                url: '/massages/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/chatTpl.html',
                        controller: 'chatCtrl'
                    }
                }
            })

            .state('app.create-class', {
                url: '/create-class',
                views: {
                    'app': {
                        templateUrl: 'templates/create-classTpl.html',
                        controller: 'create-classCtrl'
                    }
                }
            })

            .state('app.join-game', {
                url: '/join-game/:id_game/',
                views: {
                    'app': {
                        templateUrl: 'templates/join-gameTpl.html',
                        controller: 'join-gameCtrl'
                    }
                }
            })
            .state('app.reserve-game', {
                url: '/reserve-game/:id_game/',
                views: {
                    'app': {
                        templateUrl: 'templates/reserve-gameTpl.html',
                        controller: 'reserve-gameCtrl'
                    }
                }
            })
            .state('app.create-game', {
                url: '/create-game/:id_gym/',
                params:{
                    club_params: {
                    }
                },
                views: {
                    'app': {
                        templateUrl: 'templates/create-gameTpl.html',
                        controller: 'create-gameCtrl'
                    }
                }
            })
            .state('app.teammates', {
                url: '/teammates',
                views: {
                    'app': {
                        templateUrl: 'templates/teammatesTpl.html',
                        controller: 'teammatesCtrl'
                    }
                }
            })
            .state('app.club-page', {
                url: '/club-page/:club_id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-pageTpl.html',
                        controller: 'club-pageCtrl'
                    }
                }
            })
            .state('app.teammate-page', {
                url: '/teammate-page/:t_id',
                views: {
                    'app': {
                        templateUrl: 'templates/teammate-pageTpl.html',
                        controller: 'teammate-pageCtrl'
                    }
                }
            })
            .state('app.user', {
                url: '/user/:t_id',
                views: {
                    'app': {
                        templateUrl: 'templates/profile-pageTpl.html',
                        controller: 'profile-pageCtrl'
                    }
                }
            })
            .state('app.members', {
                url: '/members',
                views: {
                    'app': {
                        templateUrl: 'templates/membersTpl.html',
                        controller: 'membersCtrl'
                    }
                }
            })
            .state('app.booking', {
                url: '/booking',
                views: {
                    'app': {
                        templateUrl: 'templates/bookingTpl.html',
                        controller: 'bookingCtrl'
                    }
                }
            })
            .state('app.clubRequest', {
                url: '/club-request',
                views: {
                    'app': {
                        templateUrl: 'templates/notification/business_club_requestTpl.html',
                        controller: 'b_club_requestCtrl'
                    }
                }
            })
            .state('app.createMyClub', {
                url: '/create-my-club',
                views: {
                    'app': {
                        templateUrl: 'templates/find_to_create_clubTpl.html',
                        controller: 'findToCreateClubCtrl'
                    }
                }
            })
            .state('app.clubCourts', {
                url: '/club-courts',
                views: {
                    'app': {
                        templateUrl: 'templates/club-courtsTpl.html',
                        controller: 'club-courtsCtrl'
                    }
                }
            })
            .state('app.courtsItem', {
                url: '/club-courts/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-courts_itemTpl.html',
                        controller: 'club-courts_itemCtrl'
                    }
                }
            })
            .state('app.clubClasses', {
                url: '/club-classes',
                views: {
                    'app': {
                        templateUrl: 'templates/club-classesTpl.html',
                        controller: 'club-classesCtrl'
                    }
                }
            })
            .state('app.classesItem', {
                url: '/club-classes/:id',
                views: {
                    'app': {
                        templateUrl: 'templates/club-classes_itemTpl.html',
                        controller: 'club-classes_itemCtrl'
                    }
                }
            })
            .state('app.socialConnect', {
                url: '/social-connect',
                views: {
                    'app': {
                        templateUrl: 'templates/social-connect.html',
                        controller: 'socialConnectCtrl'
                    }
                }
            })
            .state('signin', {
                url: '/signin',
                templateUrl: '/templates/signinTpl.html',
                controller: 'signinCtrl'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $urlRouterProvider.otherwise('/');
    }]);
