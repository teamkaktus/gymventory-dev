services

    .factory('AuthFactory', ['$http', '$state', 'ConfigApi', 'Socket', 'jwtHelper', function ($http, $state, ConfigApi, Socket, jwtHelper) {
        var urlBase = ConfigApi.serverAddress + '/';
        var AuthFactory = {};

        AuthFactory.authenticate = function (email, password) {
            return $http.post(urlBase + 'authenticate', {email: email, password: password});
        };
        AuthFactory.authenticateSocial = function (profile) {
            return $http.post(urlBase + 'authenticateSocial', {profile: profile});
        };
        AuthFactory.changeSocial = function (social_id,email) {
            return $http.post(urlBase + 'changeSocial', {social_id:social_id,email:email}, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        AuthFactory.uploadImg = function (file) {
            return $http.post(urlBase + 'imageupload', file);
        };
        AuthFactory.signUp = function (user) {
            return $http.post(urlBase + 'signUp', {user: user});
        };
        AuthFactory.forgotpsw = function (email) {
            return $http.post(urlBase + 'forgotpsw', {email: email});
        };
        AuthFactory.telephone = function (user) {
            return $http.get(ConfigApi.serverStatic + 'data/CountryCodes.json');
        };

        AuthFactory.authenticated = function () {
            return (window.localStorage.getItem('token') != undefined) ? true : false;
        };

        AuthFactory.setToken = function (token) {
            window.localStorage.setItem('token', token);
            Socket.login(jwtHelper.decodeToken(token).id)
        };
        AuthFactory.removeToken = function () {
          Socket.logout(jwtHelper.decodeToken(window.localStorage.getItem('token')).id)
            localStorage.removeItem('token');
        };
        AuthFactory.getToken = function () {
            return window.localStorage.getItem('token');
        };

        return AuthFactory;
    }]);
