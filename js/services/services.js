var services = angular.module('app.services', [])

    .factory('userProfile', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var userProfile = {};
        userProfile.get = function () {
            return $http.get(urlBase + 'getProfile', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        userProfile.getUserData = function () {
            return $http.get(urlBase + 'getUserData', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        userProfile.update = function (data) {
            return $http.post(urlBase + 'updateProfile', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        userProfile.getUserTransactions = function () {
            return $http.get(urlBase + 'user-transactions', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return userProfile;
    }])

    .factory('configFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var configFactory = {};
        configFactory.get = function (data) {
            return $http.get(urlBase + 'getConfig?code='+data.code+'&key='+data.key, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return configFactory;
    }])

    .factory('clubFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var clubFactory = {};

        clubFactory.uniqueClubs = function (name) {
            return $http.get(urlBase + 'uniqueNameClub/'+name, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.createClub = function (data) {
            return $http.post(urlBase + 'createClub', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        clubFactory.requestOwnership = function (data) {
            return $http.post(urlBase + 'requestOwnership', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getAddressCoordinate = function(address){
            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + ConfigApi.googleKey);
        };

        clubFactory.checkClub = function () {
            return $http.get(urlBase + 'checkClubUser', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.countClub = function () {
            return $http.get(urlBase + 'countClubUser', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getClubData = function () {
            return $http.get(urlBase + 'clubData', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        clubFactory.getTypeHours = function () {
            return $http.get(urlBase + 'clubTypeHours', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        clubFactory.updateClubData = function (data) {
            return $http.post(urlBase + 'updateClubData', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.addMember = function (data) {
            return $http.post(urlBase + 'addClubMember', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.deleteMember = function (data) {
            return $http.post(urlBase + 'deleteClubMember', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getClub = function (id) {
            return $http.get(urlBase + 'club/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getLastClubs = function () {
            return $http.get(urlBase + 'clublast-visited', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getClubGames = function (data) {
            return $http.post(urlBase + 'clubGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getAllGames = function(data){
            return $http.post(urlBase + 'clubAllGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.filterClubNA = function(data){
            return $http.post(urlBase + 'clubFilterNA', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.getFavorite = function(){
            return $http.get(urlBase + 'club-favorite',  {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.addFavorite = function(club_id){
            return $http.post(urlBase + 'club-favorite', {club_id:club_id} ,{headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.removeFavorite = function(club_id){
            return $http.delete(urlBase + 'club-favorite?club_id='+club_id,  {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        clubFactory.managerRequest = function(data){
            return $http.post(urlBase + 'manager-club', data ,{headers: {'X-Access-Token': AuthFactory.getToken()}});
        }
        clubFactory.getClubRequest = function(){
            return $http.get(urlBase + 'club-request', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        }
        clubFactory.setAcceptRequest = function(data){
            return $http.post(urlBase + 'accept-club-request', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        }
        clubFactory.setDeclineRequest = function(data){
            return $http.post(urlBase + 'decline-club-request', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        }

        return clubFactory;
    }])

    .factory('gymFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var gymFactory = {};
        gymFactory.createGym = function (data) {
            return $http.post(urlBase + 'createGym', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        gymFactory.getAllGyms = function (type) {
            return $http.get(urlBase + 'getAllGyms/'+type, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        gymFactory.getGym = function (id) {
            return $http.get(urlBase + 'getGym/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        gymFactory.updateGym = function (data) {
            return $http.post(urlBase + 'updateGymData', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        return gymFactory;
    }])

    .factory('myGeocode', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi) {
        var myGeocode = {};

        myGeocode.getAddressCoordinate = function(address){
            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + ConfigApi.googleKey);
        };

        myGeocode.coordinateToAddress = function(coords){
            return $http.get('//maps.googleapis.com/maps/api/geocode/json?latlng='+coords+'&key=' + ConfigApi.googleKey);
        };
        return myGeocode;
    }])

    .factory('FiltersFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var FiltersFactory = {};

        FiltersFactory.clubFilters = function(data){
            return $http.post(urlBase + 'clubFilter', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        FiltersFactory.clublast_visitedA = function(){
            return $http.post(urlBase + '/clublast-visitedA', {}, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        FiltersFactory.clubFavorite = function(){
            return $http.post(urlBase + '/clublast-visitedA', {}, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        FiltersFactory.getProfileAddress = function(){
            return $http.get(urlBase + 'getProfileAddress', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return FiltersFactory;
    }])

    .factory('GamesFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var GamesFactory = {};

        GamesFactory.urlBase = ConfigApi.serverStatic;

        GamesFactory.findGames = function(data){
            return $http.post(urlBase + 'findGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.findBookingGames = function(data){
            return $http.post(urlBase + 'bookingGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.findAfterGames = function(data){
            return $http.post(urlBase + 'findAfterGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.getGame = function(id){
            return $http.get(urlBase + 'game/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.getGames = function(data){
          var params = ''
          angular.forEach(data, function(value, key) {
            if(value != "")
            params += '&' + key + '=' + escape(value);
          });

          return $http.get(urlBase + 'games?token=' + AuthFactory.getToken() + params);
            // return $http.get(urlBase + 'games', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.getGamesReserved = function(){
            return $http.get(urlBase + 'getReserved', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.getGamesWatch = function(data){
            var params = ''
            angular.forEach(data, function(value, key) {
              if(value != "")
              params += '&' + key + '=' + escape(value);
            });

            return $http.get(urlBase + 'gamesWatch?token=' + AuthFactory.getToken() + params);
        };

        GamesFactory.getGamesInvites = function(){
            return $http.get(urlBase + 'gamesInvites', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.sendGameParams = function(params) {
            window.localStorage.setItem('gameParams', JSON.stringify(params));
        }

        GamesFactory.removeGameParams = function () {
            localStorage.removeItem('gameParams');
        };
        GamesFactory.getGameParams = function () {
            return JSON.parse(window.localStorage.getItem('gameParams'));
        };

        GamesFactory.getGym = function (id) {
            return $http.get(urlBase + 'gym/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        }

        GamesFactory.addGameInvites = function(invites) {
            window.localStorage.setItem('gameInvites', JSON.stringify(invites));
        }

        GamesFactory.getGameInvites = function () {
            return JSON.parse(window.localStorage.getItem('gameInvites'));
        };

        GamesFactory.removeGameInvites = function () {
            localStorage.removeItem('gameInvites');
        };

        GamesFactory.createGame = function (data) {
            return $http.post(urlBase + 'createGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.ManagerCreateGame = function (data) {
            return $http.post(urlBase + 'ManagerCreateGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.ManagerDeleteGame = function (id) {
            return $http.post(urlBase + 'ManagerDeleteGame', {game_id: id}, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.getManagerGames = function (court) {
            return $http.get(urlBase + 'getManagerGames/'+court, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.addReserveGame = function (data) {
            return $http.post(urlBase + 'addReserveGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        GamesFactory.joinReserveGame = function (data) {
            return $http.post(urlBase + 'joinReserveGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.joinGame = function (data) {
            return $http.post(urlBase + 'joinGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.payReserve = function (data) {
            return $http.post(urlBase + 'payReserve', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.addGameMemberPrice = function(member_price) {
            window.localStorage.setItem('gameMember_price', member_price);
        };

        GamesFactory.getGameMemberPrice = function () {
            return JSON.parse(window.localStorage.getItem('gameMember_price'));
        };

        GamesFactory.removeGameMemberPrice = function () {
            localStorage.removeItem('gameMember_price');
        };

        GamesFactory.setWatchGame = function (data) {
            return $http.post(urlBase + 'watchGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.msgInviteUser = function (data) {
            return $http.post(urlBase + 'msgInviteUser', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        }

        GamesFactory.getBookGames = function (id) {
            return $http.get(urlBase + 'game_forInvite/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        GamesFactory.mustStartGame = function (data) {
            return $http.put(urlBase + 'must-start-game', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return GamesFactory;
    }])

    .factory('ClassFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var ClassFactory = {};

        ClassFactory.urlBase = ConfigApi.serverStatic;

        ClassFactory.findGames = function(data){
            return $http.post(urlBase + 'findClass', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        return ClassFactory;
    }])

    .factory('CreditsFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var CreditsFactory = {};

        CreditsFactory.buyCredits = function(data){
            return $http.post(urlBase + 'buyCredits', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        CreditsFactory.getBalance = function(){
            return $http.get(urlBase + 'getBalance', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        CreditsFactory.getPayoutsHistory = function(){
            return $http.get(urlBase + 'payout_history', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        CreditsFactory.create_withdraw = function(data){
            return $http.post(urlBase + 'createPayout', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        CreditsFactory.paymentGame = function(data){
            return $http.post(urlBase + 'paymentGame', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        CreditsFactory.paymentClub = function(data){
            return $http.post(urlBase + 'paymentClub', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return CreditsFactory;
    }])
    .factory('TeammatesFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var TeammatesFactory = {};

        TeammatesFactory.getTeammates = function(name){
            return $http.get(urlBase + 'teammates/?name='+name, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        TeammatesFactory.getMyTeammates = function(data){
            return $http.post(urlBase + 'my_teammates', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        TeammatesFactory.getTeammate = function(data){
            return $http.post(urlBase + 'teammate', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        TeammatesFactory.getProfile = function(data){
            return $http.post(urlBase + 'profile', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        TeammatesFactory.getGames = function(data){
            return $http.post(urlBase + 'teammateGames', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };


        return TeammatesFactory;
    }])
    .factory('MembersFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var MembersFactory = {};

        MembersFactory.getMembers = function(data){
            return $http.post(urlBase + 'members', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };


        return MembersFactory;
    }])
    .factory('paypalFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var paypalFactory = {};

        paypalFactory.successPayment = function(data){
            return $http.post(urlBase + 'successPayment', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        paypalFactory.setCredits = function(credits) {
            window.localStorage.setItem('userCredits', credits);
        };

        paypalFactory.getCredits = function () {
            return JSON.parse(window.localStorage.getItem('userCredits'));
        };

        paypalFactory.removeCredits = function () {
            localStorage.removeItem('userCredits');
        };

        return paypalFactory;
    }])
    .factory('notificationFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var notificationFactory = {};

        notificationFactory.getAll = function(){
            return $http.get(urlBase + 'notifications', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.view = function(data){
            return $http.post(urlBase + 'viewNotifications', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.getQuetions = function(){
            return $http.get(urlBase + 'badQuetions', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.getMembersGame = function(id){
            return $http.get(urlBase + 'getMembersGame/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.setRating = function(data){
            return $http.post(urlBase + 'setUserRating', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.setBad = function(data){
            return $http.post(urlBase + 'setBad', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.sendAllMembers = function(data){
            return $http.post(urlBase + 'sendAllMembers', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.getDescription = function(id){
            return $http.get(urlBase + 'getNDescription/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        notificationFactory.getHistory = function(){
            return $http.get(urlBase + 'getHistory/', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return notificationFactory;
    }]);
