services.factory('Socket', function(ConfigApi, $rootScope, $filter, jwtHelper, toaster) {
    var Socket = {};
    socket = io(ConfigApi.serverStatic);
    Socket.login = function(user_id) {
        socket.emit('login', {
            user_id: user_id
        })
    };
    Socket.logout = function(user_id) {
        socket.emit('logout', {
            user_id: user_id
        })
    };
    Socket.sendMessage = function(data) {
        socket.emit('sendMessage', data)
    };
    Socket.typing = function(data) {
        socket.emit('typing', data)
    };
    Socket.setReaded = function(data) {
        socket.emit('set readed', data)
    };
    Socket.getUnreaded = function() {
        socket.emit('get unreaded')
    };
    socket.on('connect', function() {
        var token = window.localStorage.getItem('token');
        if (token != undefined) {
            Socket.login(jwtHelper.decodeToken(token).id)
        }
    });
    Socket.logged = false;
    socket.on('login ok', function() {
        Socket.logged = false;
    });
    socket.on('recive message', function(data) {
        $rootScope.$emit('recive-message', data);
        if (!$rootScope.unreaded[data.chat_id]) {
            $rootScope.unreaded[data.chat_id] = 0
        }
        $rootScope.unreaded[data.chat_id] = $rootScope.unreaded[data.chat_id] + 1;
        $rootScope.$emit('update-unreaded')
    });
    socket.on('typing-response', function(data) {
        $rootScope.$emit('typing-response', data)
    });
    socket.on('online', function(data) {
        $rootScope.users.online = data;
        $rootScope.$emit('online', data);
            // $rootScope.$emit('recive-message',data)
    });
    socket.on('unreaded count', function(data) {
        $rootScope.unreaded = data;
        $rootScope.$emit('update-unreaded')
    });
    socket.on('message delivered', function(data) {
        $rootScope.$emit('message-delivered', data)
    });
    socket.on('send-message-error', function(data) {
        $rootScope.$emit('send-message-error', data)
    });

    socket.on('notification', function(data) {
        $rootScope.$emit('new_notification', data)
        toaster.pop({type: 'note', title: "New notification", body: data.text});
    });

    socket.on('notificationUnreaded', function(data) {
        $rootScope.unreadedN = data.count;
        $rootScope.$emit('not-unreaded', data.count);
    });

    Socket.s = socket;
    return Socket;
});
