//Адрес до иконки с маркером
var marker_url = '../img/map-marker.png';


var main_color = '#ffff00';

var stylesArray = [{
    "featureType": "landscape",
    "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]
}, {
    "featureType": "poi",
    "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]
}, {
    "featureType": "road.highway",
    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
}, {
    "featureType": "road.arterial",
    "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]
}, {
    "featureType": "road.local",
    "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]
}, {
    "featureType": "transit",
    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
}, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {
    "featureType": "water",
    "elementType": "labels",
    "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]
}, {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{"hue": main_color}, {"lightness": -25}, {"saturation": -97}]
}];