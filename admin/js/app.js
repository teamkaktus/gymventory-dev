var app = angular.module('adminPanel', ['ui.router', 'admin.routes', 'admin.configs', 'google.places', 'ui.bootstrap', 'ui.mask', 'admin.services', 'ngDialog', 'admin.controllers', 'admin.directives', 'oitozero.ngSweetAlert'])

    .run(function ($rootScope, $state, AuthFactory, $timeout) {
        $state.go('app.home');
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (['login'].indexOf(toState.name) == -1) {
                    if (!AuthFactory.authenticated()) {
                        $timeout(function () {
                            $state.go('app.login');
                        });
                    }
                } else {
                    if (AuthFactory.authenticated()) {
                        $state.go('app.home');
                    }
                }
            });
    });

app.filter('filterClock', function () {
    return function (item) {
        var output = 0;
        if (item <= 12) {
            if (item < 10) {
                output = '0' + item + ' am';
            } else {
                output = item + ' am';
            }
        }
        if (item > 12 && item <= 24) {
            if ((item - 12) < 10) {
                output = '0' + (item - 12) + ' pm';
            } else {
                output = (item - 12) + ' pm';
            }
        }
        return output;
    };
});