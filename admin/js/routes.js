angular.module('admin.routes', [])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container@': {
                        templateUrl: 'templates/adminTpl.html',
                        controller: 'mainCtrl'
                    }
                }
            })
            .state('clubs', {
                url: '/clubs',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container': {
                        templateUrl: 'templates/clubsTpl.html',
                        controller: 'added_clubsCtrl'
                    }
                }
            })
            .state('list-clubs', {
                url: '/list-clubs',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container': {
                        templateUrl: 'templates/list-clubsTpl.html',
                        controller: 'listClubsCtrl'
                    }
                }
            })
            .state('add-club', {
                url: '/add-club',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container': {
                        templateUrl: 'templates/add-clubsTpl.html',
                        controller: 'addClubsCtrl'
                    }
                }
            })
            .state('registered-users', {
                url: '/registered-users',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container': {
                        templateUrl: 'templates/registered-usersTpl.html',
                        controller: 'registeredUsersCtrl'
                    }
                }
            })
            .state('clubTypes', {
                url: '/club-types',
                views: {
                    'sidebar': {
                        templateUrl: 'templates/sidebarTpl.html',
                        controller: 'sidebarCtrl'
                    },
                    'container': {
                        templateUrl: 'templates/club-typesTpl.html',
                        controller: 'clubTypesCtrl'
                    }
                }
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/loginTpl.html',
                controller: 'loginCtrl'
            });

        /*$locationProvider.html5Mode({
         enabled: true,
         requireBase: false
         });*/
        $urlRouterProvider.otherwise('/');
    }]);