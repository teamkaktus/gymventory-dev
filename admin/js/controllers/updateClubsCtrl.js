controllers
    .controller('updateClubsCtrl', function ($scope, $state, ClubsFactory, ngDialog, $timeout, SweetAlert) {
        $scope.o_hours = [];

        function get24clock() {
            var array = [];
            for (var i = 1; i <= 24; i++) {
                array.push(i);
            }
            return array;
        }

        $scope.o_hours = get24clock();

        ClubsFactory.getAdminClubs($scope.club_id).then(
            function (response) {
                console.log('response');
                if(!response.data){
                    $state.go('app.home');
                }
                $scope.gym = {
                    id: response.data.club_id
                };
                $scope.club = {
                    id: $scope.club_id,
                    name: response.data.name,
                    type: response.data.type,
                    address: response.data.address,
                    latitude: response.data.latitude,
                    longitude: response.data.longitude,
                    phone: response.data.phone,
                    website: response.data.website,
                    email: response.data.email,
                    fax: response.data.fax,
                    day: {
                        mon: {
                            start: 8,
                            end: 22
                        },
                        tue: {
                            start: 8,
                            end: 22
                        },
                        wed: {
                            start: 8,
                            end: 22
                        },
                        thu: {
                            start: 8,
                            end: 22
                        },
                        fri: {
                            start: 8,
                            end: 22
                        },
                        sat: {
                            start: 8,
                            end: 22
                        },
                        sun: {
                            start: 8,
                            end: 22
                        }
                    }
                };

                $scope.day = {mon: true, tue: true, wed: true, thu: true, fri: true, sat: false, sun: false};
                if (response.data.hours) {
                    $scope.day = {mon: false, tue: false, wed: false, thu: false, fri: false, sat: false, sun: false};
                    var work_hours = JSON.parse(response.data.hours);
                    for (var key in work_hours) {
                        $scope.day[key] = true;
                        $scope.club.day[key] = work_hours[key];
                    }
                }
            },
            function (error) {
                console.log('error');
                $scope.check_club = false;
                $scope.inform_text = true;
                console.log(error);
            }
        );


        $scope.check = true;
        $scope.chekSpiner = false;
        $scope.chekText = '';

        $scope.checkName = function (name) {
            var timer = {};
            $scope.chekSpiner = true;
            $scope.chekText = '';
            timer = $timeout(function () {
                ClubsFactory.uniqueClubs(name).then(
                    function (response) {
                        if (response.data.status == true && response.data.url == 'no-isset') {
                            $scope.chekText = 'Success!';
                            $scope.check = true;
                            $scope.slug = response.data.slug;
                        } else {
                            $scope.chekText = 'Name used!';
                            $scope.check = false;
                        }
                        $scope.chekSpiner = false;
                    },
                    function (error) {
                        $scope.chekSpiner = false;
                        $scope.check = false;
                        console.log(error);
                    }
                )
                delete(timer);
            }, 100);
        }

        $scope.updateClub = function (data) {
            if ($scope.club.address != null) {
                if ($scope.club.address.formatted_address) {
                    $scope.club.address = $scope.club.address.formatted_address;
                }
            }
            ClubsFactory.getAddressCoordinate($scope.club.address).then(
                function (response) {
                    var coords = response.data.results[0].geometry.location;

                    $scope.club.latitude = coords.lat;
                    $scope.club.longitude = coords.lng;

                    if (data.day) {
                        for (var key in $scope.day) {
                            if ($scope.day[key] == false) {
                                if (data.day[key])
                                    delete(data.day[key]);
                            }
                        }
                    }
                    if ($scope.check) {
                        data.slug = $scope.slug;
                        console.log(data);
                        ClubsFactory.updateAdminClub(data).then(
                            function (response) {
                                ngDialog.closeAll();
                                SweetAlert.swal({
                                    title: "Success",
                                    text: "Club updatet!",
                                    type: "success"}, function(){
                                    $state.reload();
                                });
                            },
                            function (error) {
                                ngDialog.closeAll();
                                SweetAlert.swal("Error!", "Club not update!", "error")
                            }
                        )
                    }
                },
                function (error) {
                }
            )
        }
    });