controllers
    .controller('addClubsCtrl', function ($scope, $state, ClubsFactory, $timeout, SweetAlert) {

        $scope.o_hours = [];

        function get24clock() {
            var array = [];
            for (var i = 1; i <= 24; i++) {
                array.push(i);
            }
            return array;
        }

        $scope.o_hours = get24clock();

        $scope.club = {
            name: "",
            type: "private",
            address: '',
            latitude: '',
            longitude: '',
            phone: '',
            website: '',
            email: '',
            fax: '',
            day: {
                mon: {
                    start: 8,
                    end: 22
                },
                tue: {
                    start: 8,
                    end: 22
                },
                wed: {
                    start: 8,
                    end: 22
                },
                thu: {
                    start: 8,
                    end: 22
                },
                fri: {
                    start: 8,
                    end: 22
                },
                sat: {
                    start: 8,
                    end: 22
                },
                sun: {
                    start: 8,
                    end: 22
                }
            }
        };

        $scope.day = {mon: true, tue: true, wed: true, thu: true, fri: true, sat: false, sun: false};


        $scope.check = false;
        $scope.chekSpiner = false;
        $scope.chekText = '';

        $scope.checkName = function (name) {
            var timer = {};
            $scope.chekSpiner = true;
            $scope.chekText = '';
            timer = $timeout(function () {
                ClubsFactory.uniqueClubs(name).then(
                    function (response) {
                        if (response.data.status == true && response.data.url == 'no-isset') {
                            $scope.chekText = 'Success!';
                            $scope.check = true;
                            $scope.slug = response.data.slug;
                        } else {
                            $scope.chekText = 'Name used!';
                            $scope.check = false;
                        }
                        $scope.chekSpiner = false;
                    },
                    function (error) {
                        $scope.chekSpiner = false;
                        $scope.check = false;
                        console.log(error);
                    }
                )
                delete(timer);
            }, 100);
        }

        $scope.addClub = function (data) {
            if ($scope.club.address != null) {
                if ($scope.club.address.formatted_address) {
                    $scope.club.address = $scope.club.address.formatted_address;
                }
            }
            ClubsFactory.getAddressCoordinate($scope.club.address).then(
                function (response) {
                    var coords = response.data.results[0].geometry.location;

                    $scope.club.latitude = coords.lat;
                    $scope.club.longitude = coords.lng;

                    if (data.day) {
                        for (var key in $scope.day) {
                            if ($scope.day[key] == false) {
                                if (data.day[key])
                                    delete(data.day[key]);
                            }
                        }
                    }
                    if ($scope.check) {
                        data.slug = $scope.slug;
                        console.log(data);
                        ClubsFactory.addAdminClub(data).then(
                            function (response) {
                                SweetAlert.swal({
                                    title: "Success",
                                    text: "Club added!",
                                    type: "success"}, function(){
                                    $state.reload();
                                });
                            },
                            function (error) {
                                SweetAlert.swal("Error!", "Club not added!", "error")
                            }
                        )
                    }
                },
                function (error) {
                }
            )
        }
    });