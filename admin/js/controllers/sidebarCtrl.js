controllers
    .controller('sidebarCtrl', function($scope, $rootScope, $state, AuthFactory) {
        $scope.signout = function (){
            AuthFactory.removeToken();
            $state.go('app.login');
        };
    });