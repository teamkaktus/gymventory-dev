controllers

    .controller('loginCtrl', function($scope, $rootScope, $state, $timeout, AuthFactory) {
        $scope.data = {
            email: '',
            password: ''
        }
        $scope.error = {
            email: '',
            password: '',
            db: ''
        }
        $scope.checkUsername = function(){
            if ($scope.data.email == '') {
                $scope.error.email = 'Email can\'t be blank'
                return false;
            }else {
                $scope.error.email = ''
                return true;
            }
        }
        $scope.checkPassword = function(){
            if ($scope.data.password == '') {
                $scope.error.password = 'Password can\'t be blank'
                return false;
            }else {
                $scope.error.password = '';
                return true;
            }
        }

        $scope.loading_spiner = false;
        $scope.success_text = false;
        var timer;

        $scope.login = function () {
            cu = $scope.checkUsername();
            cp = $scope.checkPassword();
            if(cu && cp){
                $scope.success_text = false;
                $scope.loading_spiner = true;
                AuthFactory.adminauth($scope.data.email, $scope.data.password)
                    .then(
                        function (response) {
                            if (response.data.status) {
                                AuthFactory.setToken(response.data.token);
                                $state.go('app.home', {}, {reload: true});

                            }else{
                                $scope.success_text = true;
                                $scope.inform_text = response.data.message;
                                $scope.loading_spiner = false;
                                timer = $timeout(function() {
                                    $scope.success_text = false;
                                    delete(timer);
                                }, 5000);
                            }
                        },
                        function (error) {
                            console.log('error');
                        }
                    )
            }
        }
    })
