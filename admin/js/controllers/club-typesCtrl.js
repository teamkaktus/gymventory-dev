controllers
    .controller('clubTypesCtrl', function($scope, $rootScope, $state, categoryFactory, SweetAlert) {
        $scope.tp = {
            name: '',
            percent: ''
        };
        $scope.types = {};
        
        $scope.newForm = false;

        categoryFactory.getAll().then(
            function (response) {
                $scope.types = response.data.rows;
            },
            function error(){
                console.log(error)
            }
        );

        $scope.newCategory = function () {
            $scope.newForm = true;
        }

        $scope.up_category = function(data){
            console.log(data);
            categoryFactory.update(data).then(
                function (response){
                    if(response.status){
                        SweetAlert.swal("Success!", "Type updated!", "success")
                    }else{
                        SweetAlert.swal("Error!", "Not updated type!", "error")
                    }

                },
                function (error){
                    console.log(error);
                }
            )
        };

        $scope.add_category = function (data) {
            if(data.name != '' && data.percent != ''){
                console.log(data)
                categoryFactory.add(data).then(
                    function(response){
                        console.log(response);
                        $scope.newForm = false;
                        $state.reload();
                    },
                    function(error){
                        console.log(error);
                    }
                )
            }

        };
        console.log('types');
    });