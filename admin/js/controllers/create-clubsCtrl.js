controllers.controller('create-clubsCtrl', function ($scope, clubFactory, $state) {

    $scope.map = {
        center: {
            latitude: 56.162939,
            longitude: 10.203921
        },
        showOverlay: false,
        zoom: 12
    };

    $scope.options = {
        scrollwheel: false
    };
    $scope.club = {
        latitude: '',
        longitude: ''
    };

    $scope.marker = {
        id: 0,
        coords: {
            latitude: 56.162939,
            longitude: 10.203921
        },
        options: {
            draggable: true
        },
        events: {
            dragend: function (marker, eventName, args) {
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();

                $scope.club.latitude = lat;
                $scope.club.longitude = lon;

                $scope.marker.options = {
                    draggable: true,
                    labelContent: "",
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                };
            }
        }
    };

    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
        $scope.map.center.latitude = $scope.marker.coords.latitude;
        $scope.map.center.longitude = $scope.marker.coords.longitude;

        if (_.isEqual(newVal, oldVal))
            return;
        return true;
    });

    clubFactory.getClubData().then(
        function (response) {
            if(!response.data){
                $state.go('app.home');
            }
            $scope.gym = {
              id: response.data.club_id
            };
            $scope.club = {
                id: response.data.club_id,
                name: response.data.name,
                type: response.data.type,
                address: response.data.address,
                latitude: response.data.latitude,
                longitude: response.data.longitude,
                phone: response.data.phone,
                website: response.data.website,
                email: response.data.email,
                fax: response.data.fax,
                day: {
                    mon: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    tue: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    wed: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    thu: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    fri: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    sat: {
                        start: '08 am',
                        end: '10 pm'
                    },
                    sun: {
                        start: '08 am',
                        end: '10 pm'
                    }
                }
            };

            $scope.day = {mon: true, tue: true, wed: true, thu: true, fri: true, sat: false, sun: false};
            if (response.data.hours) {
                $scope.day = {mon: false, tue: false, wed: false, thu: false, fri: false, sat: false, sun: false};
                var work_hours = JSON.parse(response.data.hours);
                for (var key in work_hours) {
                    $scope.day[key] = true;
                    $scope.club.day[key] = work_hours[key];
                }
            }


            $scope.map.center.latitude = response.data.latitude;
            $scope.map.center.longitude = response.data.longitude;

            $scope.marker.coords.latitude = response.data.latitude;
            $scope.marker.coords.longitude = response.data.longitude;

        },
        function (error) {
            console.log(error);
        }
    );


    $scope.changeAddress = function () {
        clubFactory.getAddressCoordinate($scope.club.address).then(
            function (response) {
                var coords = response.data.results[0].geometry.location;

                $scope.map.center.latitude = coords.lat;
                $scope.map.center.longitude = coords.lng;

                $scope.marker.coords.latitude = coords.lat;
                $scope.marker.coords.longitude = coords.lng;

                $scope.club.latitude = coords.lat;
                $scope.club.longitude = coords.lng;
            },
            function (error) {
            }
        )
    };

    $scope.o_hours = [];

    function get24clock() {
        var hour_tmp = 0;
        var array = [];
        for (var i = 1; i <= 12; i++) {
            if (i < 10) {
                hour_tmp = '0' + i + ' am';
            } else {
                hour_tmp = i + ' am';
            }
            array.push(hour_tmp);
        }
        for (var j = 1; j <= 12; j++) {
            if (j < 10) {
                hour_tmp = '0' + j + ' pm';
            } else {
                hour_tmp = j + ' pm';
            }
            array.push(hour_tmp);
        }
        return array;
    }

    $scope.o_hours = get24clock();

    $scope.editClub = function (data) {
        if (data.day) {
            for (var key in $scope.day) {
                if ($scope.day[key] == false) {
                    if (data.day[key])
                        delete(data.day[key]);
                }
            }
        }
        clubFactory.updateClubData(data).then(
            function (response) {
                $state.reload();
                console.log(response);

            },
            function (error) {
                console.log(error);
            }
        )
    }
});