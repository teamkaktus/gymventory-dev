controllers
    .controller('added_clubsCtrl', function($scope, $rootScope, $state, ClubsFactory, categoryFactory) {
        $scope.clubs = {};
        $scope.club_types = {};
        categoryFactory.getAll().then(
            function (response){
                $scope.club_types = response.data.rows;
                console.log(response.data.rows);
            },
            function (error){
                console.log(error);
            }
        );
        ClubsFactory.getClubs().then(
            function (response) {
                console.log(response.data);
                $scope.club_cards = response.data;
            },
            function (error) {
                console.log(error);
            }
        );
        $scope.confirm = function(club){
            console.log(club);
            ClubsFactory.confirmClub(club).then(
                function (response) {
                    $state.reload();
                },
                function (error) {
                    console.log(error);
                })
        }
        $scope.remove = function(id){
            ClubsFactory.removeClub({club: id}).then(
                function (response) {
                    $state.reload();
                },
                function (error) {
                    console.log(error);
                })
        }
    });