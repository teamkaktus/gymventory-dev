controllers
    .controller('indexCtrl', function($scope, $rootScope, $state, AuthFactory) {
        $scope.toggleClass = function($event, className) {
            className = className || 'toggled';
            if($scope.leftSidebar == undefined || $scope.leftSidebar == ''){
                $scope.leftSidebar = className;
            }else{
                $scope.leftSidebar = '';
            }
        };
    });