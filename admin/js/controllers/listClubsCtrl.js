controllers
    .controller('listClubsCtrl', function ($scope, $state, ClubsFactory, ngDialog) {

        ClubsFactory.getListClubs().then(
            function (response) {
                for(var index in response.data){
                    response.data[index].hours = JSON.parse(response.data[index].hours);
                }
                console.log(response.data);
                $scope.club_cards = response.data;
            }
        );

        $scope.updateClub = function(id){
            $scope.club_id = id;
            var dialog = ngDialog.open({
                template: 'updateModalContent',
                className: 'ngdialog-theme-plain',
                controller: 'updateClubsCtrl',
                scope: $scope,
                preCloseCallback: function(value) {

                }
            });
        }
    });