controllers
    .controller('registeredUsersCtrl', function ($scope, $state, UsersFactory) {
        UsersFactory.getClubs().then(
            function (response) {
                $scope.users = response.data;
            }
        );
    });