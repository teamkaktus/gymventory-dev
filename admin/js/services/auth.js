services

    .factory('AuthFactory', ['$http', '$state', 'ConfigApi', function ($http, $state, ConfigApi) {
        var urlBase = ConfigApi.serverAddress + '/';
        var AuthFactory = {};

        AuthFactory.adminauth = function (email, password) {
            return $http.post(urlBase + 'adminauth', {email: email, password: password});
        };
        AuthFactory.forgotpsw = function (email) {
            return $http.post(urlBase + 'forgotpsw', {email: email});
        };

        AuthFactory.authenticated = function () {
            return (window.localStorage.getItem('admin_token') != undefined) ? true : false;
        };

        AuthFactory.setToken = function (token) {
            window.localStorage.setItem('admin_token', token);
        };
        AuthFactory.removeToken = function () {
            localStorage.removeItem('admin_token');
        };
        AuthFactory.getToken = function () {
            return window.localStorage.getItem('admin_token');
        };

        return AuthFactory;
    }]);
