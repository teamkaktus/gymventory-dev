var services = angular.module('admin.services', [])
    .factory('ClubsFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var ClubsFactory = {};

        ClubsFactory.getAddressCoordinate = function(address){
            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + ConfigApi.googleKey);
        };

        ClubsFactory.getClubs = function () {
            return $http.get(urlBase + 'getAdminUClubs', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        ClubsFactory.confirmClub = function (data) {
            return $http.post(urlBase + 'confirmAdminUClubs', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.addAdminClub = function (data) {
            return $http.put(urlBase + 'addClubAdmin', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.removeClub = function (data) {
            return $http.post(urlBase + 'removeAdminUClubs', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.getListClubs = function () {
            return $http.get(urlBase + 'getAdminListClubs', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.getAdminClubs = function (id) {
            return $http.get(urlBase + 'clubAdminData/'+id, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.uniqueClubs = function (name) {
            return $http.get(urlBase + 'uniqueNameClub/'+name, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        ClubsFactory.updateAdminClub = function (data) {
            return $http.post(urlBase + 'updateClubData', data, {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return ClubsFactory;

    }])
    .factory('UsersFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var UsersFactory = {};

        UsersFactory.getClubs = function () {
            return $http.get(urlBase + 'getAdminRegUsers', {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return UsersFactory;

    }])
    .factory('categoryFactory', ['$http', '$state', 'ConfigApi', 'AuthFactory', function ($http, $state, ConfigApi, AuthFactory) {
        var urlBase = ConfigApi.serverAddress + '/';
        var categoryFactory = {};

        categoryFactory.add = function (data) {
            return $http.post(urlBase + 'addClubType', data,  {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        categoryFactory.getAll = function () {
            return $http.get(urlBase + 'getClubTypes',  {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };
        categoryFactory.update = function (data) {
            return $http.post(urlBase + 'updateClubTypes', data,  {headers: {'X-Access-Token': AuthFactory.getToken()}});
        };

        return categoryFactory;

    }]);